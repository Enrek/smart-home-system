/*
 * WebSocketServer_LEDcontrol.ino
 *
 *  Created on: 26.11.2015
 *
 */

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Hash.h>

#include <ModbusMaster232.h>

#define LED_RED     15
#define LED_GREEN   12
#define LED_BLUE    13

#define USE_SERIAL Serial


ESP8266WiFiMulti WiFiMulti;

ESP8266WebServer server = ESP8266WebServer(80);
WebSocketsServer webSocket = WebSocketsServer(81);
ModbusMaster232 node(1);

int pos = 0;
int ledNum = 30;

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {

    switch(type) {
        case WStype_DISCONNECTED:
            //USE_SERIAL.printf("[%u] Disconnected!\n", num);
            break;
        case WStype_CONNECTED: {
            IPAddress ip = webSocket.remoteIP(num);
           // USE_SERIAL.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);

            // send message to client
            webSocket.sendTXT(num, "Connected");
        }
            break;
        case WStype_TEXT:
            //USE_SERIAL.printf("[%u] get Text: %s\n", num, payload);

            if(payload[0] == '#') {
                // we get RGB data

                // decode rgb data
                uint32_t rgb = (uint32_t) strtol((const char *) &payload[1], NULL, 16);
                node.setTransmitBuffer(0, pos);
                node.setTransmitBuffer(1, (rgb >> 16) & 0xFF);
                node.setTransmitBuffer(2, (rgb >> 8) & 0xFF);
                node.setTransmitBuffer(3, (rgb >> 0) & 0xFF);
                node.writeMultipleRegisters(0,4);
                node.clearTransmitBuffer();
                pos++;
                if(pos>ledNum) 
                  pos=0;
            }

            break;
    }

}

void setup() {
node.begin(9600);
   
    //USE_SERIAL.begin(921600);
   // USE_SERIAL.begin(115200);

    //USE_SERIAL.setDebugOutput(true);

    USE_SERIAL.println();
    USE_SERIAL.println();
    USE_SERIAL.println();

    for(uint8_t t = 4; t > 0; t--) {
       USE_SERIAL.printf("[SETUP] BOOT WAIT %d...\n", t);
       USE_SERIAL.flush();
        delay(1000);
    }

    WiFiMulti.addAP("DSL-2650U", "19760107");

    while(WiFiMulti.run() != WL_CONNECTED) {
        delay(100);
    }

    // start webSocket server
    webSocket.begin();
    webSocket.onEvent(webSocketEvent);

    if(MDNS.begin("esp8266")) {
        USE_SERIAL.println("MDNS responder started");
    }

    // handle index
    server.on("/", []() {
        // send index.html
        server.send(200, "text/html", "<html> <head> <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script> <script src='https://cdnjs.cloudflare.com/ajax/libs/chroma-js/1.3.4/chroma.min.js'></script> <script>var mouseX;var mouseY;var topOffset = 0.5;var color='#FF0000';$(function () {$('#colorpicker').on('mousemove', function (e) {mouseX = e.pageX;mouseY = e.pageY;generateColor(mouseX, mouseY, topOffset);});$('#colorpicker').on('scroll', function () {topOffset = $('#huescroll').position().top;topOffset = Math.abs(topOffset);generateColor(mouseX, mouseY, topOffset);});});$('#colorpicker').on('click', function () {});function generateColor(mouseX, mouseY, topOffset) {var windowWidth = $('#colorpicker').width();var windowHeight = $('#colorpicker').height();var hue = Math.ceil(mouseX / windowWidth * 360);var saturation = topOffset / windowHeight;console.log(saturation);var lightness = mouseY / windowHeight;color = chroma(hue, saturation, lightness, 'hsl');var contrast = chroma.contrast(color, 'black');$('#colorValue').text(color);if (contrast < 4.5) {$('#colorValue').css('color', 'white');} else {$('#colorValue').css('color', 'black');}$('#colorpicker').css('background-color', color);} var connection = new WebSocket('ws://'+location.hostname+':81/', ['arduino']); connection.onopen = function () { connection.send('Connect ' + new Date()); }; connection.onerror = function (error) { console.log('WebSocket Error ', error);}; connection.onmessage = function (e) { console.log('Server: ', e.data);}; setInterval(function() { console.log('Send color ' + color);connection.send(color);}, 100); </script> </head> <style> body { height: 100vh; width: 100vw; overflow: hidden; cursor: crosshair;}* { -ms-touch-action: none; touch-action: none;}#colorpicker { width: 100vw; height: 100vh; overflow: auto; position: fixed; top: 0; left: 0;}#colorValue { position: fixed; top: 0; left: 0; width: 100vw; height: 100vh; display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-align: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -ms-flex-pack: center; justify-content: center; pointer-events: none; font-family: 'Lato', sans-serif; font-size: 48px; font-weight: 700; text-transform: uppercase; -webkit-transition: color .125s ease; transition: color .125s ease;}#huescroll { width: 100vw; height: 200vh;} </style> <div id='colorpicker'><div id='huescroll'></div> </div><div id='colorValue'>#FF0000</div></html>");
    });

    server.begin();
   
    // Add service to MDNS
    MDNS.addService("http", "tcp", 80);
    MDNS.addService("ws", "tcp", 81);
  //  node.begin(9600);
    Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
    webSocket.loop();
    server.handleClient();
}

