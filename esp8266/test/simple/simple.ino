
#include <ESP8266WiFi.h>
#include "./DNSServer.h"                  // Patched lib
#include <ESP8266WebServer.h>
#include <ModbusMaster232.h>

const byte        DNS_PORT = 53;          // Capture DNS requests on port 53
IPAddress         apIP(10, 10, 10, 1);    // Private network for server
DNSServer         dnsServer;              // Create the DNS object
ESP8266WebServer  webServer(80);          // HTTP server
ModbusMaster232 main(1);
ModbusMaster232 controller(2);



String responseHTML = 
  "<p>Connect</p>"
  "<form action='/' method='POST'> <button type='button submit' name='init' value='1' class='btn btn-success btn-lg'>Connect</button></form>"
  "<p>FAN</p>"
  "<form action='/' method='POST'> <button type='button submit' name='F' value='1' class='btn btn-success btn-lg'>ON</button></form>  <form action='/' method='POST'>  <button type='button submit' name='F' value='0' class='btn btn-danger btn-lg'>OFF</button></form>"
  ;

String top = "<!DOCTYPE html>"
  "<html lang=\"en\">"
  "<head>"
    "<meta charset=\"utf-8\">"
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
    "<title>Internet of Bottles</title>"
  "</head>"
  "<body>";

String bottom = "</body>"
  "</html>";
  
String buildConnect(String errorMsg = "") {
   return top +
  "<p>Connect</p>"
  "<form action='/' method='POST'> <button type='button submit' name='connect' value='1' class='btn btn-success btn-lg'>Connect</button></form>"
  "<h3 style='color:red'>"+errorMsg+"</h3>"
   + bottom;
}

void setup() {
  main.begin(9600);
  controller.begin(9600);
  // configure access point
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP("IoT --- Free WiFi"); // WiFi name

  // if DNSServer is started with "*" for domain name, it will reply with
  // provided IP to all DNS request
  dnsServer.start(DNS_PORT, "*", apIP);

  // replay to all requests with same HTML
  webServer.onNotFound([]() {
    webServer.send(404, "text/html", "=(");
  });
  webServer.on ( "/",  handleRoot);
  webServer.begin();

}


void handleConnect() {
    int result = controller.writeSingleCoil(0, 1); 
   if(result != 0) {
      webServer.send ( 200, "text/html", buildConnect("Controller device connection error! Try again."));
      return;
   }
    result = main.writeSingleCoil(0, 1); 
   if(result != 0) {
      webServer.send ( 200, "text/html", buildConnect("Main device connection error! Try again."));
      return;
   }

   webServer.send ( 200, "text/html", "=)");
}


void handleRoot(){ 
   if(webServer.hasArg("connect")) {
      handleConnect();
   } else
  if ( webServer.hasArg("F") ) {
   main.writeSingleCoil(1, webServer.arg("F") == "0"); 
   delay(100);
  
   webServer.send ( 200, "text/html", responseHTML);
  } else {
    webServer.send ( 200, "text/html", buildConnect());
  }  

  main.clearResponseBuffer();
   controller.clearResponseBuffer();
}

void loop() {
  dnsServer.processNextRequest();
  webServer.handleClient();
}
