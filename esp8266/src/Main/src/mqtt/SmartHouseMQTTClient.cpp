#include "SmartHouseMQTTClient.h"

void SmartHouseMQTTClient::begin() {
    //   setupMQTTCongfig();
    clientMQTT.begin(mqttHost.c_str(), clientWiFiMQTT);
    connectMQTT();
}

void SmartHouseMQTTClient::processMessage(const char* topic, const char* json) {
    lastMillis = millis();
    if (millis() - lastReciveMillis < 2000) {
        ERROR("[MQTT] Too many messages! I can't do it senpai");
        //clientMQTT.publish(topic.c_str(), const char*("").c_str(), 0, true, 2); // CLEAR TOPIC (NOT TESTED =D )
        return;
    }
    lastReciveMillis = millis();
    DEBUG("[MQTT] Incoming: ");
    DEBUG(topic);
    StaticJsonBuffer<1000> jsonBuffer;
    JsonObject& data = jsonBuffer.parseObject(json);

    if (!data.success()) {
        ERROR("[MQTT] Can't parse JSON!");
        return;
    }

    const char* deviceType = data["deviceType"];
    const char* deviceName = data["deviceName"];
    const char* method = data["method"];
    JsonObject& payload = data["d"];

    if (method == NULL || strlen(method) == 0) {
        ERROR("[MQTT] Property method can't be empty!");
        return;
    }
    if (strcmp(method, "shakeHand") == 0) {
        DEBUG("[MQTT] shakeHand mode activated :)");
        DEBUG("[MQTT] Hello, my name is BINGO");
        topicToPublisIndex = 0;
        sendAll = true;
        return;
    }

    if (strcmp(method, "info") == 0) {
        infoPending = true;
        return;
    }

    if (deviceType == NULL || strlen(deviceType) == 0) {
        ERROR("[MQTT] Property deviceType can't be empty!");
        return;
    }
    if (deviceName == NULL || strlen(deviceName) == 0) {
        ERROR("[MQTT] Property deviceName can't be empty!");
        return;
    }

    if (strcmp(topic, MQTT_REQUEST_PATH) != 0) {
        return;
    }
    char* canonicalTopicName = (char*)calloc(strlen(MQTT_BASE_PATH) + strlen(deviceType) + strlen(deviceName) + strlen("/") * 2 + 1, sizeof(char));
    strcpy(canonicalTopicName, MQTT_BASE_PATH);
    strcat(canonicalTopicName, deviceType);
    strcat(canonicalTopicName, "/");
    strcat(canonicalTopicName, deviceName);
    strcat(canonicalTopicName, "/");

    for (int i = 0; i < topicsCount; i++) {
        if (topics[i].isManageable() && strcmp(topics[i].getTopic(), canonicalTopicName) == 0) {
            DEBUG("[MQTT] Process requset for: ");
            DEBUG(canonicalTopicName);
            if (topics[i].processRequest(method, payload)) {
                pendingResponse = true;
                topics[i].setPendingResponse(true);
                DEBUG("[MQTT] Response add to pending queue ");
            } else {
                ERROR("[MQTT] This method is not allowed: ");
                ERROR(method);
            }
            free(canonicalTopicName);
            return;
        }
    }
    free(canonicalTopicName);
    ERROR("[MQTT] Can't find device :");
    ERROR(canonicalTopicName);
}

void SmartHouseMQTTClient::handle() {
    if (lastConnectionFailedMQTT) {
        if (millis() - lastTimeErrorMessageShown > 30000) {  // 30 sec.
            ERROR("[MQTT] Can't connect to MQTT broker, try to reboot device or change config");
            lastTimeErrorMessageShown = millis();
        }
        return;
    }
    clientMQTT.unsubscribe(MQTT_REQUEST_PATH);
    clientMQTT.subscribe(MQTT_REQUEST_PATH, 0);
    clientMQTT.loop();
    delay(10);

    if (pendingResponse) {
        sendPendingResponse();
        pendingResponse = false;
        return;
    }
    if (infoPending) {
        deviceInfo();
        infoPending = false;
        return;
    }

    if (sendErrorCount > 10) {
        ERROR("[MQTT] Too much errors try to reconect");
        clientMQTT.disconnect();
        connectMQTT();
    }

    if (!clientMQTT.connected() && !lastConnectionFailedMQTT) {
        connectMQTT();
    } else {
        if (millis() - lastMillis > MQTT_UPDATE_TIME) {
            if (++topicToPublisIndex == topicsCount) {
                topicToPublisIndex = 0;
                sendAll = false;
            }
            if (topics[topicToPublisIndex].isManageable() && !sendAll) {
                return;
            }
            topics[topicToPublisIndex].publish();
        }
    }
}

void SmartHouseMQTTClient::deviceInfo() {
    //   DEBUG("Publish MQTT " + MQTT_BASE_PATH + "info/");
    StaticJsonBuffer<2000> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    JsonArray& devices = root.createNestedArray("devices");
    for (int i = 0; i < topicsCount; i++) {
        JsonObject& device = devices.createNestedObject();
        device["type"] = topics[i].getType();
        device["name"] = topics[i].getName();
        device["topic"] = topics[i].getTopic();
        device["manageable"] = topics[i].isManageable();
    }
    JsonObject& method = root.createNestedObject("methods");
    method["get"] = "Initiate publication to device topic";
    method["set"] = "Update device";
    method["shakeHand"] = "Initiate publications in all device topics";
    method["info"] = "This topic";
    char* json = (char*)malloc(sizeof(char) * 2000);
    root.prettyPrintTo(json, 2000);
    if (publish(MQTT_INFO_PATH, json)) {
        DEBUG("[MQTT] Message was sent");
        sendErrorCount = 0;
    } else {
        ERROR("[MQTT] Message was not sent");
        sendErrorCount++;
    }
    free(json);
}

void SmartHouseMQTTClient::sendPendingResponse() {
    for (int i = 0; i < topicsCount; i++) {
        if (topics[i].isPendingResponse()) {
            topics[i].publish();
            topics[i].setPendingResponse(false);
            DEBUG("[MQTT] Client response was removed from pending queue: ");
            DEBUG(topics[i].getTopic());
        }
    }
}

bool SmartHouseMQTTClient::publish(const char* topic, const char* payload) {
    lastMillis = millis();
    bool result = clientMQTT.publish(topic, payload, true, mqttQOS);
    if (!result) {
        logError();
    }
    return result;
}

void SmartHouseMQTTClient::connectMQTT() {
    if (lastConnectionFailedMQTT) {
        DEBUG("[MQTT] Can't connect, try to reboot device or change config");
        return;
    }
    // DEBUG(" MQTT connecting to " + mqttHost);
    int attempt = 0;
    bool connected = false;
    // if (mqttUsername.length() != 0) {
    //   DEBUG("Connecting to MQTT broker as " + mqttUsername.length());
    //   connected = clientMQTT.connect(mqttClientName.c_str(), mqttUsername.c_str(), mqttPassword.c_str());
    // } else {
    DEBUG("[MQTT] Connecting to MQTT broker without username");
    connected = clientMQTT.connect(mqttClientName.c_str());
    // }
    while (!connected) {
        delay(100);
        if (attempt++ > 50) {
            ERROR("[MQTT] Connection failed!");
            logError();
            lastConnectionFailedMQTT = true;
            return;
        }
    }
    DEBUG("[MQTT] Connected!");
}

// MQTT Device

bool SmartHouseMQTTClient::DeviceMQTT::processRequest(const char* method, JsonObject& payload) {
    if (strcmp(method, "get") == 0) {
        return true;
    } else if (strcmp(method, "set") == 0) {
        updater(payload);
    } else {
        return false;
    }
    return true;
}

void SmartHouseMQTTClient::DeviceMQTT::publish(const char* topicURI) {
    if (lastConnectionFailedMQTT) {
        DEBUG("[MQTT] Can't connect, try to reboot device or change config");
        return;
    }

    DEBUG("[MQTT] Publish MQTT :");
    DEBUG(topicURI);
    StaticJsonBuffer<2000> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    JsonObject& device = root.createNestedObject("d");
    populator(device);

    char* json = (char*)malloc(sizeof(char) * 2000);
    root.prettyPrintTo(json, 2000);
    if (client.publish(topicURI, json)) {
        DEBUG("[MQTT] Message was sent");
        client.sendErrorCount = 0;
    } else {
        ERROR("[MQTT] Message was not sent");
        client.logError();
        client.sendErrorCount++;
    }
    free(json);
}

void SmartHouseMQTTClient::logError() {
    ERROR("[MQTT] Error code :");
    switch (clientMQTT.lastError()) {
        case 0:
            ERROR("It's not ERROR o_0");
            break;
        case -1:
            ERROR("LWMQTT_BUFFER_TOO_SHORT");
            statusDisplay.setErrorCode(201);
            break;
        case -2:
            ERROR("LWMQTT_VARNUM_OVERFLOW");
            statusDisplay.setErrorCode(202);
            break;
        case -3:
            ERROR("LWMQTT_NETWORK_FAILED_CONNECT");
            statusDisplay.setErrorCode(203);
            break;
        case -4:
            ERROR("LWMQTT_NETWORK_TIMEOUT");
            statusDisplay.setErrorCode(204);
            break;
        case -5:
            ERROR("LWMQTT_NETWORK_FAILED_READ");
            statusDisplay.setErrorCode(205);
            break;
        case -6:
            ERROR("LWMQTT_NETWORK_FAILED_WRITE");
            statusDisplay.setErrorCode(206);
            break;
        case -7:
            ERROR("LWMQTT_REMAINING_LENGTH_OVERFLOW");
            statusDisplay.setErrorCode(207);
            break;
        case -8:
            ERROR("LWMQTT_REMAINING_LENGTH_MISMATCH");
            statusDisplay.setErrorCode(208);
            break;
        case -10:
            ERROR("LWMQTT_CONNECTION_DENIED");
            statusDisplay.setErrorCode(209);
            break;
        case -11:
            ERROR("LWMQTT_FAILED_SUBSCRIPTION");
            statusDisplay.setErrorCode(210);
            break;
        case -9:
            ERROR("LWMQTT_MISSING_OR_WRONG_PACKET");
            statusDisplay.setErrorCode(211);
            break;
        case -12:
            ERROR("LWMQTT_SUBACK_ARRAY_OVERFLOW");
            statusDisplay.setErrorCode(212);
            break;
        case -13:
            ERROR("LWMQTT_PONG_TIMEOUT");
            statusDisplay.setErrorCode(213);
            break;
    }
}