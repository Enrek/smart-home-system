//SmartHouseMQTTClient.h

#ifndef SMART_HOUSE_MQTT_CLIENT
#define SMART_HOUSE_MQTT_CLIENT
#include <MQTT.h>
#include <ArduinoJson.h>
#include "../modbus/ModbusController.h"
#include "SmartHouseConfiguration.h"
#include "display/StatusDisplay.h"

class SmartHouseMQTTClient {
  public:
    void begin();
    void handle();
    void connectMQTT();
    bool publish(const char* topic, const char* payload);
    void processMessage(const char* topic,const char* payload);
    void sendPendingResponse();
    void deviceInfo();
    void logError();
    MQTTClient* getClient() {
      return &clientMQTT;
    }

    class DeviceMQTT
    {
      public:
        DeviceMQTT(const char* deviceType,
                   const char* deviceName, 
                   DEVICE_JSON_POPULATE_FUN populator, 
                   SmartHouseMQTTClient& client,
                   DEVICE_JSON_UPDATE_FUN updater = NULL) : client(client) {
          this->topic =  (char*) calloc(strlen(MQTT_BASE_PATH)+strlen(deviceType)+strlen(deviceName)+strlen("/")*2+1, sizeof(char));
          strcpy(this->topic,MQTT_BASE_PATH);
          strcat(this->topic,deviceType);
          strcat(this->topic,"/");
          strcat(this->topic,deviceName);
          strcat(this->topic,"/");
          this->populator = populator;
          this->updater = updater;
          this->type = deviceType;
          this->name = deviceName;
        }
        bool isManageable() {
          return updater != NULL;
        }
        const char* getTopic() {
          return topic;
        }
        const char* getName() {
          return name;
        }
        const char* getType() {
          return type;
        }
        bool processRequest(const char* method, JsonObject& data);
        void publish()
        {
          return publish(topic);
        }
        void publish(const char* topicURI);
        bool isPendingResponse() {
          return pendingResponse;
        }
        void setPendingResponse(bool pendingResponse) {
          this->pendingResponse = pendingResponse;
        }
        
      private:
        SmartHouseMQTTClient&  client;
        bool pendingResponse;
        char* topic;
        const char* name;
        const char* type;
        bool manageable;
        DEVICE_JSON_POPULATE_FUN populator = NULL;
        DEVICE_JSON_UPDATE_FUN updater = NULL;
    };
  private:
    unsigned long lastTimeErrorMessageShown;
    bool infoPending;
    bool sendAll = true;
    bool pendingResponse;
    WiFiClient clientWiFiMQTT;
    MQTTClient clientMQTT = MQTTClient(2048);
    int sendErrorCount = 0;
    unsigned long lastMillis = 0;
    unsigned long lastReciveMillis = 0;
    int topicToPublisIndex = 0;
    int topicsCount = 13;
    typedef void (*MQTTClientCallbackSimple)(const char* topic, const char* payload);


    DeviceMQTT topics[13] = {DeviceMQTT("sensor", "temp", populateTempSensor, *this),
                             DeviceMQTT("sensor", "co2", populateCO2Sensor, *this),
                             DeviceMQTT("sensor", "solar", populateSolarSensor, *this),
                             DeviceMQTT("sensor", "pir/0", populateFirstFloorPIRSensor, *this),
                             DeviceMQTT("sensor", "pir/1", populateSecondFloorPIRSensor, *this),
                             DeviceMQTT("module", "fan", populateFanModule, *this, updateFanModule),
                             DeviceMQTT("module", "fire", populateFireModule, *this, updateFireModule),
                             DeviceMQTT("module", "security", populateSecurityModule, *this, updateSecurityModule),
                             DeviceMQTT("module", "conditioner", populateConditionerModule, *this, updateConditionerModule),
                             DeviceMQTT("module", "heating", populateHeatingModule, *this, updateHeatingModule),
                             DeviceMQTT("module", "socket", populateSocketModule, *this, updateSocketModule),
                             DeviceMQTT("light", "sunblind", populateSunbling, *this, updateSunblind),
                             DeviceMQTT("light", "led", populateLED, *this, updateLED)
                            };

};

#endif

