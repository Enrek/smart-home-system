/**
 * Test the telnet functions.
 *
 * Perform a test using the telnet functions.
 * This code exports two new global functions:
 *
 * void telnet_listenForClients(void (*callback)(uint8_t *buffer, size_t size))
 * void telnet_sendData(uint8_t *buffer, size_t size)
 *
 * For additional details and documentation see:
 * * Free book on ESP32 - https://leanpub.com/kolban-ESP32
 *
 *
 * Neil Kolban <kolban1@kolban.com>
 *
 */
#include "Telnet.h"

WiFiClient telnetClient;

WiFiServer telnetServer(23);

// The global tnHandle ... since we are only processing ONE telnet
// client at a time, this can be a global static.
static telnet_t *tnHandle;

static void (*receivedDataCallback)(uint8_t *buffer, size_t size);

  static const telnet_telopt_t my_telopts[] = {
    { TELNET_TELOPT_ECHO,      TELNET_WILL, TELNET_DONT },
    { TELNET_TELOPT_TTYPE,     TELNET_WILL, TELNET_DONT },
    { TELNET_TELOPT_COMPRESS2, TELNET_WONT, TELNET_DO   },
    { TELNET_TELOPT_ZMP,       TELNET_WONT, TELNET_DO   },
    { TELNET_TELOPT_MSSP,      TELNET_WONT, TELNET_DO   },
    { TELNET_TELOPT_BINARY,    TELNET_WILL, TELNET_DO   },
    { TELNET_TELOPT_NAWS,      TELNET_WILL, TELNET_DONT },
    { -1, 0, 0 }
  };


/**
 * Convert a telnet event type to its string representation.
 */
static char *eventToString(telnet_event_type_t type) {
	switch(type) {
	case TELNET_EV_COMPRESS:
		return "TELNET_EV_COMPRESS";
	case TELNET_EV_DATA:
		return "TELNET_EV_DATA";
	case TELNET_EV_DO:
		return "TELNET_EV_DO";
	case TELNET_EV_DONT:
		return "TELNET_EV_DONT";
	case TELNET_EV_ERROR:
		return "TELNET_EV_ERROR";
	case TELNET_EV_IAC:
		return "TELNET_EV_IAC";
	case TELNET_EV_SEND:
		return "TELNET_EV_SEND";
	case TELNET_EV_SUBNEGOTIATION:
		return "TELNET_EV_SUBNEGOTIATION";
	case TELNET_EV_WARNING:
		return "TELNET_EV_WARNING";
	case TELNET_EV_WILL:
		return "TELNET_EV_WILL";
	case TELNET_EV_WONT:
		return "TELNET_EV_WONT";
	}
	return "Unknown type";
} // eventToString


/**
 * Send data to the telnet partner.
 */
void telnet_esp32_sendData(uint8_t *buffer, size_t size) {
	if (tnHandle != NULL) {
		telnet_send(tnHandle, (char *)buffer, size);
	}
} // telnet_esp32_sendData


/**
 * Send a vprintf formatted output to the telnet partner.
 */
int telnet_esp32_vprintf(const char *fmt, va_list va) {
	if (tnHandle == NULL) {
		return 0;
	}
	return telnet_printf(tnHandle, fmt, va);
} // telnet_esp32_vprintf

/**
 * Telnet handler.
 */
static void telnetHandler(
		telnet_t *thisTelnet,
		telnet_event_t *event,
		void *userData) {
	int rc;
	switch(event->type) {
		case TELNET_EV_SEND:
			rc = telnetClient.write(event->buffer, event->size);
			break;
		case TELNET_EV_DATA:
			/**
			 * Here is where we would want to handle newly received data.
			 * The data receive is in event->data.buffer of size
			 * event->data.size.
			 */
			if (receivedDataCallback != NULL) {
				receivedDataCallback((uint8_t *)event->buffer, (size_t)event->size);
			}
			break;

		default:
			break;
	} // End of switch event type
} // myTelnetHandler


static void doTelnet() {
	//ESP_LOGD(tag, ">> doTelnet");

  tnHandle = telnet_init(my_telopts, telnetHandler, 0, NULL);

  uint8_t buffer[1024];
  while(telnetClient.available()) {
  	size_t len = telnetClient.readBytes((char *)buffer, telnetClient.available());
  	buffer[len] = '\0';
	if (len == 0) {
  		break;
  	}
  	telnet_recv(tnHandle, (char *)buffer, len);
  }
  //ESP_LOGD(tag, "Telnet partner finished");
  telnet_free(tnHandle);
  tnHandle = NULL;
} // doTelnet


/**
 * Listen for telnet clients and handle them.
 */
void telnet_esp32_listenForClients(void (*callbackParam)(uint8_t *buffer, size_t size)) {

	receivedDataCallback = callbackParam;
if (telnetServer.hasClient())
  {
    if (!telnetClient || !telnetClient.connected())
    {
      if(telnetClient) telnetClient.stop();
      telnetClient = telnetServer.available();
    }
  }
  if (telnetClient) {
		doTelnet();
  }
} // listenForNewClient