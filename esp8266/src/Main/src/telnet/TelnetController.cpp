#include "TelnetController.h"

void TelnetController::begin() {
    telnetServer.begin();
}
void TelnetController::task() {
    telnet_esp32_listenForClients(handleRequest);
}


static sarg_root root;
static char telnet_cmd_response_buff[RESPONSE_BUFFER_LEN];
static char telnet_cmd_buffer[CMD_BUFFER_LEN];

static const char* errorTag = "[ERROR] "; 
static const char* debugTag = "[DEBUG] "; 

static bool logEnabled = 0;

void DEBUG(const char* msg) {
    if(!logEnabled)
        return;
#ifdef SERIAL_LOG
    Serial.print("[DEBUG] ");
    Serial.println(msg);
#endif
    telnetClient.print(debugTag);
    telnetClient.println(msg);
}

void ERROR(const char* msg) {
#ifdef SERIAL_LOG
    Serial.print("[ERROR] ");
    Serial.println(msg);
#endif
    telnetClient.print(errorTag);
    telnetClient.println(msg);
}

static int helpCallback(const sarg_result *res) {
    UNUSED(res);
    char *buf;
    int ret;

    ret = sarg_help_text(&root, &buf);
    if (ret != SARG_ERR_SUCCESS)
        return ret;
    //    we can't spare much memory!!
    //    int length = 0;
    //    length += sprintf(telnet_cmd_response_buff+length, "%s\n", buf);
    //    ESP_LOGD(TAG," help_cb: %s",telnet_cmd_response_buff);
    //    telnet_esp32_sendData((uint8_t *)telnet_cmd_response_buff, strlen(telnet_cmd_response_buff));
    telnet_esp32_sendData((uint8_t *)buf, strlen(buf));
    free(buf);
    return 0;
}

static int logCallback(const sarg_result *res)
{
    UNUSED(res);
    logEnabled = !logEnabled;
    
    return SARG_ERR_SUCCESS;
}

static int configurationCallback(const sarg_result *res)
{
    UNUSED(res);

    uint8_t length = 0;
    length += sprintf(telnet_cmd_response_buff+length, "-- MQTT -- \n");
    length += sprintf(telnet_cmd_response_buff+length, "Host: ");
    length += sprintf(telnet_cmd_response_buff+length, mqttHost.c_str());
    length += sprintf(telnet_cmd_response_buff+length, "\nUser: ");
    length += sprintf(telnet_cmd_response_buff+length, mqttUsername.c_str());
    length += sprintf(telnet_cmd_response_buff+length, "\nPassword: ");
    length += sprintf(telnet_cmd_response_buff+length, mqttPassword.c_str());
    length += sprintf(telnet_cmd_response_buff+length, "\nClient name: ");
    length += sprintf(telnet_cmd_response_buff+length, mqttClientName.c_str());
    length += sprintf(telnet_cmd_response_buff+length, "\nQOS: %d \nTimeout: %d \n", mqttQOS, mqttTimeout);
    length += sprintf(telnet_cmd_response_buff+length, "\n-- Modbus TCP -- \n Slave ID: %d \n", MODBUS_TCP_SLAVE_ID);

    telnet_esp32_sendData((uint8_t *)telnet_cmd_response_buff, strlen(telnet_cmd_response_buff));
    return SARG_ERR_SUCCESS;
}

static int mqttHostCallback(const sarg_result *res)
{
    if(saveMQTTConfiguration(mqttClientName.c_str(), res->str_val, mqttUsername.c_str(), mqttPassword.c_str(), mqttQOS, mqttTimeout))
        restart();
    else 
        sprintf(telnet_cmd_response_buff, "Failed! \n");

    telnet_esp32_sendData((uint8_t *)telnet_cmd_response_buff, strlen(telnet_cmd_response_buff));
    restart();
    return SARG_ERR_SUCCESS;
}

static int mqttClientCallback(const sarg_result *res)
{
    if(saveMQTTConfiguration(res->str_val, mqttHost.c_str(), mqttUsername.c_str(), mqttPassword.c_str(), mqttQOS, mqttTimeout))
        restart();
    else 
        sprintf(telnet_cmd_response_buff, "Failed! \n");

    telnet_esp32_sendData((uint8_t *)telnet_cmd_response_buff, strlen(telnet_cmd_response_buff));
    restart();
    return SARG_ERR_SUCCESS;
}

static int mqttUserCallback(const sarg_result *res)
{
    int resLength = strlen(res->str_val);
    if(resLength == 2 && res->str_val[0] == '"' && res->str_val[1] == '"') {
        sprintf(telnet_cmd_response_buff, "");
    } else {
        sprintf(telnet_cmd_response_buff, res->str_val);
    }

    if(saveMQTTConfiguration(mqttClientName.c_str(), mqttHost.c_str(), telnet_cmd_response_buff, mqttPassword.c_str(), mqttQOS, mqttTimeout))
        restart();
    else 
        sprintf(telnet_cmd_response_buff, "Failed! \n");

    telnet_esp32_sendData((uint8_t *)telnet_cmd_response_buff, strlen(telnet_cmd_response_buff));
    restart();
    return SARG_ERR_SUCCESS;
}

static int mqttPasswordCallback(const sarg_result *res)
{
    int resLength = strlen(res->str_val);
    if(resLength == 2 && res->str_val[0] == '"' && res->str_val[1] == '"') {
        sprintf(telnet_cmd_response_buff, "");
    } else {
        sprintf(telnet_cmd_response_buff, res->str_val);
    }

    if(saveMQTTConfiguration(mqttClientName.c_str(), mqttHost.c_str(), mqttUsername.c_str(), telnet_cmd_response_buff, mqttQOS, mqttTimeout))
        restart();
    else 
        sprintf(telnet_cmd_response_buff, "Failed! \n");

    telnet_esp32_sendData((uint8_t *)telnet_cmd_response_buff, strlen(telnet_cmd_response_buff));
    return SARG_ERR_SUCCESS;
}

static int restartCallback(const sarg_result *res)
{
    restart();
    return SARG_ERR_SUCCESS;
}

static void restart() {
    if(telnetClient) telnetClient.stop();
    ESP.restart();
}

const static sarg_opt my_opts[] = {
    {"h", "help", "show help text", BOOL, helpCallback},
    {"d", "debug-log", "debug logs trigger (disable/enable)", BOOL, logCallback},
    {"c", "config", "show configuration", BOOL, configurationCallback},
    {"r", "restart", "restart ESP", BOOL, restartCallback},
    {NULL, "mqtt-host", "set new mqtt host and restart", STRING, mqttHostCallback},
    {NULL, "mqtt-user", "set new mqtt username and restart (\"\" - to set empty)", STRING, mqttUserCallback},
    {NULL, "mqtt-password", "set new mqtt password and restart (\"\" - to set empty)", STRING, mqttPasswordCallback},
    {NULL, "mqtt-client", "set new mqtt client name and restart", STRING, mqttClientCallback},


    // {"s", "stats", "system stats (0=mem,1=tasks)", INT, callBack},
    // {NULL, "clock", "set camera xclock frequency", INT, callBack},
    // {NULL, "pixformat", "set pixel format (yuv422, rgb565)", STRING, callBack},
    // {NULL, "framerate", "set framerate (14,15,25,30)", INT, callBack},
    // {NULL, "colorbar", "set test pattern (0=off/1=on)", INT, callBack},
    // {NULL, "saturation", "set saturation (1-256)", INT, callBack},
    // {NULL, "hue", "set hue (-180 to 180)", INT, callBack},
    // {NULL, "brightness", "set brightness (-4 to 4)", INT, callBack},
    // {NULL, "contrast", "set contrast (-4 to 4)", INT, callBack},
    // {NULL, "hflip", "flip horizontal (0=off/1=on)", INT, callBack},
    // {NULL, "vflip", "flip vertical (0=off/1=on)", INT, callBack},
    // {NULL, "light", "ov7670 light mode (0 - 5)", INT, callBack},
    // {NULL, "night", "ov7670 night mode (0 - 3)", INT, callBack},
    // {NULL, "effect", "special effects (0 - 8)", INT, callBack},
    // {NULL, "gamma", "ov7670 gamma mode (0=disabled,1=slope1)", INT, callBack},
    // {NULL, "whitebalance", "ov7670 whitebalance (0,1,2)", INT, callBack},
    // {NULL, "video", "video mode (0=off,1=on)", INT, callBack},
    {NULL, NULL, NULL, INT, NULL}};

void handleRequest(uint8_t *cmdLine, size_t len) {
    int ret = sarg_init(&root, my_opts, "SmartHouse");
    // assert(ret == SARG_ERR_SUCCESS);
    // lots of redundant code here! //strcpy or memcpy would suffice
    size_t cmd_len = len;
    if (len > CMD_BUFFER_LEN) cmd_len = CMD_BUFFER_LEN;
    for (int i = 0; i < cmd_len; i++)
        telnet_cmd_buffer[i] = *(cmdLine + i);
    telnet_cmd_buffer[cmd_len - 1] = '\0';

    if (telnet_cmd_buffer != NULL) {
        // next command will call sarg_parse and call callbacks as needed...
        ret = sarg_parse_command_buffer(&root, telnet_cmd_buffer, cmd_len);
        if (ret != SARG_ERR_SUCCESS) {
            sarg_destroy(&root);
        }
        // command has been parsed and executed!
    }
    sarg_destroy(&root);
}

TelnetController telnetController;