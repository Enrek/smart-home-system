#ifndef TELNET_CONTROLLER_H
#define TELNET_CONTROLLER_H


#define RESPONSE_BUFFER_LEN 256
#define CMD_BUFFER_LEN 128

#define UNUSED(x) ((void)x)

#include "Telnet.h"
#include "smallargs.h"
#include "Arduino.h"
#include "../SmartHouseConfiguration.h"

class TelnetController {
    public:
        void begin();
        void task();
};

static void restart();

void DEBUG(const char* msg);
void ERROR(const char* msg);

void handleRequest(uint8_t *cmdLine, size_t len);

extern TelnetController telnetController;

#endif