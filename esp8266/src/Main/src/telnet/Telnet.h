
#ifndef TELNET_H
#define TELNET_H

#include <stdlib.h> // Required for libtelnet.h
#include "libtelnet.h"
#include <ESP8266WiFi.h>
#include "Arduino.h"

void telnet_esp32_listenForClients(void (*callbackParam)(uint8_t *buffer, size_t size));
void telnet_esp32_sendData(uint8_t *buffer, size_t size);
int telnet_esp32_vprintf(const char *fmt, va_list va);
extern WiFiServer telnetServer;
extern WiFiClient telnetClient;
#endif