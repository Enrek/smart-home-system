/*
 * smallargs.h
 *
 *  Created on: 6 Oct 2016
 *      Author: Fabian Meyer
 *
 * LICENSE
 * =======
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Fabian Meyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef INCLUDE_SMALLARGS_H_
#define INCLUDE_SMALLARGS_H_

#include <stddef.h>
#include <string.h>
#include <stdlib.h>

#define SARG_VERSION "0.1.0"

#define SARG_ERR_SUCCESS       0
#define SARG_ERR_ERRNO        -1
#define SARG_ERR_OTHER        -2
#define SARG_ERR_INVALARG     -3
#define SARG_ERR_PARSE        -4
#define SARG_ERR_NOTFOUND     -5
#define SARG_ERR_ALLOC        -6
#define SARG_ERR_ABORT        -7

#define _SARG_UNUSED(e) ((void) e)
#define _SARG_IS_SHORT_ARG(s) (s[0] == '-' && s[1] != '-')
#define _SARG_IS_LONG_ARG(s) (s[0] == '-' && s[1] == '-')
#define _SARG_IS_HEX_NUM(s) (s[0] == '0' && s[1] == 'x')
#define _SARG_IS_OCT_NUM(s) (s[0] == '0' && strchr("1234567", s[1]) != NULL)


#define sarg_foreach(root, it) \
    for(_sarg_iterator_init((it), (root)); \
        _sarg_iterator_has_next((it)); \
        _sarg_iterator_next((it)))

typedef enum _sarg_opt_type {
    INT = 0,
    UINT,
    DOUBLE,
    BOOL,
    STRING,
    COUNT
} sarg_opt_type;

typedef struct _sarg_result {
    sarg_opt_type type;
    int count;
    union {
        int int_val;
        unsigned int uint_val;
        double double_val;
        int bool_val;
        char *str_val;
    };
} sarg_result;

typedef int (*sarg_opt_cb)(const sarg_result *);

typedef struct _sarg_opt {
    char *short_name;
    char *long_name;
    char *help;
    sarg_opt_type type;
    sarg_opt_cb callback;
} sarg_opt;

typedef struct _sarg_root {
    char *name;
    sarg_opt *opts;
    int opt_len;
    sarg_result *results;
    int res_len;
} sarg_root;

typedef struct _sarg_iterator {
    sarg_root *root;
    sarg_result *result;
    char *name;
    int idx;
} sarg_iterator;

void _sarg_iterator_init(sarg_iterator *it, sarg_root *root);

int _sarg_iterator_has_next(sarg_iterator *it);

void _sarg_iterator_next(sarg_iterator *it);

const char * sarg_strerror(const int errval);

void _sarg_result_destroy(sarg_result *res);

void _sarg_opt_destroy(sarg_opt *opt);

/**
 * @brief Destroys the given root and frees its memory.
 *
 * @param root root object that will be cleared
 */
void sarg_destroy(sarg_root *root);

void _sarg_result_init(sarg_result *res, sarg_opt_type type);

int _sarg_opt_init(sarg_opt *opt, const char *short_name, const char *long_name, const char *help, const sarg_opt_type type, sarg_opt_cb cb);

int _sarg_opt_duplicate(sarg_opt *dest, const sarg_opt *src);

int _sarg_opt_len(const sarg_opt *options);

/**
 * @brief Initializes the root data structure with the given options.
 *
 * @param root root data structure which will be used to parse arguments
 * @param options NULL-terminated array of allowed options
 * @param name name of the application
 *
 * @return SARG_ERR_SUCCESS on success or a SARG_ERR_* code otherwise
 */
int sarg_init(sarg_root *root, const sarg_opt *options, const char *name);

int _sarg_find_opt(sarg_root *root, const char *name);

int _sarg_get_number_base(const char *arg);

int _sarg_parse_int(const char *arg, sarg_result *res);

int _sarg_parse_uint(const char *arg, sarg_result *res);

int _sarg_parse_double(const char *arg, sarg_result *res);

int _sarg_parse_bool(const char *arg, sarg_result *res);

int _sarg_parse_str(const char *arg, sarg_result *res);

typedef int (*_sarg_parse_func)(const char *, sarg_result *);
static _sarg_parse_func _sarg_parse_funcs[COUNT] = {
    _sarg_parse_int,
    _sarg_parse_uint,
    _sarg_parse_double,
    _sarg_parse_bool,
    _sarg_parse_str,
};

/**
 * @brief Parses the given arguments with the given root object.
 *
 * The root object has to be initialized with sarg_init before
 * being passed to this function.
 *
 * If specified this function will call callback functions on the
 * appearance of the corresponding options.
 *
 * @param root root object which should be used to parse arguments
 * @param argv array of arguments to be parsed
 * @param argc number of elements in argv
 *
 * @return SARG_ERR_SUCCESS on success or a SARG_ERR_* code otherwise
 */
int sarg_parse(sarg_root *root, const char **argv, const int argc);

/**
 * @brief Access the parsing result of the specified option.
 *
 * @param root root object that was used to parse arguments
 * @param name short or long name of the option
 * @param res result object for the given option
 *
 * @return SARG_ERR_SUCCESS on success or SARG_ERR_NOTFOUND if the option was not found
 */
int sarg_get(sarg_root *root, const char *name, sarg_result **res);

#ifndef SARG_NO_PRINT

#include <stdarg.h>
#include <stdio.h>

int _sarg_buf_resize(char **buf, int *len);

int _sarg_snprintf(char **buf, int *len, int *off, const char *fmt, ...);

static const char *_sarg_opt_type_str[COUNT] = {
    "INT",
    "UINT",
    "DOUBLE",
    "",
    "STRING"
};

/**
 * @brief Prints a help text into the given buffer.
 *
 * The root object has to be initialized with sarg_init before
 * being passed to this function.
 *
 * This function dynamically allocates memory for the given
 * buffer, so that the help text fits into the buffer. outbuf
 * has to be freed manually afterwards.
 *
 * @param root root object for creating help text
 * @param outbuf dynamically allocated output buffer for help text
 *
 * @return SARG_ERR_SUCCESS on success or a SARG_ERR_* code otherwise
 */
int sarg_help_text(sarg_root *root, char **outbuf);

/**
 * @brief Prints a help text to stdout.
 *
 * @param root root object for creating help text
 *
 * @return SARG_ERR_SUCCESS on success or a SARG_ERR_* code otherwise
 */
int sarg_help_print(sarg_root *root);

#endif

#ifndef SARG_NO_FILE

#include <stdio.h>
#include <ctype.h>

char *_sarg_trim(char *str);

int _sarg_argv_resize(char ***argv, int *argc);

int _sarg_argv_add_arg(const char *fmt, const char *arg, int arglen,
                       char **argv, int *currarg);

int _sarg_argv_add_from_line(const char *line, char ***argv, int *argc,
                             int *currarg);

/**
 * @brief Parse arguments from the given argument file.
 *
 * Options in the file are specified without preceding dashes
 * '-'. If the option expects an additional parameter it has to
 * be separated by a whitespace ' '. One line per option.
 *
 * The arguments in the file have to be specified in the
 * following format:
 *
 * OPTION1 ARG
 * OPTION2 ARG
 * OPTION3
 * ...
 *
 * @param root root object which should be used to parse arguments
 * @param filename file which should be used to read arguments
 *
 * @return SARG_ERR_SUCCESS on success or a SARG_ERR_* code otherwise
 */
 /*
int sarg_parse_file(sarg_root *root, const char *filename)
{
    int argc, currarg, ret, i;
    size_t len;
    FILE *fp = NULL;
    char **argv = NULL;
    char *line = NULL;
    char *line_trim = NULL;

    fp = fopen(filename , "r");
    if(!fp)
        return SARG_ERR_ERRNO;

    currarg = 1;
    argc = 16;
    argv = (char **) malloc(sizeof(char *) * argc);
    if(!argv) {
        ret = SARG_ERR_ALLOC;
        goto _sarg_parse_file_exit;
    }
    memset(argv, 0, sizeof(char *) * argc);

    // convert arguments in file to arg vector
    len = 0;
    while((ret = getline(&line, &len, fp)) != -1) {
        line_trim = _sarg_trim(line);
        if(strlen(line_trim) == 0)
            continue;

        ret = _sarg_argv_add_from_line(line_trim, &argv, &argc, &currarg);
        if(ret != SARG_ERR_SUCCESS)
            goto _sarg_parse_file_exit;
    }

    // parse created arg vector
    ret = sarg_parse(root, (const char **) argv, currarg);
    if(ret != SARG_ERR_SUCCESS)
        goto _sarg_parse_file_exit;

    ret = SARG_ERR_SUCCESS;

_sarg_parse_file_exit:
    if(line)
        free(line);
    if(argv) {
        for(i = 0; i < currarg; ++i) {
            if(argv[i])
                free(argv[i]);
        }
        free(argv);
    }
    if(fp)
        fclose(fp);
    return ret;
}
*/

// TEKKER MOD
// buffer contains single command...
int sarg_parse_command_buffer(sarg_root *root, char *buff, const size_t len);


#endif

#endif