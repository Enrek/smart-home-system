
#ifndef SMART_HOUSE_CONFIGURATION
#define SMART_HOUSE_CONFIGURATION

#include "FS.h"
#include <ArduinoJson.h>
#include "telnet/TelnetController.h"
#include "display/StatusDisplay.h"

// #define DEV
// #define SERIAL_LOG

#define DEVICE_JSON_POPULATE_FUN std::function<String(JsonObject& device)>
#define DEVICE_JSON_UPDATE_FUN std::function<String(JsonObject& device)>

#define BASE_PATH "/api/device/"
#define MQTT_BASE_PATH "/device/"
#define MQTT_REQUEST_PATH "/device/request/"
#define MQTT_INFO_PATH "/device/info/"
#define MQTT_CONFIG_FILE_PATH "/mqttConfig.json"
#define CONFIG_FILE_PATH "/config.json"
#define MQTT_UPDATE_TIME 7000
#define MODBUS_TCP_SLAVE_ID 42

// #### MQTT

// You can check on your device after a successful
// connection here: https://shiftr.io/try.

extern String mqttClientName;
extern String mqttHost;
extern String mqttUsername;
extern String mqttPassword;
extern String ssid;
extern String wifiPassword;
extern int mqttQOS;
extern int mqttTimeout;
extern bool lastConnectionFailedMQTT;

void setupConfig();
void setupMQTTCongfig();
bool loadMQTTConfig();
bool saveMQTTConfiguration(const char* mqttClientName,
                            const char* mqttHost,
                            const char* mqttUser,
                            const char* mqttPassword,
                            int mqttQOS,
                            int mqttTimeout);

void resetConfiguration();

#endif
