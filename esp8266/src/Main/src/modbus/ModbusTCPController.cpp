#include "ModbusTCPController.h"

void ModbusTCPController::begin() {
    slaveTCP.cbVector[CB_WRITE_COIL] = writeDigitlOut;
    slaveTCP.cbVector[CB_READ_COILS] = readDigitalIn;
    slaveTCP.cbVector[CB_READ_REGISTERS] = readAnalogIn;

    slaveTCP.cbVector[CB_WRITE_MULTIPLE_REGISTERS] = writeRegisters;

    slaveTCP.begin();
}

void ModbusTCPController::handle() {
    slaveTCP.poll();
}
//fc 05 - write coil
void writeDigitlOut(uint8_t fc, uint16_t address, uint16_t status) {
    int errorCode = slaveRTU.writeSingleCoil(address, status);
    if (errorCode != 0)
        logModbusTCPError(errorCode);
}
//fc 01 - read coil
//   02 - read input
void readDigitalIn(uint8_t fc, uint16_t address, uint16_t length) {
    int errorCode;
    if (fc == 01)
        errorCode = slaveRTU.readCoils(address, length);
    if (fc == 02)
        errorCode = slaveRTU.readDiscreteInputs(address, length);
    if (errorCode != 0) {
        logModbusTCPError(errorCode);
        return;
    }
    for (int i = 0; i < length; i++) {
        slaveTCP.writeCoilToBuffer(i, slaveRTU.getResponseBuffer(i));
    }
    // slaveRTU.clearResponseBuffer();
    // slaveRTU.clearTransmitBuffer();
}
//fc 03 - read holding reg
//   04 - read input reg
void readAnalogIn(uint8_t fc, uint16_t address, uint16_t length) {
    int errorCode;
    if (fc == 03)
        errorCode = slaveRTU.readHoldingRegisters(address, length);
    if (fc == 04)
        errorCode = slaveRTU.readInputRegisters(address, length);
    if (errorCode != 0) {
        logModbusTCPError(errorCode);
        return;
    }
    for (int i = 0; i < length; i++) {
        slaveTCP.writeRegisterToBuffer(i, slaveRTU.getResponseBuffer(i));
    }
    // slaveRTU.clearResponseBuffer();
    // slaveRTU.clearTransmitBuffer();
}

/**
 * Handel Write Holding Registers (FC=16)
 */
void writeRegisters(uint8_t fc, uint16_t address, uint16_t length) {
    int errorCode;
    for (int i = 0; i < length; i++) {
        slaveRTU.setTransmitBuffer(i, slaveTCP.readRegisterFromBuffer(i));
    }
    errorCode = slaveRTU.writeMultipleRegisters(address, length);
    if (errorCode != 0)
        logModbusTCPError(errorCode);
    // slaveRTU.clearResponseBuffer();
    // slaveRTU.clearTransmitBuffer();
}

void logModbusTCPError(const uint8_t code) {
    switch (code) {
        case 0:
            ERROR("It's not ERROR o_0");
            break;
        case 0x01:
            ERROR("[ModbusTCP] illegal function [ku8MBIllegalFunction]");
            statusDisplay.setErrorCode(401);
            break;
        case 0x02:
            ERROR("[ModbusTCP] illegal data address [ku8MBIllegalDataAddress]");
            statusDisplay.setErrorCode(402);
            break;
        case 0x03:
            ERROR("[ModbusTCP] illegal data value [ku8MBIllegalDataValue]");
            statusDisplay.setErrorCode(403);
            break;
        case 0x04:
            ERROR("[ModbusTCP] slave device failure [ku8MBSlaveDeviceFailure]");
            statusDisplay.setErrorCode(404);
            break;
        case 0xE0:
            ERROR("[ModbusTCP] invalid slave id [ku8MBInvalidSlaveID]");
            statusDisplay.setErrorCode(405);
            break;
        case 0xE1:
            ERROR("[ModbusTCP] invalid function [ku8MBInvalidFunction]");
            statusDisplay.setErrorCode(406);
            break;
        case 0xE2:
            ERROR("[ModbusTCP] response timeout [ku8MBResponseTimedOut]");
            statusDisplay.setErrorCode(407);
            break;
        case 0xE3:
            ERROR("[ModbusTCP] invalid CRC [ku8MBInvalidCRC]");
            statusDisplay.setErrorCode(408);
            break;
        default:
            ERROR("[ModbusTCP] is too high, please try later");
            break;
    }
}

ModbusTCP slaveTCP(MODBUS_TCP_SLAVE_ID);