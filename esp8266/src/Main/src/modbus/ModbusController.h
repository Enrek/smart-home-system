//ModbusController.h

#ifndef MODBUS_CONTROLLER
  #define MODBUS_CONTROLLER
  
  #include "Arduino.h"
  #include <stdint.h>
  #include "ModbusService.h"
  #include "ModbusMap.h"
  #include <ArduinoJson.h>

  void updateStorage();
  
  // SENSOR
  
  String populateTempSensor(JsonObject& device);
  String populateCO2Sensor(JsonObject& device);
  String populateSolarSensor(JsonObject& device);
  String populateTime(JsonObject& device);
  
  String populateFirstFloorPIRSensor(JsonObject& device);
  String populateSecondFloorPIRSensor(JsonObject& device);
  
  // MODULE
  
  String populateFanModule(JsonObject& device);
  String updateFanModule(JsonObject& device);
  String populateFireModule(JsonObject& device);
  String updateFireModule(JsonObject& device);
  String populateSecurityModule(JsonObject& device);
  String updateSecurityModule(JsonObject& device);
  String populateConditionerModule(JsonObject& device);
  String updateConditionerModule(JsonObject& device);
  String populateHeatingModule(JsonObject& device);
  String updateHeatingModule(JsonObject& device);
  String populateSocketModule(JsonObject& device);
  String updateSocketModule(JsonObject& device);
  
  // LIGHT
  String populateSunbling(JsonObject& device);
  String updateSunblind(JsonObject& device);
  String populateLED(JsonObject& device);
  String updateLED(JsonObject& device);


#endif
