//ModbusTCPController.h

#ifndef MODBUS_TCP_CONTROLLER_H
#define MODBUS_TCP_CONTROLLER_H

#include <ModbusSlaveTCP.h>
#include "Arduino.h"
#include "ModbusService.h"
#include "SmartHouseConfiguration.h"
#include "display/StatusDisplay.h"

class ModbusTCPController {
   public:
    void begin();
    void handle();
};

extern ModbusTCP slaveTCP;

void writeDigitlOut(uint8_t fc, uint16_t address, uint16_t status);
void readDigitalIn(uint8_t fc, uint16_t address, uint16_t length);
void readAnalogIn(uint8_t fc, uint16_t address, uint16_t length);
void writeRegisters(uint8_t fc, uint16_t address, uint16_t length);
void logModbusTCPError(const uint8_t code);
#endif