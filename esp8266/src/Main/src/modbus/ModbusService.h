//ModbusService.h

#ifndef MODBUS_SERVICE
#define MODBUS_SERVICE

#include <ModbusMaster.h>
#include <stdint.h>
#include "Arduino.h"
#include "ModbusController.h"
#include "ModbusMap.h"
#include "SmartHouseConfiguration.h"
#include "display/StatusDisplay.h"

enum MODBUS_TYPE {
    ANALOG,
    COIL,
    DISCRETE,
    REGISTER
};

extern uint16_t lastReturnCode;

class ModbusEntity {
   public:
    ModbusEntity(MODBUS_TYPE type, uint8_t offset, int length, int updateInterval, uint16_t* storage) {
        this->offset = offset;
        this->updateInterval = updateInterval;
        this->type = type;
        this->offset = offset;
        this->length = length;
        this->storage = storage;
    }
    void clearTime() {
        updateTime = 0;
    }
    uint16_t* getValue(uint8_t reg, int registersCount) {
        update();
        uint16_t result[length];
        if ((reg + registersCount) > offset + length) {
            ERROR("[ModbusRTU] Register out of bounds exception");
            return result;
        }
        for (int i = 0; i < registersCount; i++) {
            result[i] = storage[reg + i];
        }
        return result;
    }
    bool update();

   private:
    unsigned long updateTime;
    uint16_t* storage;
    int updateInterval;
    int length;
    uint8_t offset;
    MODBUS_TYPE type;
    bool processResult(uint16_t errorCode);
};

extern ModbusEntity coilStorage;
extern ModbusEntity analogStorage;
extern ModbusEntity discreteStorage;
extern ModbusEntity registerStorage;
extern bool lastRTUError;

void testCheckModbus();

void initModbus();
void logModbusRTUError(const uint8_t code);

uint16_t readAnalogInput(uint8_t deviceRegister);
bool readDiscreteCoils(uint8_t deviceRegister);
String writeDiscreteoCoils(uint8_t deviceRegister, uint8_t value);
bool readDiscreteInput(uint8_t deviceRegister);
uint16_t readHoldingRegister(uint8_t deviceRegister);
uint16_t* readHoldingRegister(uint8_t deviceRegister, int length);
String writeHoldingRegister(uint8_t deviceRegister, uint8_t value);
String writeHoldingRegister(uint8_t deviceRegister, uint8_t* values, int length);
uint16_t processResult(uint8_t deviceRegister, uint16_t errorCode, uint16_t* cache);
uint16_t* processResult(uint8_t deviceRegister, uint16_t errorCode, uint16_t* cache, int length);

extern ModbusMaster slaveRTU;

#endif
