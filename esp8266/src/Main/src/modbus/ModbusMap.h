//ModbusMap.h

#ifndef MODBUS_MAP
#define MODBUS_MAP

#include <stdint.h>
#include "ModbusService.h"

const uint8_t SOLAR_REGISTER = 0;
const uint8_t SUNBLIND_CURRENT = 1;
const uint8_t TEMP_REGISTER = 2;
const uint8_t CO2_REGISTER = 3;
const uint8_t TIME_REGISTER = 4;

extern uint16_t analogCache[5];

const uint8_t FAN_COIL = 0;
const uint8_t LED_MODE_COIL = 1;
const uint8_t HEATING_COIL = 2;
const uint8_t CONDITIONER_COIL = 3;
const uint8_t SOCKET_COIL = 4;
const uint8_t SECURITY_COIL = 5;
const uint8_t FIRE_COIL = 6;

extern uint16_t coilCache[7];

const uint8_t PIR_FIRST_FLOOR_DI = 0;
const uint8_t PIR_SECOND_FLOOR_DI = 1;
const uint8_t SECURITY_ALARM_DI = 2;
const uint8_t FIRE_ALARM_DI = 3;

extern uint16_t discreteCache[4];

const uint8_t SUNBLIND_HR = 0;
const uint8_t LED_OFFSET_HR = 1;
const uint8_t LED_SECTOR_COUNT = 4;

extern uint16_t holdingRegisterCache[2 + 3 * LED_SECTOR_COUNT];


#endif
