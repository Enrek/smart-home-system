#include "ModbusController.h"

// #### Controller 


int updateIndex = 0;

ModbusEntity storages[4] = {discreteStorage, coilStorage, analogStorage, registerStorage};

void updateStorage() {
  while(!storages[updateIndex].update()) {
    if(++updateIndex == 4) {
      updateIndex = 0;
    }
  }
}

// Sensor

String populateTempSensor(JsonObject& device) {
  device["temperature"] = readAnalogInput(TEMP_REGISTER);
  return "";
}

String populateCO2Sensor(JsonObject& device) {
  device["concentration"] = readAnalogInput(CO2_REGISTER);
  return "";
}

String populateSolarSensor(JsonObject& device) {
  device["voltage"] = readAnalogInput(SOLAR_REGISTER);
  return "";
}

String populateTime(JsonObject& device) {
   device["time"] = readAnalogInput(TIME_REGISTER);
   return "";
}

String populateFirstFloorPIRSensor(JsonObject& device) {
  device["movement"] = readDiscreteInput(PIR_FIRST_FLOOR_DI);
  return "";
}

String populateSecondFloorPIRSensor(JsonObject& device) {
  device["movement"] = readDiscreteInput(PIR_SECOND_FLOOR_DI);
  return "";
}

// Modules

String populateFanModule(JsonObject& device) {
  device["active"] = readDiscreteCoils(FAN_COIL);
  return "";
}

String updateFanModule(JsonObject& device) {
  writeDiscreteoCoils(FAN_COIL,device["active"]);
  return "";
}

String populateFireModule(JsonObject& device) {
  device["active"] = readDiscreteCoils(FIRE_COIL);
  device["alarm"] = readDiscreteInput(FIRE_ALARM_DI);
  return "";
}

String updateFireModule(JsonObject& device) {
  writeDiscreteoCoils(FIRE_COIL,device["active"]);
  return "";
}

String populateSecurityModule(JsonObject& device) {
  device["active"] = readDiscreteCoils(SECURITY_COIL);
  device["alarm"] = readDiscreteInput(SECURITY_ALARM_DI);;
  return "";
}

String updateSecurityModule(JsonObject& device) {
  writeDiscreteoCoils(SECURITY_COIL,device["active"]);
  return "";
}

String populateConditionerModule(JsonObject& device) {
  device["active"] = readDiscreteCoils(CONDITIONER_COIL);
  return "";
}

String updateConditionerModule(JsonObject& device) {
  writeDiscreteoCoils(CONDITIONER_COIL, device["active"]);
  return "";
}

String populateHeatingModule(JsonObject& device) {
  device["active"] = readDiscreteCoils(HEATING_COIL);
  return "";
}

String updateHeatingModule(JsonObject& device) {
  writeDiscreteoCoils(HEATING_COIL, device["active"]);
  return "";
}

String populateSocketModule(JsonObject& device) {
  device["active"] = readDiscreteCoils(SOCKET_COIL);
  return "";
}

String updateSocketModule(JsonObject& device) {
  writeDiscreteoCoils(SOCKET_COIL, device["active"]);
  return "";
}

// LIGHT
String populateSunbling(JsonObject& device) {
  device["targetPosition"] = readHoldingRegister(SUNBLIND_HR);
  return "";
}

String updateSunblind(JsonObject& device) {
  writeHoldingRegister(SUNBLIND_HR,device["targetPosition"]);
  return "";
}

String populateLED(JsonObject& device) {
  JsonArray& ledItems = device.createNestedArray("ledItems");
  for(int i=0; i < LED_SECTOR_COUNT; i++) {
    JsonObject& ledItem = ledItems.createNestedObject();
    ledItem["sector"] = i;
    JsonObject& color = ledItem.createNestedObject("color");
    uint16_t* colorReg = readHoldingRegister(i*3+LED_OFFSET_HR,3);
    color["red"] = colorReg[0];
    color["green"] = colorReg[1];
    color["blue"] = colorReg[2];
  }
 
  return "";
}

String updateLED(JsonObject& device) {   
  int size = device["ledItems"].size();
  for(int i = 0; i < size; i++) {
    int sector = device["ledItems"][i]["sector"];
    uint8_t color[] = {device["ledItems"][i]["color"]["red"], device["ledItems"][i]["color"]["green"], device["ledItems"][i]["color"]["blue"]};
    writeHoldingRegister(sector*3+LED_OFFSET_HR,color,3);
  }
  return "";
}


