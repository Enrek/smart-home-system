#include "ModbusService.h"

// #### Modbus

ModbusMaster slaveRTU;

// void testCheckModbus() {
//   int code = 0;
//   slaveRTU.clearResponseBuffer();
//   DEBUG("GET ALL COILS [BATCH]");
//   code = slaveRTU.readCoils(0, 7);
//   if (code != 0) {
//     logModbusError(code);
//   } else {
//     for (int i = 0; i < 7; i++) {
//       DEBUG("COIL " + String(i) + " -----> " + String(slaveRTU.getResponseBuffer(i)));
//     }
//   }
//   slaveRTU.clearResponseBuffer();
//   DEBUG("GET ALL COILS");

//   for (int i = 0; i < 7; i++) {
//     code = slaveRTU.readCoils(i, 1);
//     if (code != 0) {
//       ERROR("Error code " + String(code));
//     } else {
//       DEBUG("COIL " + String(i) + " -----> " + String(slaveRTU.getResponseBuffer(0)));
//     }
//     slaveRTU.clearResponseBuffer();
//   }
//   DEBUG("WRITE COILS");
//   for (int i = 0; i < 6; i++) {
//     code =  slaveRTU.writeSingleCoil(i, 1);
//     if (code != 0) {
//       ERROR("Error code " + String(code));
//     }
//   }
// }

uint16_t lastReturnCode = 0;
ModbusEntity coilStorage = ModbusEntity(COIL, 0, 7, 15000, coilCache);
ModbusEntity analogStorage = ModbusEntity(ANALOG, 0, 5, 8000, analogCache);
ModbusEntity discreteStorage = ModbusEntity(DISCRETE, 0, 4, 2000, discreteCache);
ModbusEntity registerStorage = ModbusEntity(REGISTER, 0, 2 + 3 * LED_SECTOR_COUNT, 20000, holdingRegisterCache);

bool lastRTUError = false;


void logModbusRTUError(const uint8_t code) {
    switch (code) {
        case 0:
            ERROR("It's not ERROR o_0");
            break;
        case 0x01:
            ERROR("[ModbusRTU] illegal function [ku8MBIllegalFunction]");
            statusDisplay.setErrorCode(301);
            break;
        case 0x02:
            ERROR("[ModbusRTU] illegal data address [ku8MBIllegalDataAddress]");
            statusDisplay.setErrorCode(302);
            break;
        case 0x03:
            ERROR("[ModbusRTU] illegal data value [ku8MBIllegalDataValue]");
            statusDisplay.setErrorCode(303);
            break;
        case 0x04:
            ERROR("[ModbusRTU] slave device failure [ku8MBSlaveDeviceFailure]");
            statusDisplay.setErrorCode(304);
            break;
        case 0xE0:
            ERROR("[ModbusRTU] invalid slave id [ku8MBInvalidSlaveID]");
            statusDisplay.setErrorCode(305);
            break;
        case 0xE1:
            ERROR("[ModbusRTU] invalid function [ku8MBInvalidFunction]");
            statusDisplay.setErrorCode(306);
            break;
        case 0xE2:
            ERROR("[ModbusRTU] response timeout [ku8MBResponseTimedOut]");
            statusDisplay.setErrorCode(307);
            break;
        case 0xE3:
            ERROR("[ModbusRTU] invalid CRC [ku8MBInvalidCRC]");
            statusDisplay.setErrorCode(308);
            break;
        default:
            ERROR("[ModbusRTU] is too high, please try later");
            break;
    }
}

bool ModbusEntity::processResult(uint16_t errorCode) {
    lastReturnCode = errorCode;
    if (errorCode == 0) {
        for (int i = 0; i < length; i++) {
            storage[i] = slaveRTU.getResponseBuffer(i);
        }
        slaveRTU.clearResponseBuffer();
        return true;
    }
    slaveRTU.clearResponseBuffer();
    return false;
}

bool ModbusEntity::update() {
    uint16_t errorCode;
    if (millis() - updateTime > updateInterval) {
        DEBUG("[ModbusRTU] Tring to update modbus storage.");
        bool updateResult = false;
        switch (type) {
            case (ANALOG):
                DEBUG("[ModbusRTU] Analog table modbus update");
                errorCode = slaveRTU.readInputRegisters(offset, length);
                 if (errorCode != 0)
                        logModbusRTUError(errorCode);
                updateResult = processResult(errorCode);
                break;
            case (COIL):
                DEBUG("Coil table modbus update");
                updateResult = true;
                for (int i = offset; i < length; i++) {
                    errorCode = slaveRTU.readCoils(i, 1);
                    if (errorCode != 0) {
                        logModbusRTUError(errorCode);
                        updateResult = false;
                    } else {
                        storage[i] = slaveRTU.getResponseBuffer(0);
                    }
                    slaveRTU.clearResponseBuffer();
                }
                break;
            case (DISCRETE):
                DEBUG("[ModbusRTU] Discrete table modbus update");
                updateResult = true;
                for (int i = offset; i < length; i++) {
                    errorCode = slaveRTU.readDiscreteInputs(i, 1);
                    if (errorCode != 0) {
                        logModbusRTUError(errorCode);
                        updateResult = false;
                    } else {
                        storage[i] = slaveRTU.getResponseBuffer(0);
                    }
                    slaveRTU.clearResponseBuffer();
                }
                //errorCode = slaveRTU.readDiscreteInputs(offset, length);
                //updateResult = processResult(errorCode);
                break;
            case (REGISTER):
                DEBUG("[ModbusRTU] Register table modbus update");
                errorCode = slaveRTU.readHoldingRegisters(offset, length);
                updateResult = processResult(errorCode);
                break;
        }
        if (updateResult) {
            updateTime = millis();
            DEBUG("[ModbusRTU] Modbus storage update - done!");
            return true;
        } else {
            ERROR("[ModbusRTU] Error during modbus update ");
            logModbusRTUError(lastReturnCode);
            return false;
        }
    }
    DEBUG("[ModbusRTU] Modbus storage is up to date");
    return false;
}

void initModbus() {
    slaveRTU.begin(1, Serial);
}

uint16_t readAnalogInput(uint8_t deviceRegister) {
    uint16_t errorCode = slaveRTU.readInputRegisters(deviceRegister, 1);
    if (errorCode != 0) {
        lastRTUError = true;
        logModbusRTUError(errorCode);
    } else {
        lastRTUError = false;
    }
    return slaveRTU.getResponseBuffer(0);
    //   return analogStorage.getValue(deviceRegister, 1)[0];
}

bool readDiscreteCoils(uint8_t deviceRegister) {
    uint16_t errorCode = slaveRTU.readCoils(deviceRegister, 1);
    if (errorCode != 0) {
        lastRTUError = true;
        logModbusRTUError(errorCode);
    } else {
        lastRTUError = false;
    }
    return slaveRTU.getResponseBuffer(0);
    //   return coilStorage.getValue(deviceRegister, 1)[0];//processResult(deviceRegister, errorCode, 1);
}

bool readDiscreteInput(uint8_t deviceRegister) {
    uint16_t errorCode = slaveRTU.readDiscreteInputs(deviceRegister, 1);
    if (errorCode != 0) {
        lastRTUError = true;
        logModbusRTUError(errorCode);
    } else {
        lastRTUError = false;
    }
    return slaveRTU.getResponseBuffer(0);
    //   return discreteStorage.getValue(deviceRegister, 1)[0];
}

uint16_t readHoldingRegister(uint8_t deviceRegister) {
    uint16_t errorCode = slaveRTU.readHoldingRegisters(deviceRegister, 1);
    if (errorCode != 0) {
        lastRTUError = true;
        logModbusRTUError(errorCode);
    } else {
        lastRTUError = false;
    }
    return slaveRTU.getResponseBuffer(0);
    //   return registerStorage.getValue(deviceRegister, 1)[0];
}

uint16_t* readHoldingRegister(uint8_t deviceRegister, int length) {
    uint16_t errorCode = slaveRTU.readHoldingRegisters(deviceRegister, length);
    if (errorCode != 0) {
        lastRTUError = true;
        logModbusRTUError(errorCode);
    } else {
        lastRTUError = false;
    }
    uint16_t result[length];
    for (int i = 0; i < length; i++) {
        result[i] = slaveRTU.getResponseBuffer(i);
    }
    return result;
    //   return registerStorage.getValue(deviceRegister, length);
}

String writeDiscreteoCoils(uint8_t deviceRegister, uint8_t value) {
    uint16_t errorCode = slaveRTU.writeSingleCoil(deviceRegister, value);
    slaveRTU.clearResponseBuffer();
    //   coilStorage.clearTime();
    return "";
}

String writeHoldingRegister(uint8_t deviceRegister, uint8_t value) {
    slaveRTU.setTransmitBuffer(0, value);
    uint16_t errorCode = slaveRTU.writeMultipleRegisters(deviceRegister, 1);
    slaveRTU.clearResponseBuffer();
    //   registerStorage.clearTime();
    return "";
}

String writeHoldingRegister(uint8_t deviceRegister, uint8_t* values, int length) {
    for (int i = 0; i < length; i++) {
        slaveRTU.setTransmitBuffer(i, values[i]);
    }
    uint16_t errorCode = slaveRTU.writeMultipleRegisters(deviceRegister, length);
    slaveRTU.clearResponseBuffer();
    //   registerStorage.clearTime();
    return "";
}