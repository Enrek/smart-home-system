// #include "ModbusController_DEV.h"

// // #### Controller 

// // Sensor

// String populateTempSensor(JsonObject& device) {
//  device["temperature"] = random(40);
//  return "";
// }

// String populateCO2Sensor(JsonObject& device) {
//  device["concentration"] = random(100);
//  return "";
// }

// String populateSolarSensor(JsonObject& device) {
//  device["voltage"] = random(30);
//  return "";
// }

// String populateFirstFloorPIRSensor(JsonObject& device) {
//  device["movement"] = random(1) == 0;
//  return "";
// }

// String populateSecondFloorPIRSensor(JsonObject& device) {
//  device["movement"] = random(1) == 0;
//  return "";
// }

// // Modules
// bool fan = false;
// bool fire = true;
// bool fireAlarm = false;
// bool security = false;
// bool securityAlarm = false;
// bool conditioner = false;
// bool heating = false;
// bool socket = false;

// int postion = 0;

// bool led[4];

// String populateFanModule(JsonObject& device) {
//  device["active"] = fan;
//  return "";
// }

// String updateFanModule(JsonObject& device) {
//  fan = device["active"];
//  return "";
// }

// String populateFireModule(JsonObject& device) {
//  device["active"] = fire;
//  device["alarm"] = fireAlarm;
//  return "";
// }

// String updateFireModule(JsonObject& device) {
//  fire = device["active"];
//  return "";
// }

// String populateSecurityModule(JsonObject& device) {
//  device["active"] = security;
//  device["alarm"] = securityAlarm;
//  return "";
// }

// String updateSecurityModule(JsonObject& device) {
//  security = device["active"];
//  return "";
// }

// String populateConditionerModule(JsonObject& device) {
//  device["active"] = conditioner;
//  return "";
// }

// String updateConditionerModule(JsonObject& device) {
//  conditioner = device["active"];
//  return "";
// }

// String populateHeatingModule(JsonObject& device) {
//  device["active"] = heating;
//  return "";
// }

// String updateHeatingModule(JsonObject& device) {
//  heating = device["active"];
//  return "";
// }

// String populateSocketModule(JsonObject& device) {
//  device["active"] = socket;
//  return "";
// }

// String updateSocketModule(JsonObject& device) {
//  socket = device["active"];
//  return "";
// }

// // LIGHT
// String populateSunbling(JsonObject& device) {
//  device["targetPosition"] = postion;
//  return "";
// }

// String updateSunblind(JsonObject& device) {
//  postion = device["targetPosition"];
//  return "";
// }

// String populateLED(JsonObject& device) {
//  JsonArray& ledItems = device.createNestedArray("ledItems");
//  for(int i=0; i < LED_SECTOR_COUNT; i++) {
//    JsonObject& ledItem = ledItems.createNestedObject();
//    ledItem["sector"] = i;
//    JsonObject& color = ledItem.createNestedObject("color");
//    uint16_t colorReg = led[i] ? 255 : 0;
//    color["red"] = colorReg;
//    color["green"] = colorReg;
//    color["blue"] = colorReg;
//  }

//  return "";
// }

// String updateLED(JsonObject& device) {   
//  int sector = device["ledItems"][0]["sector"];
//  led[sector] = device["ledItems"][0]["color"]["red"] != 0;
//  return "";
// }

// String populateTime(JsonObject& device) {
//    device["time"] =  random(24);
//    return "";
// }