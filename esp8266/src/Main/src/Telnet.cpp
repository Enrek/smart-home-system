// #include "Telnet.h"

// WiFiServer tcpTraceServer(23);

// void DEBUG(String msg) {
// #ifdef SERIAL_LOG
//   Serial.print("[DEBUG] ");
//   Serial.println(msg);
// #endif
//   telnet.print("[DEBUG] ");
//   telnet.println(msg);
// }

// void ERROR(String msg) {
// #ifdef SERIAL_LOG
//   Serial.print("[ERROR] ");
//   Serial.println(msg);
// #endif
//   telnet.print("[ERROR] ");
//   telnet.println(msg);
// }

// /******************************************************************************/
// void Telnet::begin()
// {
//   tcpTraceServer.begin();
//   menuState = 0;
// }
// /******************************************************************************/
// void Telnet::task(void)
// {
//   if (tcpTraceServer.hasClient())
//   {
//     //if (!tcpTraceClient || !tcpTraceClient.connected())
//     {
//       if(tcpTraceClient) tcpTraceClient.stop();
//       tcpTraceClient = tcpTraceServer.available();
//       println("Hello!");
//     }
//   }
//   if (tcpTraceClient)
//   {
//      menu();
//   }
// }
// /******************************************************************************/
// size_t Telnet::write(uint8_t c)
// {
//   if (tcpTraceClient)
//   {
//      return tcpTraceClient.write(c);
//   }
//   return 0;
// }
// /******************************************************************************/
// size_t  Telnet::write(const uint8_t *buf, size_t size)
// {
//   if (tcpTraceClient)
//   {
//      return tcpTraceClient.write(buf, size);
//      tcpTraceClient.flush();
//   }
//   return 0;
// }
// /******************************************************************************/
// int Telnet::available(void)
// {
//   if (tcpTraceClient)
//   {
//     return tcpTraceClient.available();
//   }
//   else
//     return 0;
// }
// /******************************************************************************/
// int Telnet::peek(void)
// {
//   if (tcpTraceClient)
//   {
//     return tcpTraceClient.peek();
//   }
//   else
//     return 0;
// }

// /******************************************************************************/
// int Telnet::read(void)
// {
//   if (tcpTraceClient)
//   {
//     return tcpTraceClient.read();
//   }
//   else
//     return 0;
// }
// /******************************************************************************/
// void Telnet::flush()
// {
//   if (tcpTraceClient)
//   {
//     tcpTraceClient.flush();
//   }
// }

// /******************************************************************************/
// void Telnet::menu()
// {
//   static String dataIn = "";
//   static String dataInChar = "";
//   switch (menuState)
//   {
//     case 1:
//       println("********************************************************************************");
//       println("* My DHCP IP address:      ");
//       println(WiFi.localIP());
//       print("* (M) MQTT host:           ");
//       println(String(mqttHost));
//       println("* (D) Delete config file(!)");
//       println("* (S) Save config!");
//       println("* (R) Restart");
      
//       menuState = 2;
//       break;
//     case 2:
//     {
//         while( available())
//         {
//           dataInChar = readString();
//           print(dataInChar);
//           dataIn += dataInChar;
//         }
//         int16_t len = dataIn.indexOf ("\r\n");
//         if (len != -1)
//         {
//           println("");
//           uint16_t comm = dataIn.charAt(0);
//           switch (comm)
//           {
//             case 'M': { menuState = 20; print("Enter new MQTT host: "); break; }
//             case 'R': { ESP.restart(); break;}
//             case 'S':
//               menuState = 1;
//               println("Saving config...");
              
//     saveMQTTConfiguration(mqttClientName, mqttHost, mqttUsername, mqttPassword, mqttQOS, mqttTimeout);
//               break;
//             case 'D': 
//               menuState = 1;
//               resetConfiguration();
//               break;
//             default:
//               println("No command!!!" );
//               flush();
//               print(comm, HEX);
//           }
//           dataIn = "";
//         }
//     }

//     case 20:
//     {
//      while( available())
//       {
//         dataInChar = readString();
//         print(dataInChar);
//         dataIn += dataInChar;
//       }
//       int16_t len = dataIn.indexOf ("\r\n");
//       if (len != -1)
//       {
//         mqttHost = dataIn.substring(0, len).c_str();
//         mqttUsername = "";
//         mqttPassword = "";
//         dataIn = "";
//         menuState = 1;
//       }

//       break;
//     }
//     default:
//       menuState = 1;
//   }
// }

// Telnet telnet;
