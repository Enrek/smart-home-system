#include "SmartHouseHTTPServer.h"

void SmartHouseHTTPServer::handle() {
  server.handleClient();
}

void SmartHouseHTTPServer::begin() {
  initResources();
  initAPI();
  server.begin();
  DEBUG("HTTP Server initalization done!");
}

void SmartHouseHTTPServer::initResources() {

  server.on("/dashboard", HTTP_GET, [this]() {
    server.sendHeader("Location", String("/"), true);
    server.send ( 302, "text/plain", "");
  });

  server.on("/sensor/temp", HTTP_GET, [this]() {
    server.sendHeader("Location", String("/"), true);
    server.send ( 302, "text/plain", "");
  });

  server.on("/sensor/co2", HTTP_GET, [this]() {
    server.sendHeader("Location", String("/"), true);
    server.send ( 302, "text/plain", "");
  });

  server.on("/api/mgmt/initiate/device/reboot", HTTP_POST, [this]() {
    DEBUG("Bye my friend! Hope to see you soon!");
    ESP.restart();
  });

  server.on("/api/mgmt/initiate/device/reset", HTTP_POST, [this]() {
    DEBUG("Bye my friend! Hope to s!@#e12$!@$!@e yo@#ru s14$&%Sr@$!");
    resetConfiguration();
    DEBUG("Who's there?!");
    ESP.restart();
  });

  server.on("/api/device/configuration/mqtt", HTTP_GET, [this]() {
    if (!handleFileRead(MQTT_CONFIG_FILE_PATH)) {
      ERROR("MQTT configuration file doesn't exist!");
      server.send(404, "text/plain", "404 File Not Found");
    }
  });

  server.on("/api/device/system", HTTP_GET, [this]() {
    StaticJsonBuffer<400> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    JsonObject& device = root.createNestedObject("d");
    device["freeHeap"] = ESP.getFreeHeap();
    device["flashChipSize"] = ESP.getFlashChipSize();
    device["sketchSize"] = ESP.getSketchSize();
    device["freeSketchSpace"] = ESP.getFreeSketchSpace();
    device["vcc"] = ESP.getVcc();
    device["lastReset"] = ESP.getResetReason();
    device["flashChipSpeed"] = ESP.getFlashChipSpeed();
    device["mqttConnected"] = !lastConnectionFailedMQTT;
    sendJSON(root);
  });

  server.on("/api/device/configuration/mqtt", HTTP_POST, [this]() {
    DynamicJsonBuffer jsonBuffer(400);
    JsonObject& root = jsonBuffer.parseObject(server.arg("plain"));
    JsonObject& device = root["d"];
    if (saveMQTTConfiguration(device["clientName"], device["host"], device["username"], device["password"], device["qos"], device["timeout"])) {
      server.send(201, "text/plain", "");
      ESP.restart();
    } else {
      server.send(503, "text/plain", "");
    }
  });
  corsEnable("/api/device/configuration/mqtt");

  server.onNotFound([this]() {
    DEBUG("Trying to find file in FS");
    server.sendHeader("Connection", "close");
    server.sendHeader("Access-Control-Allow-Origin", "*");
    if (!handleFileRead(server.uri())) {
      ERROR("404 =(");
      server.send(404, "text/plain", "404 File Not Found");
    }
  });
}

void SmartHouseHTTPServer::initAPI() {
  DEBUG("Init API for HTTP server");
  setupEndpoints(
    "sensor",
    sensorPopulators,
    sensorDevices,
    sizeof(sensorPopulators) / sizeof(DEVICE_JSON_POPULATE_FUN)
  );
  setupEndpoints(
    "module",
    modulePopulators,
    moduleUpdaters, moduleDevices,
    sizeof(modulePopulators) / sizeof(DEVICE_JSON_POPULATE_FUN),
    sizeof(moduleUpdaters) / sizeof(DEVICE_JSON_UPDATE_FUN)
  );
  setupEndpoints(
    "light",
    lightPopulators,
    lightUpdaters,
    lightDevices,
    sizeof(lightPopulators) / sizeof(DEVICE_JSON_POPULATE_FUN),
    sizeof(lightUpdaters) / sizeof(DEVICE_JSON_UPDATE_FUN)
  );
  DEBUG("API initalization done!");
}

void SmartHouseHTTPServer::setupEndpoints(String deviceGroup, const DEVICE_JSON_POPULATE_FUN* populators, const String* devices, int populatorsCount) {
  setupEndpoints(deviceGroup, populators, NULL, devices, populatorsCount, 0);
}

void SmartHouseHTTPServer::setupEndpoints(String deviceGroup, const DEVICE_JSON_POPULATE_FUN* populators, const DEVICE_JSON_UPDATE_FUN* updaters, const String* devices, int populatorsCount, int updaterCount) {
  for (int i = 0; i < populatorsCount; i++) {
    String uri = BASE_PATH + deviceGroup + "/" + devices[i];
    DEVICE_JSON_POPULATE_FUN populator = populators[i];
    server.on(uri.c_str(), HTTP_GET, [populator, this]() {
      StaticJsonBuffer<2000> jsonBuffer;
      JsonObject& root = jsonBuffer.createObject();
      JsonObject& device = root.createNestedObject("d");
      populator(device);
      this->sendJSON(root);
    });
  }
  for (int i = 0; i < updaterCount; i++) {
    String uri = BASE_PATH + deviceGroup + "/" + devices[i];
    DEVICE_JSON_POPULATE_FUN populator = populators[i];
    DEVICE_JSON_UPDATE_FUN updater = updaters[i];
    corsEnable(uri.c_str());
    server.on(uri.c_str(), HTTP_POST, [populator, updater, this]() {
      DynamicJsonBuffer rjsonBuffer(800);
      JsonObject& rroot = rjsonBuffer.parseObject(server.arg("plain"));
      if (rroot.success()) {
        updater(rroot["d"]);
      }

      StaticJsonBuffer<2000> jsonBuffer;
      JsonObject& root = jsonBuffer.createObject();
      JsonObject& device = root.createNestedObject("d");
      populator(device);
      this->sendJSON(root);
    });
  }
}


// #### Util

void SmartHouseHTTPServer::corsEnable(String url) {
  server.on(url, HTTP_OPTIONS, [this]() {
    server.sendHeader("Connection", "close");
    //      server.sendHeader("Access-Control-Allow-Origin", "*");
    server.sendHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
    server.sendHeader("Access-Control-Allow-Headers", "Origin, Content-Type, X-Auth-Token");
    server.send (200, "text/plain", "");
});
}


bool SmartHouseHTTPServer::handleFileRead(String path) {
  if (path.endsWith("/"))
    path += "index.html";
  String contentType = getContentType(path);
  String pathWithGz = path + ".gz";
  if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)) {
    if (SPIFFS.exists(pathWithGz))
      path += ".gz";
    File file = SPIFFS.open(path, "r");
    size_t sent = server.streamFile(file, contentType);
    file.close();
    return true;
  }
  return false;
}

String SmartHouseHTTPServer::getContentType(String filename) {
  if (server.hasArg("download")) return "application/octet-stream";
  else if (filename.endsWith(".htm")) return "text/html";
  else if (filename.endsWith(".html")) return "text/html";
  else if (filename.endsWith(".css")) return "text/css";
  else if (filename.endsWith(".js")) return "application/javascript";
  else if (filename.endsWith(".json")) return "application/json";
  else if (filename.endsWith(".png")) return "image/png";
  else if (filename.endsWith(".gif")) return "image/gif";
  else if (filename.endsWith(".jpg")) return "image/jpeg";
  else if (filename.endsWith(".ico")) return "image/x-icon";
  else if (filename.endsWith(".xml")) return "text/xml";
  else if (filename.endsWith(".pdf")) return "application/x-pdf";
  else if (filename.endsWith(".zip")) return "application/x-zip";
  else if (filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
}


void SmartHouseHTTPServer::sendJSON(JsonObject& root) {
  String json;
  root.prettyPrintTo(json);
  server.send(200, "text/json", json);
}

