  //SmartHouseHTTPServer.h

#ifndef SMART_HOUSE_HTTP_SERVER
#define SMART_HOUSE_HTTP_SERVER

#include "Arduino.h"
#include <stdint.h>
#include <ArduinoJson.h>
#include "../modbus/ModbusController.h"
#include "FS.h"
#include <ESP8266WebServer.h>
#include "../SmartHouseConfiguration.h"


class SmartHouseHTTPServer {
  public:
    void begin();
    void handle();
    // #### Util
    void corsEnable(String url);
    bool handleFileRead(String path);
    String getContentType(String filename);
  private:  
    
    ESP8266WebServer server;
  
    void initResources();
    void initAPI();
    void setupEndpoints(String deviceGroup, const DEVICE_JSON_POPULATE_FUN* populators, const String* devices, int populatorsCount);
    void setupEndpoints(String deviceGroup, const DEVICE_JSON_POPULATE_FUN* populators, const DEVICE_JSON_UPDATE_FUN* updaters, const String* devices, int populatorsCount, int updaterCount);
    void sendJSON(JsonObject& root);

    
    const String sensorDevices[6] = {"temp", "co2", "solar", "pir/0", "pir/1", "time"};
    const DEVICE_JSON_POPULATE_FUN sensorPopulators[6] = {populateTempSensor, populateCO2Sensor, populateSolarSensor, populateFirstFloorPIRSensor, populateSecondFloorPIRSensor, populateTime};

    const String moduleDevices[6] = {"fan", "fire", "security", "conditioner", "heating", "socket"};

    const DEVICE_JSON_POPULATE_FUN modulePopulators[6] = {populateFanModule, populateFireModule, populateSecurityModule, populateConditionerModule, populateHeatingModule, populateSocketModule};
    const DEVICE_JSON_UPDATE_FUN moduleUpdaters[6] = {updateFanModule, updateFireModule, updateSecurityModule, updateConditionerModule, updateHeatingModule, updateSocketModule};

    const String lightDevices[2] = {"sunblind", "led"};
    const DEVICE_JSON_POPULATE_FUN lightPopulators[2] = {populateSunbling, populateLED};
    const DEVICE_JSON_UPDATE_FUN lightUpdaters[2] = {updateSunblind, updateLED};
};


#endif

