// #ifndef TELNET_Ho
// #define TELNET_Ho

// #include <inttypes.h>
// #include "Stream.h"
// #include "Arduino.h"
// #include "SmartHouseConfiguration.h"
// #include <ESP8266WiFi.h>

// class Telnet : public Stream
// {
//   private:
//   WiFiClient tcpTraceClient;
//   uint16_t menuState = 1;

//   public:
//     void begin();
//     void task(void);
//     void menu(void);
//     virtual int available(void);
//     virtual void flush(void);
//     virtual int peek(void);
//     virtual int read(void);
//     virtual size_t write(uint8_t);
//     virtual size_t write(const uint8_t *buf, size_t size);
//     inline size_t write(unsigned long n) { return write((uint8_t)n); }
//     inline size_t write(long n) { return write((uint8_t)n); }
//     inline size_t write(unsigned int n) { return write((uint8_t)n); }
//     inline size_t write(int n) { return write((uint8_t)n); }
//     using Print::write; // pull in write(str) and write(buf, size) from Print
//     operator bool() { return true; }
// };

// void DEBUG(String msg);
// void ERROR(String msg);

// extern Telnet telnet;
// #endif