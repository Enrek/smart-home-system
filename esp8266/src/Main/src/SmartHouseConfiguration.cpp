#include "SmartHouseConfiguration.h"


// ### MQTT

// You can check on your device after a successful
// connection here: https://shiftr.io/try.
String mqttClientName;
String mqttHost;
String mqttUsername;
String mqttPassword;
int mqttQOS = 0;
int mqttTimeout = 5;
bool lastConnectionFailedMQTT = false;


void setupConfig() {
  setupMQTTCongfig();
}

void setupMQTTCongfig() {
  if (!SPIFFS.exists(MQTT_CONFIG_FILE_PATH)) {
    DEBUG("Create new config file");
    saveMQTTConfiguration("SmartHome",  "broker.shiftr.io",  "try", "try", mqttQOS, mqttTimeout);
  } else {
    loadMQTTConfig();
  }
}

bool loadMQTTConfig() {
  File configFile = SPIFFS.open(MQTT_CONFIG_FILE_PATH, "r");
  if (!configFile) {
    ERROR("Failed to open MQTT config file");
    statusDisplay.setErrorCode(101);
    return false;
  }

  size_t size = configFile.size();
  if (size > 1024) {
    ERROR("Config file size is too large");
    statusDisplay.setErrorCode(103);
    return false;
  }

  std::unique_ptr<char[]> buf(new char[size]);
  configFile.readBytes(buf.get(), size);
  StaticJsonBuffer<400> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(buf.get());

  if (!root.success()) {
    ERROR("Failed to parse MQTT config file");
    statusDisplay.setErrorCode(102);
    return false;
  }

  JsonObject& device = root["d"];
  mqttClientName = device["clientName"].as<String>();
  mqttHost = device["host"].as<String>();
  mqttUsername = device["username"].as<String>();
  mqttPassword = device["password"].as<String>();
  mqttQOS = device["qos"];
  mqttTimeout = device["timeout"];

  return true;
}

bool saveMQTTConfiguration(const char* mqttClientName,
                            const char* mqttHost,
                            const char* mqttUser,
                            const char* mqttPassword,
                            int mqttQOS,
                            int mqttTimeout) {
  DEBUG("Write settings to config file");
  StaticJsonBuffer<400> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonObject& device = root.createNestedObject("d");
  device["host"] = mqttHost;
  device["username"] = mqttUser;
  device["password"] = mqttPassword;
  device["clientName"] = mqttClientName;
  device["qos"] = mqttQOS;
  device["timeout"] = mqttTimeout;
  File configFile = SPIFFS.open(MQTT_CONFIG_FILE_PATH, "w");
  if (!configFile) {
    ERROR("Failed to open the config file for writing");
    statusDisplay.setErrorCode(104);
    return false;
  }

  root.printTo(configFile);
  return true;
}

void resetConfiguration() {
  statusDisplay.print("FACTORY RESET!");
  SPIFFS.remove(MQTT_CONFIG_FILE_PATH);
  setupMQTTCongfig();
  WiFi.disconnect(true);
  delay(300);
}