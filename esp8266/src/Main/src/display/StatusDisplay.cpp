
#include "StatusDisplay.h"

LiquidCrystal_I2C lcd(0x27, 16, 2);

void StatusDisplay::show() {
    if(millis() - lastUpdate < 2000)
        return;
    lastUpdate = millis();
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(ip);

    lcd.setCursor(0, 1);
    lcd.print("Last error");

    lcd.setCursor(11, 1);
    lcd.print(errorCode);
}
void StatusDisplay::init() {
    lcd.init();
    lcd.backlight();
}

void StatusDisplay::print(const char str[]) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(str);
}
StatusDisplay statusDisplay;
