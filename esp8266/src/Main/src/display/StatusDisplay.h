//StatusDisplay.h

#ifndef STATUS_DISPLAY
#define STATUS_DISPLAY

#include <LiquidCrystal_I2C.h>
#include "../SmartHouseConfiguration.h"
#include "Arduino.h"



class StatusDisplay {
   public:
    void setIP(String _ip) {
        lastUpdate = 0;
        ip = _ip;
    }
    void setErrorCode(int code) {
        lastUpdate = 0;
        errorCode = code;
    }
    void print(const char str[]);
    void init();
    void show();

   private:
    String ip;
    int errorCode;
    unsigned long lastUpdate;
};

extern StatusDisplay statusDisplay;

#endif