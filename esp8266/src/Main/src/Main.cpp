#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include "FS.h"
#include "display/StatusDisplay.h"
#include "http/SmartHouseHTTPServer.h"
#include "modbus/ModbusTCPController.h"
#include "mqtt/SmartHouseMQTTClient.h"
#include "telnet/TelnetController.h"
// #define SERIAL_LOG
// #define START_AS_AP
#define MQTT_PROTOCOL 0
#define MODBUS_TCP_PROTOCOL 1

SmartHouseMQTTClient smartHouseMQTTClient;
ModbusTCPController modbusTCPController;
SmartHouseHTTPServer smartHouseHTTPServer;

bool protocol = MODBUS_TCP_PROTOCOL;
bool httpServer = false;

void setup() {
    pinMode(D5, INPUT);
    pinMode(D6, INPUT);

    protocol = digitalRead(D5);
    httpServer = digitalRead(D6);

    Serial.begin(115200);

    SPIFFS.begin();
    statusDisplay.init();
    statusDisplay.print("Initializing...");
#ifdef START_AS_AP
    WiFi.mode(WIFI_AP);
    IPAddress apIP(192, 168, 0, 80);

    WiFi.softAPConfig(apIP, IPAddress(192, 168, 0, 1), IPAddress(255, 255, 255, 0));
    WiFi.softAP("SmartHouse", "smarthome");
#else
    IPAddress apIP(192, 168, 0, 80);
    IPAddress gateway(192, 168, 0, 1);
    IPAddress ip_dns(192, 168, 0, 1);
    WiFi.mode(WIFI_STA);
    WiFi.config(apIP, gateway, IPAddress(255, 255, 255, 0), ip_dns);
    WiFi.begin("DSL-2650U", "19760107");
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
    }
#endif

    statusDisplay.setIP(WiFi.localIP().toString().c_str());

    statusDisplay.print("Wi-Fi connected!");
    delay(1000);
    initModbus();

    statusDisplay.print("Modbus ready!");
    delay(1000);
    setupConfig();

    statusDisplay.print("Telnet ready!");
    delay(1000);

    telnetController.begin();

    if (protocol == MQTT_PROTOCOL) {
        smartHouseMQTTClient.getClient()->onMessage([](String &topic, String &json) {
            smartHouseMQTTClient.processMessage(topic.c_str(), json.c_str());
        });

        smartHouseMQTTClient.begin();
        statusDisplay.print("MQTT active!");
    }

    if (protocol == MODBUS_TCP_PROTOCOL) {
        modbusTCPController.begin();
        statusDisplay.print("TCP active!");
    }

    delay(1000);

    if (httpServer) {
        MDNS.addService("http", "tcp", 80);
        smartHouseHTTPServer.begin();
        statusDisplay.print("WebServer active!");
    }

    delay(1000);
    ArduinoOTA.begin();
    statusDisplay.print("Init - DONE!");
}

void loop() {
    ArduinoOTA.handle();
    telnetController.task();
    if (protocol == MQTT_PROTOCOL) {
        smartHouseMQTTClient.handle();
    }
    if (protocol == MODBUS_TCP_PROTOCOL) {
        modbusTCPController.handle();
    }
    if (httpServer) {
        smartHouseHTTPServer.handle();
    }
    statusDisplay.show();
}
