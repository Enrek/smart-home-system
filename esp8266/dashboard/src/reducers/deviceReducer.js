import {
	FETCH_DEVICE,
	UPDATE_DEVICE,
	CREATE_EVENT_NOTE,
	FETCH_SENSOR_DEVICE
} from '../actions/actionTypeConstants'

const deviceReducer = (state = {}, action) => {
	switch (action.type) {
		case FETCH_DEVICE:
			{
				const {
					payload
				} = action
				const {
					group,
					name
				} = payload
				var legacy = {}
				legacy = state[group] && state[group][name] && state[group][name]
				state = {
					...state,
					[group]: {
						...state[group],
						[name]: {
							...legacy,
							...payload[group][name]
						}
					}
				}
				break
			}
		case FETCH_SENSOR_DEVICE:
			{
				const {
					payload
				} = action
				const {
					group,
					name
				} = payload
				var oldHistory = {}
				oldHistory = state[group] && state[group][name] && state[group][name]
				state = {
					...state,
					[group]: {
						...state[group],
						[name]: {
							...oldHistory,
							...payload[group][name]
						}
					}
				}
				if (state[group] && state[group][name]) {
					var key = Object.keys(payload[group][name])[0]
					if (state[group][name].history) {
						if (state[group][name].history.length > 20)
							state[group][name].history.shift()
						state[group][name].history.push(payload[group][name][key])
					} else {

						state[group][name].history = [payload[group][name][key]]
					}
				}

				break
			}
		case UPDATE_DEVICE:
			{
				const {
					payload
				} = action
				const {
					group
				} = payload
				state = {
					...state,
					[group]: {
						...state[group],
						...action.payload[group]
					}
				}
				break
			}
		case CREATE_EVENT_NOTE:
			{
				const {
					payload
				} = action
				state = {
					...state,
					history: {
						...state.history,
						...payload
					}
				}
				break
			}
		default:
			state = {
				...state
			}
			break
	}
	return state
}

export default deviceReducer