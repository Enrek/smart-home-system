import {
	FETCH_CONFIGURATION, FETCH_SYSTEM_STATE
} from '../actions/actionTypeConstants'

const systemReducer = (state = {}, action) => {
	switch (action.type) {
		case FETCH_CONFIGURATION:
			{
				state = {
					...state,
					configuration: {
						...state.configuration,
						...action.payload
					}
				}
				break
			}
		case FETCH_SYSTEM_STATE:
			{
				state = {
					...state,
					state: {
						
						...action.payload
					}
				}
				break
			}
		default:
			state = {
				...state
			}
			break
	}
	return state
}

export default systemReducer