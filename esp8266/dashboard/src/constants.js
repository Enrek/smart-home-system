export const HOST = '/api/'

export const SYSTEM_DEVICE_GROUP = 'module'
export const LIGHT_DEVICE_GROUP = 'light'
export const SENSOR_DEVICE_GROUP = 'sensor'
