import React, { Component } from 'react'
import { NavItem, Nav } from 'react-bootstrap'


class HeaderLinks extends Component {
	render() {
		// const notification = (
		// 	<div>
		// 		<i className="fa fa-globe"></i>
		// 		<b className="caret"></b>
		// 		<span className="notification">5</span>
		// 		<p className="hidden-lg hidden-md">Notification</p>
		// 	</div>
		// )
		return (
			<div>
				<Nav>
					<NavItem eventKey={1} href="#">
						{/* <i className="fa fa-dashboard"></i> */}
						<p className="hidden-lg hidden-md">Dashboard</p>
					</NavItem>
					{/* <NavDropdown eventKey={2} title={notification} noCaret id="basic-nav-dropdown">
						<MenuItem eventKey={2.1}>Notification 1</MenuItem>
						<MenuItem eventKey={2.2}>Notification 2</MenuItem>
						<MenuItem eventKey={2.3}>Notification 3</MenuItem>
						<MenuItem eventKey={2.4}>Notification 4</MenuItem>
						<MenuItem eventKey={2.5}>Another notifications</MenuItem>
					</NavDropdown>
					<NavItem eventKey={3} href="#">
						<i className="fa fa-search"></i>
						<p className="hidden-lg hidden-md">Search</p>
					</NavItem> */}
				</Nav>
				<Nav pullRight>
					<NavItem eventKey={3} onClick={()=>window.location='/'}><i className="fa fa-refresh" ></i> Refresh</NavItem>
				</Nav>
			</div>
		)
	}
}

export default HeaderLinks
