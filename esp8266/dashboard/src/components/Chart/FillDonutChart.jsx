import React, { Component } from 'react'
import ChartistGraph from 'react-chartist'
import Chartist from 'chartist'
import 'lib/chartist-plugin-fill-donut.js'


export class FillDonutChart extends Component {

	constructor(props) {
		super(props)
		this.state = {
			seriesA: 0,
			seriesB: 0,
			total: 0,
			freeSpaceValue: 20
		}

	}

	componentDidMount() {
		this.updateChart()
	}

	componentWillMount() {
		this.updateChart()
	}

	componentDidUpdate() {
		this.updateChart()
	}


	shouldComponentUpdate(nextProps) {
		return this.state.seriesA != nextProps.value || this.props.value != nextProps.value
	}

	updateChart() {
		var total = Math.abs(this.props.min) + Math.abs(this.props.max)
		var seriesA = this.props.value
		var seriesB = total - this.props.value
		total += this.state.freeSpaceValue
		this.setState({
			total: total,
			seriesA: seriesA,
			seriesB: seriesB
		})
	}

	buildChartData() {
		return {
			series: [this.props.valueClass ? { value: this.state.seriesA, className: this.props.valueClass } : this.state.seriesA,
				this.props.backClass ? { value: this.state.seriesB, className: this.props.backClass } : this.state.seriesB]
		}
	}

	buildChartOptions() {

		return {
			donut: true,
			donutWidth: 26,
			startAngle: 210,
			total: this.state.total,
			showLabel: false,
			plugins: [
				Chartist.plugins.fillDonut({
					items: [{
						content: '<h3>' +  this.state.seriesA + '<span class="small">' + this.props.valueAppend + '</span></h3>'
					}]
				})
			],
		}
	}

	render() {
		return (
			<ChartistGraph data={this.buildChartData()}
				options={this.buildChartOptions()}
				type="Pie" />
		)
	}
}

export default FillDonutChart

