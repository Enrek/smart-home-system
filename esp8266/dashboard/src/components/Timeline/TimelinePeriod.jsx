import React, { Component } from 'react'
import PropTypes from 'prop-types'

class TimelinePeriod extends Component {
	render() {
		return (
			<li className="timeline-item period">
				<div className="timeline-info"></div>
				<div className="timeline-marker"></div>
				<div className="timeline-content">
					<h2 className="timeline-title">{this.props.title}</h2>
				</div>
			</li>
		)
	}
}

TimelinePeriod.propTypes = {
	title: PropTypes.string.isRequired,
}

export default TimelinePeriod
