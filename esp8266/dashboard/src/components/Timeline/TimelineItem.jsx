import React, {Component} from 'react'
import PropTypes from 'prop-types'

class TimelineItem extends Component {
	
	render() {
		const { markerColor, time, title, description } = this.props
		return (
			
			<li className="timeline-item">
				<div className="timeline-info">
					<span>{time}</span>
				</div>
				<div className={'timeline-marker ' + markerColor + '-marker'} ></div>
				<div className="timeline-content">
					<h3 className="timeline-title">{title}</h3>
					<p>{description}</p>
				</div>
			</li>
		)
	}
}




TimelineItem.propTypes = {
	markerColor:  PropTypes.string,
	title: PropTypes.string.isRequired,
	description: PropTypes.string,
	time: PropTypes.string
}

export default TimelineItem
