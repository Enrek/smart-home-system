import React, { Component } from 'react'
import TimelineItem from '../Timeline/TimelineItem'
import TimelinePeriod from '../Timeline/TimelinePeriod'

class Timeline extends Component {
	render() {
		const { events } = this.props
		var timestamps = events && Object.keys(events).reverse()
		const timeLineItems = []
		var lastDay = 0
		var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
		timestamps && timestamps.forEach((time) => {
			var date = new Date(+time)
			if (lastDay !== date.getDay() + date.getMonth()) {
				lastDay = date.getDay() + date.getMonth()
				timeLineItems.push(
					<TimelinePeriod key={time/2} title={months[date.getMonth()] + ', ' + date.getDay() + 'th'} />
				)
			}
			timeLineItems.push(
				<TimelineItem key={time} markerColor={events[time].color}
					title={events[time].message}
					time={date.getHours() + ':' + date.getMinutes()} description="Current user" />
			)

		})

		return (
			<ul className="timeline timeline-split">
				{timeLineItems}
			</ul>
		)
	}
}

export default Timeline
