import React, { Component } from 'react'

import Card from 'elements/Card/Card.jsx'
import CardHeader from 'elements/Card/CardHeader.jsx'
import CardBody from 'elements/Card/CardBody.jsx'
import CardFooter from 'elements/Card/CardFooter.jsx'

export class SimpleCard extends Component {
	render() {
		return (
			<Card extraClass={this.props.extraClass} >
				<CardHeader>
					<h4 className="title">{this.props.title}</h4>
					<p className="category">{this.props.category}</p>
				</CardHeader>
				<CardBody {...this.props}>
					{this.props.content}
					{this.props.children}
				</CardBody>
				<CardFooter
					stats={this.props.stats}
					statsIcon={this.props.statsIcon}>
					{this.props.legend}
				</CardFooter>
			</Card>
		)
	}
}

export default SimpleCard
