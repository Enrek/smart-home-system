import React, { PureComponent } from 'react'
import Card from 'elements/Card/Card.jsx'
import CardHeader from 'elements/Card/CardHeader.jsx'
import CardBody from 'elements/Card/CardBody.jsx'
import { Row, Col } from 'react-bootstrap'
export class PIRCard extends PureComponent {
	render() {
		const {firstActive, secondActive, title, description, ctHeight, firstTitle, secondTitle} = this.props
		const activeClass = ' ' + (this.props.activeClass || 'text-success')
		return (
			<div >
				<Card extraClass="card-movement" >
					<CardHeader>
						<h4 className="title">{title}</h4>
						<p className="category">{description}</p>
					</CardHeader>
					<CardBody ctHeight={ctHeight || 1} {...this.props}>
						<Row>
							<Col xs={6}>
								<div className="icon" >
									<i className={'fa fa-user '
										+ (firstActive ? activeClass : ' text-muted')
									} ></i>
								</div>
								<div className={'state ' + (firstActive ? activeClass : ' text-muted')} >{firstTitle}</div>
							</Col>
							<Col xs={6}>
								<div className="icon" >
									<i className={'fa fa-user '
										+ (secondActive ? activeClass : ' text-muted')
									} ></i>
								</div>
								<div className={'state ' + (secondActive ? activeClass : ' text-muted')} >{secondTitle}</div>
							</Col>
						</Row>
					
					
					</CardBody>
				</Card>
			</div>
		)
	}
}

export default PIRCard
