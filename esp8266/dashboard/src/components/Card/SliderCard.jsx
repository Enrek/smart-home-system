import React, { PureComponent } from 'react'
// import Card from 'elements/Card/Card.jsx'
// import CardHeader from 'elements/Card/CardHeader.jsx'
// import CardBody from 'elements/Card/CardBody.jsx'
import { Row, Col } from 'react-bootstrap'
import Button from 'elements/CustomButton/CustomButton.jsx'
import { SimpleCard } from './SimpleCard'
import 'rc-tooltip/assets/bootstrap.css'
import 'rc-slider/assets/index.css'

export class SliderCard extends PureComponent {


	changePosition = (value) => {
		this.props.positionChange(value)
	}

	render() {
		return (
			<SimpleCard title={this.props.title} >
				<Row className='sunblind-block'>
					<Col lg={3} md={12}>
						<Button block large onClick={() => this.changePosition(0)} fill={this.props.targetPosition == 0} bsStyle={this.props.targetPosition == 0 ? 'info' : 'default'} >Close</Button>
						<br />
					</Col>
					<Col lg={6} md={12}>
						<Button block large onClick={() => this.changePosition(50)} fill={this.props.targetPosition != 0 && this.props.targetPosition != 100} bsStyle={this.props.targetPosition != 0 && this.props.targetPosition != 100 ? 'info' : 'default'} >Semi-open</Button>
						<br />
					</Col>
					<Col lg={3} md={12}>
						<Button block large onClick={() => this.changePosition(100)} fill={this.props.targetPosition == 100} bsStyle={this.props.targetPosition == 100 ? 'info' : 'default'} >Open</Button>
						<br />
					</Col>
				</Row>
			</SimpleCard>
		)
	}
}

export default SliderCard
