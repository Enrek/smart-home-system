import React, { PureComponent } from 'react'
import Card from 'elements/Card/Card.jsx'
import CardHeader from 'elements/Card/CardHeader.jsx'
import CardBody from 'elements/Card/CardBody.jsx'

export class SwitchCard extends PureComponent {
	render() {
		const {active} = this.props
		const activeClass = ' ' + (this.props.activeClass || 'text-success')
		return (
			<div >
				<Card extraClass="card-switch" >
					<CardHeader>
						<h4 className="title">{this.props.title}</h4>
						<p className="category">{this.props.description}</p>
					</CardHeader>
					<CardBody ctHeight={this.props.ctHeight || 1} {...this.props}>

						<div className="icon" >
							<i className={'fa '
								+ (this.props.iconClass ? this.props.iconClass : ' fa-exclamation')
								+ (active ? activeClass : ' text-muted')
							} ></i>
						</div>
						<div className={'state ' + (active ? activeClass : ' text-muted')} >{active ? 'ON' : 'OFF'}</div>
					</CardBody>
				</Card>
			</div>
		)
	}
}

export default SwitchCard
