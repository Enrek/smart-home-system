import React, { Component } from 'react'
import { Row, Col } from 'react-bootstrap'
import Card from 'elements/Card/Card.jsx'
import CardHeader from 'elements/Card/CardHeader.jsx'
import CardBody from 'elements/Card/CardBody.jsx'

export class SensorCard extends Component {
	render() {
		return (
			<Card extraClass="card-sensor">
				<CardHeader>
					<Row>
						<Col xs={9}>
							<h4 className="title">{this.props.title}</h4>
						</Col>
						<Col xs={3}>
							<div className="icon-title" >
								{this.props.titleIcon}
							</div>
						</Col>
					</Row>
				</CardHeader>
				<CardBody ctHeight={this.props.ctHeight || 1} {...this.props}>
					{this.props.children}
				</CardBody>
			</Card >
		)
	}
}

export default SensorCard
