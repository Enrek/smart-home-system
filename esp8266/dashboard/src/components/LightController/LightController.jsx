import React, { Component } from 'react'


class ComponentRow extends Component {

	constructor(props) {
		super(props)
		this.handleClick = this.handleClick.bind(this)
		this.state = { color: {} }
	}


	handleClick() {
		const { component } = this.props
		this.props.changeSectorState(component.sector,
			{
				color: this.isSectorActive(component.color) ? blackColor : whiteColor
			})
	}


	isSectorActive(color) {
		return color.red > 0 || color.blue > 0 || color.green > 0
	}


	render() {
		const component = this.props.component
		return (
			<tr>
				<td>
					<div onClick={() => this.handleClick()} className="status">
						<i className={'fa fa-lightbulb-o' + (this.isSectorActive(component.color) ? ' text-warning' : ' text-muted')} ></i>
					</div>
				</td>
				<td>
					<div className="light-title">
						{component.title}
					</div>
				</td>
				<td className="td-actions text-right" >
					<div className="color-pick">
						<i className="fa fa-eyedropper"></i>
					</div>
				</td>
			</tr>
		)
	}
}

class GroupRow extends Component {
	render() {
		const group = this.props.group
		return (
			<tr>
				<td colSpan="3">
					<div className="group-title">{group.title}</div>
				</td>
			</tr>
		)
	}
}


export class LightController extends Component {

	constructor(props) {
		super(props)
		this.state = { lighting: lighting }
	}


	componentDidUpdate = () => {
		this.mergeLightingState()
	}


	mergeLightingState() {
		const { ledDevice: { ledItems } } = this.props
		if (ledItems === undefined || Object.keys(ledItems).length === 0)
			return

		var newLighting = this.state.lighting
		var componentNeedUpdate = false
		newLighting.groups && newLighting.groups.forEach(function (group, groupIndex) {
			group.components && group.components.forEach((component, componentIndex) => {
				ledItems.forEach((ledItem) => {
					if (ledItem.sector === component.sector) {
						if (!isColorsNotEqual(component.color, ledItem.color)) {
							return
						}
						componentNeedUpdate = true
						newLighting.groups[groupIndex].components[componentIndex] = {
							...component,
							color: { ...ledItem.color }
						}
					}
				})
			})

		})
		if (componentNeedUpdate) {
			this.setState({
				lighting: newLighting
			})
		}
	}

	renderGroupTitle(group) {
		return group && (
			<tr>
				<td colSpan="3">
					<div className="group-title">group.title</div>
				</td>
			</tr>
		)
	}

	renderComponentsForGroup(group) {
		return group.components && group.components.map(component => {
			return (
				<tr>
					<td>
						<div className="status">
							<i className="fa fa-lightbulb-o"></i>
						</div>
					</td>
					<td>
						<div className="content">
							{component.title}
						</div>
					</td>
					<td className="td-actions text-right" >
						<div className="color-pick">
							<i className="fa fa-eyedropper"></i>
						</div>
					</td>
				</tr>
			)

		})
	}

	render() {
		const rows = []
		this.state.lighting.groups && this.state.lighting.groups.forEach((group) => {
			rows.push(
				<GroupRow group={group} key={group.title} />
			)
			group.components && group.components.forEach((component) => {
				rows.push(
					<ComponentRow changeSectorState={(sector, color) => this.props.changeSectorState(sector, color, component.title)} component={component} key={component.title} />
				)
			})
		})

		return (
			<table className="table light-controller">
				<tbody>
					{rows}
				</tbody>
			</table>
		)
	}
}

function isColorsNotEqual(firstColor, secondColor) {
	return firstColor.red !== secondColor.red ||
		firstColor.green !== secondColor.green ||
		firstColor.blue !== secondColor.blue
}

const blackColor = { red: 0, green: 0, blue: 0 }
const whiteColor = { red: 255, green: 255, blue: 255 }
const lighting = {
	groups: [
		{
			title: 'First floor',
			components: [
				{
					title: 'Living room',
					color: blackColor,
					sector: 0
				},
				{
					title: 'Bathroom',
					color: blackColor,
					sector: 2
				},
			]
		},
		{
			title: 'Second floor',
			components: [
				{
					title: 'Another room',
					color: blackColor,
					sector: 1
				},
				{
					title: 'Hall',
					color: blackColor,
					sector: 3
				},
			]
		}
	]
}



export default LightController
