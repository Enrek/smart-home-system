import React, { Component } from 'react'

import NotificationSystem from 'react-notification-system'
import { connect } from 'react-redux'
import Header from 'components/Header/Header'
import Footer from 'components/Footer/Footer'
import Sidebar from 'components/Sidebar/Sidebar'

import { style } from 'variables/Variables.jsx'
import { fetchDevice, updateDevice, createHistoryNote } from '../../actions/deviceActions'
import { fetchConfiguration, updateConfiguration, fetchSystemState, reboot, reset } from '../../actions/systemActions'
import { SYSTEM_DEVICE_GROUP, LIGHT_DEVICE_GROUP, SENSOR_DEVICE_GROUP } from '../../constants'

// import { updateSystem } from './actions/systemActions'

class App extends Component {
	constructor(props) {
		super(props)
		this.componentDidMount = this.componentDidMount.bind(this)
		this.handleNotificationClick = this.handleNotificationClick.bind(this)
		this.state = {
			_notificationSystem: null
		}
	}
	handleNotificationClick(position) {
		var color = Math.floor((Math.random() * 4) + 1)
		var level
		switch (color) {
			case 1:
				level = 'success'
				break
			case 2:
				level = 'warning'
				break
			case 3:
				level = 'error'
				break
			case 4:
				level = 'info'
				break
			default:
				break
		}
		this.state._notificationSystem.addNotification({
			title: (<span data-notify="icon" className="pe-7s-gift"></span>),
			message: (
				<div>
					Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer.
				</div>
			),
			level: level,
			position: position,
			autoDismiss: 15,
		})
	}

	componentDidMount() {
		const { fetchDevice } = this.props

		setInterval(() => {
			fetchDevice(SENSOR_DEVICE_GROUP,'temp')
			fetchDevice(SENSOR_DEVICE_GROUP,'co2')
			fetchDevice(SYSTEM_DEVICE_GROUP, 'security')
			fetchDevice(SYSTEM_DEVICE_GROUP, 'fire')
		}, 15000)

		// fetchDevice(SENSOR_DEVICE_GROUP, 'temp')
		// fetchDevice(SENSOR_DEVICE_GROUP, 'co2')
		// fetchDevice(SYSTEM_DEVICE_GROUP, 'security')
		// fetchDevice(SYSTEM_DEVICE_GROUP, 'fire')


		setInterval(() => {
			fetchDevice(SENSOR_DEVICE_GROUP, 'pir', 0)
			fetchDevice(SENSOR_DEVICE_GROUP, 'pir', 1)
		}, 4000)
	
		setInterval(() => {
			fetchDevice(SENSOR_DEVICE_GROUP, 'solar')
		}, 29000)
		setInterval(() => {
			if(this.getSystemDevice('fire').alarm 
			|| this.getSystemDevice('security').alarm
			|| this.getSystemDevice('security').active) {
				fetchDevice(SYSTEM_DEVICE_GROUP, 'heating')
				fetchDevice(SYSTEM_DEVICE_GROUP, 'conditioner')
				fetchDevice(SENSOR_DEVICE_GROUP, 'solar')
				fetchDevice(SYSTEM_DEVICE_GROUP, 'fan')
				fetchDevice(LIGHT_DEVICE_GROUP, 'led')
				fetchDevice(LIGHT_DEVICE_GROUP, 'sunblind')
			}
		}, 45000)
	
		// setInterval(() => {
		// 	fetchDevice(SYSTEM_DEVICE_GROUP, 'security')
		// 	fetchDevice(SYSTEM_DEVICE_GROUP, 'heating')
		// 	fetchDevice(SYSTEM_DEVICE_GROUP, 'conditioner')
		// 	fetchDevice(SYSTEM_DEVICE_GROUP, 'fan')
		// 	fetchDevice(LIGHT_DEVICE_GROUP, 'sunblind')
		// 	fetchDevice(LIGHT_DEVICE_GROUP, 'led')
		// 	fetchDevice(SENSOR_DEVICE_GROUP, 'temp')
		// 	fetchDevice(SENSOR_DEVICE_GROUP, 'co2')
		// }, 30000)
		// setInterval(() => {
		// 	fetchDevice(SENSOR_DEVICE_GROUP,'temp')
		// 	fetchDevice(SENSOR_DEVICE_GROUP,'co2')
		// 	fetchDevice(SENSOR_DEVICE_GROUP, 'pir', 0)
		// 	fetchDevice(SENSOR_DEVICE_GROUP, 'pir', 1)	
		// }, 1000)
		// this.setState({ _notificationSystem: this.refs.notificationSystem })
		// var _notificationSystem = this.refs.notificationSystem
		// var color = Math.floor((Math.random() * 4) + 1)
		// var level
		// switch (color) {
		// case 1:
		// 	level = 'success'
		// 	break
		// case 2:
		// 	level = 'warning'
		// 	break
		// case 3:
		// 	level = 'error'
		// 	break
		// case 4:
		// 	level = 'info'
		// 	break
		// default:
		// 	break
		// }
		// _notificationSystem.addNotification({
		//     title: (<span data-notify="icon" className="pe-7s-gift"></span>),
		//     message: (
		//         <div>
		//             Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer.
		//         </div> 
		//     ),
		//     level: level,
		//     position: "tr",
		//     autoDismiss: 15,
		// });
	}
	componentDidUpdate(e) {
		// if (window.innerWidth < 993 && e.history.location.pathname !== e.location.pathname && document.documentElement.className.indexOf('nav-open') !== -1) {
		// 	document.documentElement.classList.toggle('nav-open')
		// }
	}


	getDevice = (group, name, id) => {
		if (this.props[group] && this.props[group][name]) {
			return this.props[group][name][id] ? this.props[group][name][id] : this.props[group][name]
		}

		return {}
	}

	getSystemDevice = (name) => {
		return this.getDevice(SYSTEM_DEVICE_GROUP, name)
	}

	switchDeviceState = (group, name) => {
		var oldStatus = this.getDevice(group, name).active

		this.props.updateDevice(group, name, { active: !oldStatus }, () => {
			var status = this.getDevice(group, name).active
			if (status == oldStatus)
				return
			this.props.createHistoryNote(this.buildHistoryNote(
				(status ? 'Turn ON ' : 'Turn OFF ') + name + ' module',
				(status ? 'green' : 'red')))
		})
	}

	switchSystemDeviceState = (name) => {
		this.switchDeviceState(SYSTEM_DEVICE_GROUP, name)
	}

	getLightDevice = (name) => {

		return this.getDevice(LIGHT_DEVICE_GROUP, name)
	}

	getSensorDevice = (name) => {
		return this.getDevice(SENSOR_DEVICE_GROUP, name)
	}

	getPIRDevice = (name, id) => {
		return this.getDevice(SENSOR_DEVICE_GROUP, name, id)
	}

	updateSunblindState = (state) => {
		var oldPosition = this.getDevice(LIGHT_DEVICE_GROUP, 'sunblind').targetPosition
		return this.props.updateDevice(LIGHT_DEVICE_GROUP, 'sunblind', state, () => {
			var position = this.getDevice(LIGHT_DEVICE_GROUP, 'sunblind').targetPosition
			if (position == oldPosition)
				return
			this.props.createHistoryNote(this.buildHistoryNote('Change sunblind position to ' + position, 'blue'))
		})
	}

	updateLedSectorState = (sector, color, title) => {

		var state = { ledItems: [] }
		state.ledItems.push(
			{ sector: sector, ...color }
		)

		this.props.updateDevice(LIGHT_DEVICE_GROUP, 'led', state, () => {
			this.props.createHistoryNote(this.buildHistoryNote('Change light in the ' + title, 'orange'))
		})
	}

	getHistory = () => {
		return this.props.history
	}

	buildHistoryNote = (message, color) => {
		var note = {}
		note[Date.now()] = {
			message: message,
			color: color
		}
		return note
	}

	getConfiguration = () => {
		return this.props.configuration || {}
	}

	getSystemState = () => {
		return this.props.systemState || {}
	}

	updateConfiguration = (name, payload) => {
		this.props.updateConfiguration(name, payload)
	}


	render() {
		return (

			<div className="wrapper">
				<NotificationSystem ref="notificationSystem" style={style} />
				<Sidebar {...this.props} />
				<div id="main-panel" className="main-panel">
					<Header {...this.props} />

					{React.Children.map(this.props.children, child =>
						React.cloneElement(child, {
							getDevice: this.getDevice,
							updateDevice: this.props.updateDevice,
							getSystemDevice: this.getSystemDevice,
							getLightDevice: this.getLightDevice,
							getSensorDevice: this.getSensorDevice,
							getPIRDevice: this.getPIRDevice,
							switchDeviceState: this.switchDeviceState,
							switchSystemDeviceState: this.switchSystemDeviceState,
							updateSunblindState: this.updateSunblindState,
							updateLedSectorState: this.updateLedSectorState,
							getHistory: this.getHistory,
							fetchConfiguration: this.props.fetchConfiguration,
							getConfiguration: this.getConfiguration,
							updateConfiguration: this.updateConfiguration,
							fetchSystemState: this.props.fetchSystemState,
							getSystemState: this.getSystemState,
							reboot: reboot,
							reset: reset
						}))}

					<Footer />
				</div>
			</div>
		)
	}
}


const mapStateToProps = (state) => {
	return {
		module: state.devices.module,
		light: state.devices.light,
		sensor: state.devices.sensor,
		history: state.devices.history,
		configuration: state.system.configuration,
		systemState: state.system.state
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		fetchDevice: (group, name, id) => {
			dispatch(fetchDevice(group, name, id))
		},
		updateDevice: (group, name, state, callback) => {
			dispatch(updateDevice(group, name, state, callback))
		},
		createHistoryNote: (note) => {
			dispatch(createHistoryNote(note))
		},
		fetchConfiguration: (name) => {
			dispatch(fetchConfiguration(name))
		},
		fetchSystemState: () => {
			dispatch(fetchSystemState())
		},
		updateConfiguration: (name, state) => {
			dispatch(updateConfiguration(name, state))
		}
	}
}


// const mapDispatchToProps = (dispatch) => {
// 	return {
// 		updateSystem: (name) => {
// 			dispatch(updateSystem(name))
// 		}
// 	}
// }
export default connect(mapStateToProps, mapDispatchToProps)(App)