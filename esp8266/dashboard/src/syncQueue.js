import Queue from 'promise-queue'

Queue.configure(window.Promise)

// max concurrent - 1
// max queue - 5
export const syncQueue =  new Queue(1, 5)
export const status = () => {
	console.log('Pending ' + syncQueue.pendingPromises + '/' + syncQueue.maxPendingPromises + ' Queued ' + syncQueue.queue.length + '/' + syncQueue.maxQueuedPromises) 
}

