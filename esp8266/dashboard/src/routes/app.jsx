import Dashboard from 'views/Dashboard/Dashboard'

import TemperatureControl from 'views/Sensor/TemperatureControl'
import CO2Control from 'views/Sensor/CO2Control'
import Configuration from 'views/Configuration/Configuration'

const appRoutes = [
	{ path: '/dashboard', name: 'Dashboard', icon: 'fa-home', component: Dashboard },
	// { path: '/user', name: 'User Profile', icon: 'pe-7s-user', component: UserProfile },
	// { path: '/table', name: 'Table List', icon: 'pe-7s-note2', component: TableList },
	// { path: '/typography', name: 'Typography', icon: 'pe-7s-news-paper', component: Typography },
	// { path: '/icons', name: 'Icons', icon: 'pe-7s-science', component: Icons },
	// { path: '/maps', name: 'Maps', icon: 'pe-7s-map-marker', component: Maps },
	// { path: '/notifications', name: 'Notifications', icon: 'pe-7s-bell', component: Notifications },
	{ path: '/sensor/temp', name: 'Temp controller', icon: 'fa-thermometer-half', component: TemperatureControl },
	{ path: '/sensor/co2', name: 'CO2 controller', icon: 'fa-cloud', component: CO2Control },
	// { upgrade: true, path: '/upgrade', name: 'Upgrade to PRO', icon: 'pe-7s-rocket', component: Upgrade },
	{ path: '/config', name: 'Configuration', icon: 'fa-wrench', component: Configuration },
	{ redirect: true, path: '/', to: '/dashboard', name: 'Dashboard' }
]

export default appRoutes
