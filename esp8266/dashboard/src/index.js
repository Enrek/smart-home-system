import React from 'react'
import thunk from 'redux-thunk'
import ReactDOM from 'react-dom'
// import {
// 	createDevTools
// } from 'redux-devtools'
// import LogMonitor from 'redux-devtools-log-monitor'
// import DockMonitor from 'redux-devtools-dock-monitor'

import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { Router, Route, browserHistory, Redirect } from 'react-router'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'

import App from 'containers/App/App.jsx'
import {
	Provider
} from 'react-redux'

import './assets/css/bootstrap.min.css'
import './assets/css/animate.min.css'
import './assets/sass/light-bootstrap-dashboard.css'
import './assets/css/demo.css'


import appRoutes from 'routes/app.jsx'
import deviceReducer from './reducers/deviceReducer'
import systemReducer from './reducers/systemReducer'
import { reducer as reduxFormReducer } from 'redux-form'

const reducer = combineReducers({
	routing: routerReducer,
	devices: deviceReducer,
	system: systemReducer,
	form: reduxFormReducer
})

// const DevTools = createDevTools(
// 	<DockMonitor toggleVisibilityKey = 'ctrl-h'
// 		changePositionKey = 'ctrl-q' >
// 		<LogMonitor theme = 'tomorrow'
// 			preserveScrollTop = {false}/> 
// 	</DockMonitor >
// )



const store = createStore(
	reducer,
	{ devices: { ...window.initData } },
	compose(
		applyMiddleware(thunk)  // ,DevTools.instrument()
	)
)

const history = syncHistoryWithStore(browserHistory, store)

ReactDOM.render((
	<Provider store={store} >
		<div>
			<Router history={history}>
				<Redirect from="/" to="/dashboard" />
				<Route path="/" component={App}>
					{appRoutes.map((prop, key) => {
						return (
							<Route path={prop.path} component={prop.component} key={key} />
						)
					})}
				</Route>
			</Router>
			{/* <DevTools /> */}
		</div>
	</Provider >
), document.getElementById('root'))




