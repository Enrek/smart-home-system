import React, { Component } from 'react'

export class CardHeader extends Component {
	render() {
		return (
			<div onClick={this.props.onClick} className={'content'
				+ (this.props.ctAllIcons ? ' all-icons' : '')
				+ (this.props.ctTableFullWidth ? ' table-full-width' : '')
				+ (this.props.ctTableResponsive ? ' table-responsive' : '')
				+ (this.props.ctTableUpgrade ? ' table-upgrade' : '')
				+ (this.props.ctHeight ? ' content-height-' + this.props.ctHeight : '')}>
				{this.props.ctScroll && <div className={(this.props.ctScroll ? ' scroll' : '')}>
					{this.props.children}
				</div>}
				{this.props.ctScroll || this.props.children}
			</div>
		)
	}
}

export default CardHeader
