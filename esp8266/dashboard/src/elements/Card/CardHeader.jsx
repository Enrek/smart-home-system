import React, { Component } from 'react'

export class CardHeader extends Component {
	render() {
		return (
			<div className={'header'
				+ (this.props.hCenter ? ' text-center' : '')}>
				{this.props.children}
			</div>
		)
	}
}

export default CardHeader
