import React, { Component } from 'react'

export class CardFooter extends Component {
	render() {
		return (
			(this.props.children || this.props.stats || this.props.statsIcon) ?
				<div className="footer">
					{this.props.children}
					{this.props.stats != null ? <hr /> : ''}
					<div className="stats">
						{this.props.stats != null ? <i className={this.props.statsIcon}></i> : ''} {this.props.stats}
					</div>
				</div> : <div></div>
		)
	}
}

export default CardFooter
