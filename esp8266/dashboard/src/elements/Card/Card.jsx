import React, { Component } from 'react'

export class Card extends Component {
	render() {
		return (
			<div className={'card' + (this.props.plain ? ' card-plain' : '') + ' ' + (this.props.extraClass ? ' ' + this.props.extraClass : '')}>
				{this.props.children}
			</div>
		)
	}
}

export default Card