import axios from 'axios'

import {syncQueue, status} from '../syncQueue'

import {
	FETCH_CONFIGURATION, FETCH_SYSTEM_STATE
} from './actionTypeConstants'
import {
	HOST
} from '../constants'



export function fetchConfiguration(name) {
	console.log('Fetch configuration')
	return dispatch => {
		return axios.get(HOST + 'device/configuration/' + name)
			.then(req => req.data)
			.then(json => dispatch(buildState(json.d, name, FETCH_CONFIGURATION)))
	}
}


export function updateConfiguration(name, state = {}) {
	return dispatch => {
		syncQueue.add(() => {
			return axios.post(HOST + 'device/configuration/' + name, {d: state}) //TODO: Changer to PATCH
					.then(req => req.data)
					.then(json => dispatch(buildState(json, name, FETCH_CONFIGURATION)))
		}).then(function () {
			status() 
		}, function (err) {
			console.error(err)
		}) 
		status()
	}

}

export function reboot() {
	syncQueue.add(() => {
		return axios.post(HOST + 'mgmt/initiate/device/reboot') //TODO: Changer to PATCH
	})
}

export function reset() {
	syncQueue.add(() => {
		return axios.post(HOST + 'mgmt/initiate/device/reset') //TODO: Changer to PATCH
	})
}

export function fetchSystemState() {
	return dispatch => {
		syncQueue.add(() => {
			return axios.get(HOST + 'device/system')
			.then(req => req.data)
			.then(json => dispatch({payload: json.d, type: FETCH_SYSTEM_STATE}))
		}).then(function () {
			status() 
		}, function (err) {
			console.error(err)
		}) 
		status()
	}
}


function buildState(data, name, type) {
	var result = {
		type: type,
		payload: {}
	}
	result.payload[name] = data || {}
	return result
}