export const FETCH_DEVICE = 'FETCH_DEVICE'

export const UPDATE_DEVICE = 'UPDATE_DEVICE'

export const CREATE_EVENT_NOTE = 'CREATE_EVENT_NOTE'

export const FETCH_SENSOR_DEVICE = 'FETCH_SENSOR_DEVICE'

export const FETCH_CONFIGURATION = 'FETCH_CONFIGURATION'

export const FETCH_SYSTEM_STATE = 'FETCH_SYSTEM_STATE'