import axios from 'axios'

import { syncQueue, status } from '../syncQueue'

import {
	FETCH_DEVICE, UPDATE_DEVICE, CREATE_EVENT_NOTE, FETCH_SENSOR_DEVICE
} from './actionTypeConstants'
import {
	HOST, SENSOR_DEVICE_GROUP
} from '../constants'


export function fetchDevice(group, name, id = -1) {

	return dispatch => {
		syncQueue.add(() => {
			return axios.get(HOST + 'device/' + group + '/' + name + (id != -1 ? '/' + id : ''), { timeout: 20000 })
				.then(req => req.data)
				.then(json => dispatch(fetchState(group, name, id, json)))
		}).then(function () {
			status()
		}, function (err) {
			console.error(err)
		})
		status()
	}
}


export function updateDevice(group, name, state = {}, callback) {
	return dispatch => {
		syncQueue.add(() => {
			return axios.post(HOST + 'device/' + group + '/' + name, { d: state }) //TODO: Changer to PATCH
				.then(req => req.data)
				.then(json => dispatch(updateState(group, name, json)))
		}).then(function (data) {
			status()
			if(callback)
				callback(data.payload)
		}, function (err) {
			console.error(err)
		})
		status()
	}

}

export function createHistoryNote(state = {}) {
	return {
		type: CREATE_EVENT_NOTE,
		payload: state
	}
}


function updateState(group, name, json) {
	return {
		type: UPDATE_DEVICE,
		payload: buildDevice(group, name, -1, json)
	}
}

function fetchState(group, name, id, json) {
	var payload = buildDevice(group, name, id, json)
	var type = FETCH_DEVICE
	if (group === SENSOR_DEVICE_GROUP && id == -1) {
		type = FETCH_SENSOR_DEVICE
	}
	return {
		type: type,
		payload: payload
	}
}



function buildDevice(group, name, id, json) {
	var device = {
		group: group,
		name: name
	}
	device[group] = {}
	if (id != -1) {
		device['id'] = id
		device[group][name] = {}
		device[group][name][id] = json.d
	} else {

		device[group][name] = json.d
	}
	return device
}




