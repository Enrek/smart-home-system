import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap'

import { SimpleCard } from 'components/Card/SimpleCard.jsx'
import SensorCard from '../../components/Card/SensorCard'
import PIRCard from '../../components/Card/PIRCard'
import Timeline from '../../components/Timeline/Timeline'
import { SwitchCard } from '../../components/Card/SwitchCard'
import SliderCard from '../../components/Card/SliderCard'
import LightController from '../../components/LightController/LightController.jsx'
import DonutChart from '../../components/DonutChart/DonutChart'
import MediaQuery from 'react-responsive'

class Dashboard extends Component {
	createLegend(json) {
		var legend = []
		for (var i = 0; i < json['names'].length; i++) {
			var type = 'fa fa-circle text-' + json['types'][i]
			legend.push(
				<i className={type} key={i}></i>
			)
			legend.push(' ')
			legend.push(
				json['names'][i]
			)
		}
		return legend
	}
	render() {
		const { getSystemDevice,
			switchSystemDeviceState,
			getLightDevice,
			getSensorDevice,
			getPIRDevice,
			updateSunblindState,
			updateLedSectorState,
			getHistory } = this.props
		return (
			<div className="content">
				<Grid fluid>
					<Row>
						<Col lg={12}>
							{getSystemDevice('fire').alarm &&
								<div className="alert alert-danger">
									<h3>There is a possible <b>FIRE</b> detected! Contact emergency service!</h3>
								</div>}
							{getSystemDevice('security').alarm &&
								<div className="alert alert-danger">
									<h3>There is a possible <b>ROBBERY</b>! Movement detected in the house! Contact emergency service!</h3>
								</div>
							}

						</Col>
					</Row>
					<Row>
						<Col lg={7}>
							<Row>
								<Col lg={6} md={6} sm={12}>
									<SensorCard title="Temperature"
										titleIcon={<i className="fa fa-thermometer-half text-warning"></i>}
									>
										<DonutChart valueAppend="C&deg;" value={getSensorDevice('temp').temperature || 0} />
									</SensorCard>
								</Col>
								<Col lg={6} md={6} sm={12}>
									<SensorCard title={<span>CO<sub>2</sub></span>}
										titleIcon={<i className="fa fa-cloud text-info"></i>}
									>
										<DonutChart color='dark-blue' value={getSensorDevice('co2').concentration || 0} />
									</SensorCard>
								</Col>
							</Row>
							<Row>
								<Col lg={4} md={4} sm={12}>
									<SwitchCard title="Security"
										iconClass="fa-shield"
										active={getSystemDevice('security').active}
										onClick={() => switchSystemDeviceState('security')}
										activeClass="text-danger" />
								</Col>
								<Col lg={4} md={4} sm={12}>
									<SwitchCard title="Conditioner"
										iconClass="fa-snowflake-o"
										active={getSystemDevice('conditioner').active}
										onClick={() => switchSystemDeviceState('conditioner')}
										activeClass="text-info" />
								</Col>
								<Col lg={4} md={4} sm={12}>
									<SwitchCard title="Heating"
										iconClass="fa-sun-o"
										active={getSystemDevice('heating').active}
										onClick={() => switchSystemDeviceState('heating')}
										activeClass="text-warning" />

								</Col>
							</Row>
							<Row>
								<Col lg={4} md={4} sm={12} >
									<SwitchCard title="Fan"
										iconClass="fa-empire"
										active={getSystemDevice('fan').active}
										onClick={() => switchSystemDeviceState('fan')}
									/>
								</Col>
								<Col lg={8} md={8} sm={12} >
									<SliderCard
										targetPosition={getLightDevice('sunblind').targetPosition}
										positionChange={(position) => updateSunblindState({ targetPosition: position })}
										title="Sunblind" />
								</Col>
							</Row>
							<Row>

							</Row>
						</Col>
						<MediaQuery query="(min-width: 1200px)">
							<Col lg={5} sm={12}>
								<SimpleCard title="Events timeline" ctHeight="3" ctScroll legend=" " >
									<Timeline events={getHistory()}>
									</Timeline>
								</SimpleCard>
							</Col>
						</MediaQuery>
					</Row>
					<Row>
						<Col lg={6}>
							<Row>
								<Col lg={6}>
									<SensorCard title="Solar panel"
										titleIcon={<i className="fa fa-leaf text-success"></i>}
									>
										<DonutChart valueAppend="W" value={getSensorDevice('solar').voltage * 20 || 0} />
									</SensorCard>
								</Col>
								<Col lg={6}>
									<SwitchCard title="Socket"
										iconClass="fa-plug"
										active={getSystemDevice('socket').active}
										onClick={() => switchSystemDeviceState('socket')}
										activeClass="text-success" />
								</Col>
							</Row>
							<Row>
								<Col lg={4}>
									<SwitchCard title="Fire alarm"
										iconClass="fa-fire"
										active={getSystemDevice('fire').active}
										onClick={() => switchSystemDeviceState('fire')}
										activeClass="text-danger" />

								</Col>
								<Col lg={8}>
									<PIRCard title="Movement sensors"
										firstTitle="First "
										secondTitle="Second "
										firstActive={getPIRDevice('pir', 0).movement}
										secondActive={getPIRDevice('pir', 1).movement} />
								</Col>
							</Row>
						</Col>


						<Col lg={6} md={12}>
							<SimpleCard title="Lighting" extraClass="card-light-controller" >
								<div className="table-full-width">
									<LightController ledDevice={getLightDevice('led')} changeSectorState={(sector, color, title) => updateLedSectorState(sector, color, title)} />
								</div>
							</SimpleCard>
						</Col>
					</Row>
					<MediaQuery query="(max-width: 1200px)">
						<Row>
							<Col lg={5} sm={12}>
								<SimpleCard title="Events timeline" ctHeight="3" ctScroll legend=" " >
									<Timeline events={getHistory()} />
								</SimpleCard>
							</Col>
						</Row>
					</MediaQuery>
				</Grid>
			</div>
		)
	}
}

export default Dashboard
