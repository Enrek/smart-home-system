import React, { Component } from 'react'

import { Grid, Row, Col } from 'react-bootstrap'
import { SimpleCard } from 'components/Card/SimpleCard.jsx'
// import { FormInputs } from 'components/FormInputs/FormInputs.jsx'
import Button from 'elements/CustomButton/CustomButton.jsx'
import MQTTForm from './MQTTForm'

class UserProfile extends Component {

	componentDidMount() {
		const { fetchConfiguration, fetchSystemState } = this.props
		fetchConfiguration('mqtt')
		fetchSystemState()
	}


	showResults = (res) => {
		this.props.updateConfiguration('mqtt', res)
	}

	render() {
		const { getConfiguration, reboot, reset, getSystemState } = this.props
		const configuration = getConfiguration()
		const systemState = getSystemState()
		console.log(systemState)
		return (
			<div className='content'>
				<Grid fluid>
					<Row>
						<Col md={8}>
							<SimpleCard title='MQTT client configuration'>
								<MQTTForm onSubmit={this.showResults} initialValues={configuration.mqtt} />
							</SimpleCard>
						</Col>
						<Col md={4}>
							<SimpleCard
								title="Status"
								category="Device status"
								content={
									<form>

										<div className="typo-line">
											<p className="category">MQTT connected</p><p className={systemState.mqttConnected ? 'text-success' : 'text-danger'} >{systemState.mqttConnected ? 'YES' : 'NO'}</p>
										</div>
										<div className="typo-line">
											<p className="category">Heap free</p><p className="text-success" >{systemState.freeHeap}</p>
										</div>
										<div className="typo-line">
											<p className="category">VCC</p><p className="text-info" >{systemState.vcc}mV</p>
										</div>

										<Row></Row>
										<Button
											bsStyle='warning'
											onClick={() => reboot()}
											fill
										>
											REBOOT
										</Button>

									</form>
								}
							/>
							<SimpleCard
								title="RESET"
							>
								<Button
									bsStyle='danger'
									onClick={() => reset()}
									fill
								>
									<b>RESET ALL SETTINGS AND REBOOT</b>
								</Button>
							</SimpleCard>

						</Col>
					</Row>
				</Grid>
			</div>
		)
	}
}

export default UserProfile
