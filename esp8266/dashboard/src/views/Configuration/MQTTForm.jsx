import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { Row, FormGroup, ControlLabel, FormControl } from 'react-bootstrap'
import Button from 'elements/CustomButton/CustomButton.jsx'

const FieldInput = ({ input, meta, type, placeholder, min, max, componentClass, children }) => {
	return (
		<FormControl
			type={type}
			placeholder={placeholder}
			min={min}
			max={max}
			value={input.value}
			onChange={input.onChange}
			componentClass={componentClass} >
			{children}
		</FormControl>
	)
}

const MQTTForm = props => {
	const { handleSubmit, pristine, submitting } = props

	return (
		<form onSubmit={handleSubmit}>
			<Row>

				<FormGroup className='col-md-12'>
					<ControlLabel>Client name</ControlLabel>
					<Field name="clientName"
						type='text'
						component={FieldInput}
						placeholder="Please wait..."
					/>
				</FormGroup>
			</Row>
			<Row>
				<FormGroup className='col-md-6'>
					<ControlLabel>Host URI</ControlLabel>
					<Field name="host"
						type='text'
						component={FieldInput}
					/>
				</FormGroup>
				<FormGroup className='col-md-3'>
					<ControlLabel>Username</ControlLabel>
					<Field name="username"
						type='text'
						component={FieldInput}
					/>
				</FormGroup>
				<FormGroup className='col-md-3'>
					<ControlLabel>Password</ControlLabel>
					<Field name="password"
						type='password'
						component={FieldInput}
					/>
				</FormGroup>
			</Row>
			<Row>
				<FormGroup className='col-md-6'>
					<ControlLabel>Quality of Service</ControlLabel>
					<Field component={FieldInput} name="qos" componentClass="select">
						<option value="0">At most once (0)</option>
						<option value="1">At least once (1)</option>
						<option value="2">Exactly once (2)</option>
					</Field>
				</FormGroup>
				<FormGroup className='col-md-6'>
					<ControlLabel>Connection timeout (sec)</ControlLabel>
					<Field component={FieldInput} name="timeout" type="number" max="20" />
				</FormGroup>
			</Row>
			<Button
				bsStyle='info'
				fill
				type='submit'
				pullRight
				disabled={pristine || submitting}
			>
				Save MQTT configuration and <b>REBOOT</b>
						</Button>
			<div className='clearfix'></div>
		</form>
	)
}

export default reduxForm({
	form: 'simple', // a unique identifier for this form
})(MQTTForm)
