
import React, { Component } from 'react'
import ChartistGraph from 'react-chartist'
import { Grid, Row, Col } from 'react-bootstrap'
import { SimpleCard } from 'components/Card/SimpleCard.jsx'
import SensorCard from '../../components/Card/SensorCard'
import { SwitchCard } from '../../components/Card/SwitchCard'
import DonutChart from '../../components/DonutChart/DonutChart'

class CO2Control extends Component {

	render() {
		const { getSystemDevice,
			switchSystemDeviceState,
			getSensorDevice,
		} = this.props

		return (
			<div className="content">
				<Grid fluid>
					<Row>
						<Col lg={6} md={6} sm={12}>
							<SensorCard title={<span>CO<sub>2</sub></span>}
								titleIcon={<i className="fa fa-cloud text-info"></i>}
							>
								<DonutChart color='dark-blue' value={getSensorDevice('co2').concentration || 0} />
							</SensorCard>
						</Col>
						<Col lg={6} md={6} sm={12} >
							<SwitchCard title="Fan"
								iconClass="fa-empire"
								active={getSystemDevice('fan').active}
								onClick={() => switchSystemDeviceState('fan')}
							/>
						</Col>
					</Row>

					<Row>
						<Col md={12}>
							<SimpleCard ctHeight="3"
								statsIcon="fa fa-history"
								id="chartHours"
								title="CO2 concentration"
								category={avrgCO2(getSensorDevice('co2').history) + '% average'}
								stats="50% is ok"
							>
								<div className="ct-chart">
									<ChartistGraph
										data={{
											labels: [...Array(getSensorDevice('co2').history ? getSensorDevice('co2').history.length : 0).keys()],
											series: [
												getSensorDevice('co2').history ? getSensorDevice('co2').history : []
											]
										}
										}
										type="Line"
										options={{
											low: 0,
											high: 100,
											onlyInteger: true,
											showArea: true
										}}
									/>
								</div>
							</SimpleCard>
						</Col>
					</Row>
				</Grid>
			</div >
		)
	}
}

const avrgCO2 = (history) => {
	return history ? Math.round(history.reduce(function (a, b) { return a + b }) / history.length) : 0
}

export default CO2Control
