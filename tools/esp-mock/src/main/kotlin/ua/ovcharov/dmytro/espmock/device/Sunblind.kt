package ua.ovcharov.dmytro.espmock.device

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

class Sunblind(var currentPosition: Int ,@get: NotNull @get: Min(0) @get: Max(100) var targetPosition: Int) : Device
