package ua.ovcharov.dmytro.espmock.mqtt

import com.fasterxml.jackson.annotation.JsonProperty
import ua.ovcharov.dmytro.espmock.device.Device

class DeviceResponse(@JsonProperty("d") val device: Device)