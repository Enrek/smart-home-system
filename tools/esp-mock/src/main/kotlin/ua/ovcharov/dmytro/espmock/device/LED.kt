package ua.ovcharov.dmytro.espmock.device

import javax.validation.Valid
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

class LED(@get: Valid @get: NotNull var ledItems: MutableList<LEDItem>) : Device

class LEDItem(@get: NotNull val sector: Int, @get: Valid @get: NotNull var color: Color)

class Color(@get: NotNull @get: Max(255) @get: Min(0) val red: Int,
            @get: NotNull @get: Max(255) @get: Min(0) val green: Int,
            @get: NotNull @get: Max(255) @get: Min(0) val blue: Int)