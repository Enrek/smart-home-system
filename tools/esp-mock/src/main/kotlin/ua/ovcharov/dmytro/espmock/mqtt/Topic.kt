package ua.ovcharov.dmytro.espmock.mqtt

class Topic(val uri: String, val qos: Int = 0)