package ua.ovcharov.dmytro.espmock.device

import com.fasterxml.jackson.annotation.JsonIgnore

class Fan(var active: Boolean = false) : Device

class Fire(active: Boolean = true, var alarm: Boolean = false) : Device {
    var active: Boolean = active
        set(value) {
            alarm = if(!value) false else alarm
            field = value
        }
}

class Security(active: Boolean = false, @JsonIgnore var validIDs: List<String> = arrayListOf(), var alarm: Boolean = false) : Device {
    var active: Boolean = active
        set(value) {
            alarm = if(!value) false else alarm
            field = value
        }
}


class Conditioner(var active: Boolean = false) : Device

class Heating(var active: Boolean = false) : Device

class Socket(var active: Boolean = false) : Device
