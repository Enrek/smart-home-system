package ua.ovcharov.dmytro.espmock.interceptor

import javax.servlet.http.HttpServletResponse
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import ua.ovcharov.dmytro.espmock.esp.FakeSlaves
import javax.servlet.http.HttpServletRequest


class UpdateInterceptor(val fakeSlaves: FakeSlaves) : HandlerInterceptorAdapter() {

    override fun preHandle(request: HttpServletRequest,
                  response: HttpServletResponse, handler: Any): Boolean {
        fakeSlaves.updateData()
        return super.preHandle(request, response, handler)
    }



}