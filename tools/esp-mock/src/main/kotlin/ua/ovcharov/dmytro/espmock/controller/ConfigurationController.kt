package ua.ovcharov.dmytro.espmock.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import ua.ovcharov.dmytro.espmock.device.Configuration
import ua.ovcharov.dmytro.espmock.device.ConfigurationMQTT
import ua.ovcharov.dmytro.espmock.mqtt.DeviceRequest
import ua.ovcharov.dmytro.espmock.mqtt.DeviceResponceBuilder
import ua.ovcharov.dmytro.espmock.mqtt.DeviceResponse

import javax.validation.Valid


@RestController
@RequestMapping("/api/device/configuration/")
class ConfigurationController(
        val deviceResponceBuilder: DeviceResponceBuilder,
        val configuration: Configuration) {

    @RequestMapping("/mqtt")
    fun temp(): DeviceResponse {
        return deviceResponceBuilder.buildDeviceResponse(configuration.configurationMQTT)
    }

    @RequestMapping(method = [RequestMethod.POST], path = ["/mqtt"])
    fun updateSunblindTargetPosition(@Valid @RequestBody deviceRequest: DeviceRequest<ConfigurationMQTT>): ResponseEntity<Any> {
        configuration.configurationMQTT = deviceRequest.device
        return ResponseEntity.ok(deviceResponceBuilder.buildDeviceResponse(configuration.configurationMQTT))
    }

}