package ua.ovcharov.dmytro.espmock.mqtt

import org.springframework.stereotype.Service
import ua.ovcharov.dmytro.espmock.device.Device

@Service
class DeviceResponceBuilder() {
    fun buildDeviceResponse(device: Device): DeviceResponse {
        return DeviceResponse(device)
    }
}