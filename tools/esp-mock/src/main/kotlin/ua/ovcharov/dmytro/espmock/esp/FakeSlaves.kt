package ua.ovcharov.dmytro.espmock.esp

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ua.ovcharov.dmytro.espmock.data.DataGenerator
import ua.ovcharov.dmytro.espmock.device.*
import java.util.*


@Component
class FakeSlaves constructor(@Autowired val data: DataGenerator) {
    companion object {
        const val sunblindStep = 10
        const val sectorsCount = 4
    }

    val rnd: Random = Random()
    val fan: Fan = Fan()
    val security: Security = Security()
    val fire: Fire = Fire()
    val heating: Heating = Heating()
    val conditioner: Conditioner = Conditioner()
    val socket: Socket = Socket()
    val led: LED = LED((0 until sectorsCount).map { index -> LEDItem(index, Color(0, 0, 0)) }.toMutableList())
    val tempSensor: TempSensor = TempSensor()
    val CO2Sensor: CO2Sensor = CO2Sensor()
    val PIRSensors: Array<PIRSensor> =  Array(2 ) {PIRSensor()}
    val solarPanelSensor: SolarPanelSensor = SolarPanelSensor()
    val sunblind: Sunblind = Sunblind(0, 0)


    fun updateData() {
        security.alarm = if (security.active) rnd.nextBoolean() else false
        fire.alarm = if (fire.active) rnd.nextBoolean() else false
        PIRSensors.forEach { it.movement = rnd.nextBoolean() }
        tempSensor.temperature = data.getCurrentTemp()
        CO2Sensor.concentration = data.getCurrentCO2()
        solarPanelSensor.voltage = data.getCurrentVoltage()
        updateSunblindPos()
    }

    fun updateSunblindPos() {
        val delta = sunblind.targetPosition - sunblind.currentPosition
        if (delta == 0)
            return
        sunblind.currentPosition += if (Math.abs(delta) < sunblindStep) delta else Integer.signum(delta) * sunblindStep
    }
}



