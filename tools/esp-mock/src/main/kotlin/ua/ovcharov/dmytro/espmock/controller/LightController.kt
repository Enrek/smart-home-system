package ua.ovcharov.dmytro.espmock.controller


import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import ua.ovcharov.dmytro.espmock.device.LED
import ua.ovcharov.dmytro.espmock.device.Sunblind
import ua.ovcharov.dmytro.espmock.esp.FakeSlaves
import ua.ovcharov.dmytro.espmock.mqtt.DeviceRequest
import ua.ovcharov.dmytro.espmock.mqtt.DeviceResponceBuilder
import ua.ovcharov.dmytro.espmock.mqtt.DeviceResponse
import javax.validation.Valid


@RestController
@RequestMapping("/api/device/light/")
class LightController(
        val deviceResponceBuilder: DeviceResponceBuilder,
        val slaves: FakeSlaves) {

    @RequestMapping("/led")
    fun led(): DeviceResponse {
        return deviceResponceBuilder.buildDeviceResponse(slaves.led)
    }

    @RequestMapping("/sunblind")
    fun sunblind(): DeviceResponse {
        return deviceResponceBuilder.buildDeviceResponse(slaves.sunblind)
    }

    @RequestMapping(method = [RequestMethod.POST], path = ["/sunblind"])
    fun updateSunblindTargetPosition(@Valid @RequestBody deviceRequest: DeviceRequest<Sunblind>): ResponseEntity<Any> {
        slaves.sunblind.targetPosition = deviceRequest.device.targetPosition
        return ResponseEntity.ok(deviceResponceBuilder.buildDeviceResponse(slaves.sunblind))
    }

    @RequestMapping(method = [RequestMethod.POST], path = ["/led"])
    fun updateLedColor(@Valid @RequestBody deviceRequest: DeviceRequest<LED>): ResponseEntity<Any> {
        val led =  deviceRequest.device
        if(led.ledItems.filter { ledItem -> ledItem.sector >= 0 && ledItem.sector < slaves.led.ledItems.size}.size != led.ledItems.size ) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapOf("cause" to "Wrong sector index"))
        }
        led.ledItems.forEach {
            ledItem -> slaves.led.ledItems[ledItem.sector] = ledItem
        }
        return ResponseEntity.ok(deviceResponceBuilder.buildDeviceResponse(slaves.led))
    }

}
