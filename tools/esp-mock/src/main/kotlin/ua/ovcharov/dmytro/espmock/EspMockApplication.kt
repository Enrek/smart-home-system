package ua.ovcharov.dmytro.espmock

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
import ua.ovcharov.dmytro.espmock.esp.FakeSlaves
import ua.ovcharov.dmytro.espmock.interceptor.UpdateInterceptor


@SpringBootApplication
open class EspMockApplication(@Autowired val fakeSlaves: FakeSlaves) : WebMvcConfigurerAdapter() {
    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(UpdateInterceptor(fakeSlaves))
    }
}


fun main(args: Array<String>) {
    SpringApplication.run(EspMockApplication::class.java, *args)
}
