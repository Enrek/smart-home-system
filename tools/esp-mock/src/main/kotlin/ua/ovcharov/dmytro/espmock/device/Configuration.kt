package ua.ovcharov.dmytro.espmock.device

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class Configuration(var configurationMQTT: ConfigurationMQTT)


@Component

class ConfigurationMQTT(@field:Value("\${configuration.mqtt.clientName}") val clientName: String = "",
                        @field:Value("\${configuration.mqtt.host}") val host: String = "",
                        @field:Value("\${configuration.mqtt.qos}") val qos: Int = 2,
                        @field:Value("\${configuration.mqtt.username}") val username: String = "",
                        @field:Value("\${configuration.mqtt.password}") val password: String = "",
                        @field:Value("\${configuration.mqtt.timeout}") val timeout: Int = 5) : Device