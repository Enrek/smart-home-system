package ua.ovcharov.dmytro.espmock.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ua.ovcharov.dmytro.espmock.esp.FakeSlaves
import ua.ovcharov.dmytro.espmock.mqtt.DeviceResponceBuilder
import ua.ovcharov.dmytro.espmock.mqtt.DeviceResponse


@RestController
@RequestMapping("/api/device/sensor/")
class SensorController(
        val deviceResponceBuilder: DeviceResponceBuilder,
        val slaves: FakeSlaves) {

    @RequestMapping("/temp")
    fun temp(): DeviceResponse {
        return deviceResponceBuilder.buildDeviceResponse(slaves.tempSensor)
    }

    @RequestMapping("/co2")
    fun co2(): DeviceResponse {
        return deviceResponceBuilder.buildDeviceResponse(slaves.CO2Sensor)

    }

    @RequestMapping("/solar")
    fun solar(): DeviceResponse {
        return deviceResponceBuilder.buildDeviceResponse(slaves.solarPanelSensor)
    }

    @RequestMapping("/pir/{id}")
    fun pir(@PathVariable id: Int): ResponseEntity<Any> {
        if (id >= slaves.PIRSensors.size)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapOf("cause" to "Wrong PIR id"))
        return ResponseEntity.ok(deviceResponceBuilder.buildDeviceResponse(slaves.PIRSensors[id]))
    }
}