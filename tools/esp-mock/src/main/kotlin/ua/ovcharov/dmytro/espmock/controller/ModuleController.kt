package ua.ovcharov.dmytro.espmock.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ua.ovcharov.dmytro.espmock.device.Device
import ua.ovcharov.dmytro.espmock.esp.FakeSlaves
import ua.ovcharov.dmytro.espmock.mqtt.DeviceRequest
import ua.ovcharov.dmytro.espmock.mqtt.DeviceResponceBuilder
import javax.validation.Valid
import javax.validation.constraints.NotNull


@RestController
@RequestMapping("/api/device/module/")
class ModuleController(
        val deviceResponceBuilder: DeviceResponceBuilder,
        val slaves: FakeSlaves) {

    class DeviceWithStatus(@NotNull val active: Boolean) : Device

    @RequestMapping(method = [RequestMethod.GET], path = ["/{module}"])
    fun getModule(@PathVariable module: String): ResponseEntity<Any> = responseForModule(module)

    @CrossOrigin
    @RequestMapping(method = [RequestMethod.POST], path = ["/{module}"])
    fun updateModuleState(@PathVariable module: String, @Valid @RequestBody request: DeviceRequest<DeviceWithStatus>): ResponseEntity<Any> {
        val deviceWithStatus = request.device
        when (module) {
            "socket" -> slaves.socket.active = deviceWithStatus.active
            "fan" -> slaves.fan.active = deviceWithStatus.active
            "security" -> slaves.security.active = deviceWithStatus.active
            "fire" -> slaves.fire.active = deviceWithStatus.active
            "heating" -> slaves.heating.active = deviceWithStatus.active
            "conditioner" -> slaves.conditioner.active = deviceWithStatus.active
        }
        return responseForModule(module)
    }

    private fun responseForModule(module: String): ResponseEntity<Any> {
        return when (module) {
            "socket" -> ResponseEntity.ok(deviceResponceBuilder.buildDeviceResponse(slaves.socket))
            "fan" -> ResponseEntity.ok(deviceResponceBuilder.buildDeviceResponse(slaves.fan))
            "security" -> ResponseEntity.ok(deviceResponceBuilder.buildDeviceResponse(slaves.security))
            "fire" -> ResponseEntity.ok(deviceResponceBuilder.buildDeviceResponse(slaves.fire))
            "heating" -> ResponseEntity.ok(deviceResponceBuilder.buildDeviceResponse(slaves.heating))
            "conditioner" -> ResponseEntity.ok(deviceResponceBuilder.buildDeviceResponse(slaves.conditioner))
            else -> ResponseEntity.badRequest().body(mapOf("cause" to "Wrong module name"))
        }
    }
}