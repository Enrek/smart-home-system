package ua.ovcharov.dmytro.espmock.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api/mgmt/initiate/device")
class SystemController() {

    @RequestMapping(method = [RequestMethod.POST], path = ["/reboot"])
    fun reboot(): ResponseEntity<Any> {
        return ResponseEntity.status(200).build()
    }

}