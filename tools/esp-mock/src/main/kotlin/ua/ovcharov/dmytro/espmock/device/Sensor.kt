package ua.ovcharov.dmytro.espmock.device

class TempSensor(var temperature: Int = 0) : Device

class CO2Sensor(var concentration: Int = 0) : Device

class SolarPanelSensor(var voltage: Int = 0) : Device

class PIRSensor(var movement: Boolean = false) : Device