package ua.ovcharov.dmytro.espmock.data

import org.springframework.stereotype.Component
import java.util.*

@Component
class DataGenerator {

    class DataRoad(val road: IntArray) {
        private var index: Int = 0
            get() {
                return random.nextInt(road.size-1)
            }
        fun getNextData() = road[index]

    }
    val tempRoad = DataRoad(intArrayOf(10, 20, 25, 30, 25, 40, 2, 9, 12, 7, 15, 23))
    val co2Road = DataRoad(intArrayOf(10, 40, 80, 25, 50, 82, 46, 23, 69))

    fun getCurrentTemp(): Int = tempRoad.getNextData()
    fun getCurrentCO2(): Int = co2Road.getNextData()
    fun getCurrentVoltage(): Int = random.nextInt(5)

    companion object {
        @JvmField
        val random: Random = Random()
    }
}