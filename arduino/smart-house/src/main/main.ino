#include "ErrorBus.h"
#include "LED.h"
#include "Sunblind.h"
#include "StepperDC.h"
#include "PIR.h"
#include <Modbus.h>
#include <ModbusSerial.h>
#include <Wire.h>;

#define debug(...) //Serial.println(__VA_ARGS__); 

// Modbus
#define UPDATE_INTERVAL 1000
#define SLAVE_ID 1
#define BAUDRATE 9600
#define WAIT_INIT_STATE 60 //sec

#define I2C_SLAVE_ID 42

//ID already shifted << 6
#define LED_ID 64
#define RFID_ID 128
#define POT_ID 192

#define TURN_ON 1
#define TURN_OFF 0
#define READ B11

#define CO2_SENSOR 0
#define TEMPERATURE_SENSOR 2
#define TIME 1

#define GAS_VALVE 3
#define ELECTRICAL_RELAY 4
#define SECURITY_ALARM_ACTIVITY_IDICATOR 5
#define SOCKET 0
#define HEATING 1
#define CONDITIONER 2

#define ON true
#define OFF false

#define NIGHT 0
#define EVENING 1
#define DAY 2

#define ALL 0
#define FIRST_FLOOR 1
#define SECOND_FLOOR 2

#define SUNBLIND_OPEN 100
#define SUNBLIND_CLOSE 0

#define FAN 5

#define ATTEMPTS_COUNT 200

//#define CALIBRATE

bool standalone = false;

ErrorBus errorBus;
const byte sectors[] = {5, 3, 5, 4};
const byte sectorCount = 4;
const byte ledPin = 6;
const byte ledCount = 17;
LED led(sectors, sectorCount, ledPin, errorBus);

const byte additionalPins = 16;
const byte dataPin = 9;
const byte clockPin = 7;
const byte latchPin = 8;
const byte platformDigitalPinsNum = 13;


const int stepsPerRevolution = 4095;
const byte speed = 20;
StepperDC sunblindStepper(stepsPerRevolution, 9, 10, 11, 12);

const int perMove = 15800;
const int stateAddress = 2;
const byte sensorPin = 2;
Sunblind sunblind(sunblindStepper, perMove / 100, stateAddress, sensorPin);

const byte MAX_GAS_VALUE = 85;
const byte MAX_TEMP_VALUE = 45;
bool fireAlarmSystemActive;
bool fireAlarmSignalActive;

const unsigned long VALID_RFID = 2058868779;
unsigned long tempRFID = 0;
bool securityAlarmSystemActive;
bool securityAlarmSignalActive;

PIR firstFloorPIR(4);
PIR secondFloorPIR(3);
bool firstFloorMovement, secondFloorMovement, newMovement;

const byte SOLAR_PANEL_PIN = A0;

bool singleMode;
bool lightsOn;
byte firstFloorLightTimer;
byte secondFloorLightTimer;
const uint32_t BLACK_COLOR =  led.packColor(0, 0, 0);
const uint32_t WHITE_COLOR =  led.packColor(255, 255, 255);
const uint32_t RED_COLOR =  led.packColor(255, 0, 0);
const uint32_t GREEN_COLOR =  led.packColor(0, 255, 0);
const byte LIGHT_TIMER = 100;

const byte NORMAL_CO2_VALUE = 50;
const byte COLD_TEMP_VALUE = 0;
const byte HOT_TEMP_VALUE = 20;
bool conditionerActive;
bool heatingActive;
bool ventilationActive;

int readAnswerAttempts = 0;

//Modbus
ModbusSerial mb;
uint16_t passedWaitLoops;
const byte COILS_COUNT = 6; // 0 - device coil 1 - FAN 2 - LED mode 3 - heating 4 - coditioner 5 - socket
const byte COILS_ARRAY_LENGTH = COILS_COUNT / 8 + 1;
byte coils[COILS_ARRAY_LENGTH];

const byte HOLDING_REGISTERS_COUNT = 1 + sectorCount * 4 + ledCount * 4;
uint16_t holdingRegisters[HOLDING_REGISTERS_COUNT];

const byte PIR_SENSORS_COUNT = 2;
const PIR PIR_SENSORS[] = {firstFloorPIR, secondFloorPIR};
const byte DISCRETE_INPUT_COUNT = PIR_SENSORS_COUNT; // 0-N - PIR

const byte ANALOG_INPUT_REGISTERS_COUNT = 7; // 0 - solar panel; 1 - sunblind current pos; 2 - temp; 3 - CO2;  4 - time; 5 - RFID head; 6 - RFID tail.

void setup() {

  sunblindStepper.setSpeed(speed);
  pinMode(FAN, OUTPUT);
  led.init();
  changeLight(ALL, OFF);
  mb.config(&Serial, 115200, SERIAL_8N1);
  mb.setSlaveId(SLAVE_ID);
//sunblind.setPosition(SUNBLIND_CLOSE);
  debug("Calibration");
#ifdef CALIBRATE
  sunblind.calibrate();
#endif
  if (sunblind.readCurrentPosition() > 100) {
    sunblind.calibrate();
  }
  debug("Calibration done");
  Wire.setClock(400000L);
  Wire.begin();

  delay(2000);
  debug("Synchronization");
  sync();
  //
  //  if(!standalone) {
  //    debug("Initialize modbus for sync");
  //    slave.cbVector[CB_READ_COILS] = illegalFunction;
  //    slave.cbVector[CB_WRITE_COIL] = writeDigitlOut;
  //    slave.cbVector[CB_READ_REGISTERS] = illegalFunction;
  //    slave.cbVector[CB_WRITE_MULTIPLE_REGISTERS] = illegalFunction;
  //    slave.begin(BAUDRATE);
  //    debug("Wait initial call from modbus master");
  //    do {
  //      slave.poll();
  //      passedWaitLoops++;
  //      delay(5);
  //    } while(passedWaitLoops < WAIT_INIT_STATE * 100 && coils[0] == 0);
  //    standalone = coils[0] == 0;
  //    debug("Modbus active: ");
  //    debug(!standalone);
  //  }

  if (!standalone) {
    initModbus();
  }

  greenBlink();
  debug("SETUP - DONE");
}

//const byte DEVICE_STATE_COIL = 0;
const byte DEVICE_FAN_COIL = 0;
const byte DEVICE_LED_MODE_COIL = 1;
const byte DEVICE_HEATING_COIL = 2;
const byte DEVICE_CONDITIONER_COIL = 3;
const byte DEVICE_SOCKET_COIL = 4;
const byte DEVICE_SECURITY_COIL = 5;
const byte DEVICE_FIRE_COIL = 6;

const byte DEVICE_IR_FF_DIGITAL_IN = 0;
const byte DEVICE_IR_SF_DIGITAL_IN = 1;
const byte DEVICE_SECURITY_ALARM_DIGITAL_IN = 2;
const byte DEVICE_FIRE_ALARM_DIGITAL_IN = 3;

const byte DEVICE_SOLAR_ANALOG_IN = 0;
const byte DEVICE_SUNBLIND_ANALOG_IN = 1;
const byte DEVICE_TEMP_ANALOG_IN = 2;
const byte DEVICE_CO2_ANALOG_IN = 3;
const byte DEVICE_TIME_ANALOG_IN = 4;
const byte DEVICE_RFID_HEAD_ANALOG_IN = 5;
const byte DEVICE_RFID_TAIL_ANALOG_IN = 6;

const byte DEVICE_SUNBLIND_REGISTER = 0;
const byte DEVICE_LED_REGISTER_OFFSET = 1;

void initModbus() {
  //    mb.addCoil(DEVICE_STATE_COIL);
  mb.addCoil(DEVICE_FAN_COIL);
  mb.addCoil(DEVICE_LED_MODE_COIL);
  mb.addCoil(DEVICE_HEATING_COIL);
  mb.addCoil(DEVICE_CONDITIONER_COIL);
  mb.addCoil(DEVICE_SOCKET_COIL);
  mb.addCoil(DEVICE_SECURITY_COIL);
  mb.addCoil(DEVICE_FIRE_COIL, 1);

  mb.addIsts(DEVICE_IR_FF_DIGITAL_IN);
  mb.addIsts(DEVICE_IR_SF_DIGITAL_IN);
  mb.addIsts(DEVICE_SECURITY_ALARM_DIGITAL_IN);
  mb.addIsts(DEVICE_FIRE_ALARM_DIGITAL_IN);

  mb.addIreg(DEVICE_SOLAR_ANALOG_IN);
  mb.addIreg(DEVICE_SUNBLIND_ANALOG_IN);
  mb.addIreg(DEVICE_TEMP_ANALOG_IN);
  mb.addIreg(DEVICE_CO2_ANALOG_IN);
  mb.addIreg(DEVICE_TIME_ANALOG_IN);

  mb.addHreg(DEVICE_SUNBLIND_REGISTER);
  for (int i = 0; i < sectorCount; i++) {
    mb.addHreg(i * 3 + DEVICE_LED_REGISTER_OFFSET);
    mb.addHreg(i * 3 + 1 + DEVICE_LED_REGISTER_OFFSET);
    mb.addHreg(i * 3 + 2 + DEVICE_LED_REGISTER_OFFSET);
  }
  //  slave.cbVector[CB_READ_COILS] = readDigitalIn;
  //  slave.cbVector[CB_WRITE_COIL] = writeDigitlOut;
  //  slave.cbVector[CB_READ_REGISTERS] = readMemory;
  //  slave.cbVector[CB_WRITE_MULTIPLE_REGISTERS] = writeMemory;
}

void sync() {
  debug("Sync call to slave");
  if (sendRequestToSlale(0) == 0) {
    debug("Sync finish");
    return;
  }
  sync();
}



void loop() {
  if (standalone) {
    standaloneLoop();
  } else {
    updateState();
    mb.task();
    postProcess();
  }
}

unsigned long lastUpdateTime = 0;

void updateState() {
  //  if(millis() - lastUpdateTime < UPDATE_INTERVAL) {
  //    return;
  //  }
  // lastUpdateTime = millis();

  // 1 DEV LEVEL
  int temp = convertTemp(readSensorValue(TEMPERATURE_SENSOR));
  int co2 = readSensorValue(CO2_SENSOR);

  mb.Ireg(DEVICE_TEMP_ANALOG_IN, temp);
  mb.Ireg(DEVICE_CO2_ANALOG_IN, co2);
  updatePIRStatusesModbus();

  //////
  //Check alarm systems
  ////////
  fireAlarmSystemActive = mb.Coil(DEVICE_FIRE_COIL);
  cahngeSecurityAlarmSystemState(mb.Coil(DEVICE_SECURITY_COIL));

  //Check fire alarm system
  if (fireAlarmSystemActive && (co2 > MAX_GAS_VALUE || temp > MAX_TEMP_VALUE)) {
    cahngeFireAlarmSignalState(ON);
    mb.Ists(DEVICE_FIRE_ALARM_DIGITAL_IN, ON);
  } else {
    cahngeFireAlarmSignalState(OFF);
    mb.Ists(DEVICE_FIRE_ALARM_DIGITAL_IN, OFF);
  }

  //Check security alarm system


  tempRFID = readRFID();
  if (tempRFID != 0) {
    if (tempRFID == VALID_RFID) {
      cahngeSecurityAlarmSystemState(!securityAlarmSystemActive);
      sunblind.setPosition(SUNBLIND_CLOSE);
      mb.Hreg(DEVICE_SUNBLIND_REGISTER, SUNBLIND_CLOSE);
      mb.Ists(DEVICE_SECURITY_ALARM_DIGITAL_IN, OFF);
      mb.Coil(DEVICE_SECURITY_COIL, securityAlarmSystemActive);
      greenBlink();
    } else {
      redBlink();
    }
  }

  if (securityAlarmSignalActive) {
    turnOffAllSubsystemsModbus();
    return;
  }

  if ((firstFloorMovement || secondFloorMovement) && newMovement && securityAlarmSystemActive) {
    securityAlarmSignalActive = true;
    mb.Ists(DEVICE_SECURITY_ALARM_DIGITAL_IN, ON);
  } else {
    mb.Ists(DEVICE_SECURITY_ALARM_DIGITAL_IN, OFF);
  }

  if (fireAlarmSignalActive || securityAlarmSystemActive) {
    turnOffAllSubsystemsModbus();
    return;
  }

  //2 DEV. LEVEL

  //  standalone = !mb.Coil(DEVICE_STATE_COIL);
  changeVentilationSystemState(mb.Coil(DEVICE_FAN_COIL));
  singleMode = mb.Coil(DEVICE_LED_MODE_COIL);
  changeHeatingSystemState(mb.Coil(DEVICE_HEATING_COIL));
  changeConditionerSystemState(mb.Coil(DEVICE_CONDITIONER_COIL));
  changeSoketState(mb.Coil(DEVICE_SOCKET_COIL));

  mb.Ireg(DEVICE_SOLAR_ANALOG_IN, analogRead(A0));
  mb.Ireg(DEVICE_SUNBLIND_ANALOG_IN, sunblind.getCurrentPosition());
  mb.Ireg(DEVICE_TIME_ANALOG_IN, readSensorValue(TIME));

  sunblind.setPosition(mb.Hreg(DEVICE_SUNBLIND_REGISTER));
  updateSectors();
}


void updatePIRStatusesModbus() {
  updatePIRStatuses();
  if (millis() - lastUpdateTime < 5000) {
    if (firstFloorMovement) mb.Ists(DEVICE_IR_FF_DIGITAL_IN, firstFloorMovement);
    if (secondFloorMovement) mb.Ists(DEVICE_IR_SF_DIGITAL_IN, secondFloorMovement);
    return;
  }
  if ((firstFloorMovement || secondFloorMovement) && newMovement)
    lastUpdateTime = millis();
  mb.Ists(DEVICE_IR_FF_DIGITAL_IN, firstFloorMovement);
  mb.Ists(DEVICE_IR_SF_DIGITAL_IN, secondFloorMovement);

}

void turnOffAllSubsystemsModbus() {
  changeLight(ALL, OFF);
  for (int i = 0; i < sectorCount; i++) {
    mb.Hreg(i * 3 + DEVICE_LED_REGISTER_OFFSET, 0);
    mb.Hreg(i * 3 + 1 + DEVICE_LED_REGISTER_OFFSET, 0);
    mb.Hreg(i * 3 + 2 + DEVICE_LED_REGISTER_OFFSET, 0);
  }
  mb.Coil(DEVICE_FAN_COIL, OFF);
  mb.Coil(DEVICE_HEATING_COIL, OFF);
  mb.Coil(DEVICE_CONDITIONER_COIL, OFF);
  mb.Coil(DEVICE_SOCKET_COIL, OFF);
}


// DEVICE CONTROLL

void standaloneLoop() {
  process();
  postProcess();
}

void process() {

  int temp = convertTemp(readSensorValue(TEMPERATURE_SENSOR));
  int co2 = readSensorValue(CO2_SENSOR);
  //Check fire alarm system
  if (co2 > MAX_GAS_VALUE || temp > MAX_TEMP_VALUE) {
    cahngeFireAlarmSignalState(ON);
    return;
  }
  cahngeFireAlarmSignalState(OFF);

  //Check security alarm system
  tempRFID = readRFID();
  if (tempRFID != 0) {
    if (tempRFID == VALID_RFID) {
      cahngeSecurityAlarmSystemState(!securityAlarmSystemActive);
      sunblind.setPosition(SUNBLIND_CLOSE);
      greenBlink();
    } else {
      redBlink();
    }
  }
  if (securityAlarmSignalActive) {
    return;
  }

  //Check PIR
  updatePIRStatuses();

  if ((firstFloorMovement || secondFloorMovement) && newMovement && securityAlarmSystemActive) {
    securityAlarmSignalActive = true;
    return;
  }

  if (securityAlarmSystemActive) {
    return;
  }

  //Time check
  byte currentTime = getTime();
  if (currentTime == NIGHT) {
    if (lightsOn) {
      changeLight(ALL, OFF);
    }
    processNightMovement(firstFloorMovement, FIRST_FLOOR, &firstFloorLightTimer);
    processNightMovement(secondFloorMovement, SECOND_FLOOR, &secondFloorLightTimer);
  }

  if (currentTime == EVENING) {
    if (!lightsOn) {
      changeLight(ALL, ON);
    }
  }

  sunblind.setPosition(SUNBLIND_CLOSE);

  if (currentTime == DAY) {
    if (lightsOn || firstFloorLightTimer != 0 || secondFloorLightTimer != 0) {
      changeLight(ALL, OFF);
    }
    sunblind.setPosition(SUNBLIND_OPEN);
  }

  if (temp > HOT_TEMP_VALUE) {
    changeConditionerSystemState(ON);
    changeVentilationSystemState(ON);
  } else {
    changeConditionerSystemState(OFF);
  }

  if (co2 > NORMAL_CO2_VALUE) {
    changeVentilationSystemState(ON);
  } else if (!conditionerActive) {
    changeVentilationSystemState(OFF);
  }

  if (temp < COLD_TEMP_VALUE) {
    changeHeatingSystemState(ON);
  } else {
    changeHeatingSystemState(OFF);
  }


}

void postProcess() {
  if (fireAlarmSignalActive || securityAlarmSignalActive) {
    redBlink();
    return;
  }
  sunblind.move();
}

void cahngeFireAlarmSignalState(bool state) {
  if (fireAlarmSignalActive == state) {
    return;
  }
  fireAlarmSignalActive = state;
  changeComponentState(GAS_VALVE, state);
  changeComponentState(ELECTRICAL_RELAY, state);
  turnOffSubSystems();
}

void cahngeSecurityAlarmSystemState(bool state) {
  if (securityAlarmSystemActive == state) {
    return;
  }
  if (state == OFF) {
    securityAlarmSignalActive = false;
  }
  securityAlarmSystemActive = state;
  changeComponentState(SECURITY_ALARM_ACTIVITY_IDICATOR, state);
  turnOffSubSystems();
}

void turnOffSubSystems() {
  changeSoketState(OFF);
  changeConditionerSystemState(OFF);
  changeHeatingSystemState(OFF);
  changeVentilationSystemState(OFF);
}

void changeConditionerSystemState(bool state) {
  if (conditionerActive == state) {
    return;
  }
  conditionerActive = state;
  changeComponentState(CONDITIONER, state);
}

void changeHeatingSystemState(bool state) {
  if (heatingActive == state) {
    return;
  }
  heatingActive = state;
  changeComponentState(HEATING, state);
}

void changeVentilationSystemState(bool state) {
  if (ventilationActive == state) {
    return;
  }
  ventilationActive = state;
  changeFanState(state);
}

void changeFanState(bool state) {
  digitalWrite(FAN, state);
}

void changeSoketState(bool state) {
  changeComponentState(SOCKET, state);
}

void changeLight(byte mode, bool state) {
  uint32_t color = state ? WHITE_COLOR : BLACK_COLOR;
  switch (mode) {
    case ALL:
      lightsOn = state;
      firstFloorLightTimer = 0;
      secondFloorLightTimer = 0;
      led.changeColorFull(color);
      break;
    case FIRST_FLOOR:
      firstFloorLightTimer = state ? LIGHT_TIMER : 0;
      led.changeColorInSector(0, color);
      led.changeColorInSector(1, color);
      break;
    case SECOND_FLOOR:
      secondFloorLightTimer = state ? LIGHT_TIMER : 0;
      led.changeColorInSector(2, color);
      led.changeColorInSector(3, color);
      break;
  }
}



void processNightMovement(bool pirValue, byte floorCode, byte *floorTimer) {
  if (newMovement) {
    if (pirValue) {
      if (*floorTimer == 0) {
        changeLight(floorCode, ON);
      } else {
        *floorTimer = LIGHT_TIMER;
      }
    }
  }

  if (*floorTimer > 0) {
    *floorTimer = *floorTimer - 1;
  } else {
    changeLight(floorCode, OFF);
  }
}


byte getTime() {
  int timeValue = readSensorValue(TIME);
  if (timeValue >= 6 && timeValue < 18)
    return DAY;
  if (timeValue >= 18 && timeValue <= 22)
    return EVENING;
  return NIGHT;
}

void updatePIRStatuses() {
  firstFloorMovement = firstFloorPIR.isMovmentDetected();
  secondFloorMovement = secondFloorPIR.isMovmentDetected();
  newMovement = firstFloorPIR.isNewMovement() || secondFloorPIR.isNewMovement();
}

void updateSectors() {
  for (int i = 0; i < sectorCount; i++) {
    led.changeColorInSector(i, led.packColor(mb.Hreg(i * 3 + DEVICE_LED_REGISTER_OFFSET), mb.Hreg(i * 3 + 1 + DEVICE_LED_REGISTER_OFFSET), mb.Hreg(i * 3 + 2 + DEVICE_LED_REGISTER_OFFSET)));
  }
}

int convertTemp(int temperatureValue) {
  return map(temperatureValue, 0, 100, 10, 60);
}

unsigned long readRFID() {
  return sendRequestToSlale(convertToCommand(RFID_ID, 0, READ));
}

int readSensorValue(byte componentId) {
  return sendRequestToSlale(convertToCommand(POT_ID, componentId, READ));
}

void changeComponentState(byte componentId, bool state) {
  do {
  } while (sendRequestToSlale(convertToCommand(LED_ID, componentId, state ? TURN_ON : TURN_OFF)) != 0);
}

byte convertToCommand(byte componentGroupID, byte componentID, byte componentFucntion) {
  return componentGroupID | componentID << 2 | componentFucntion;
}

unsigned long sendRequestToSlale(byte value) {
  bool longMode = value == convertToCommand(RFID_ID, 0, READ);
  int messageSize = longMode ? 4 : 1;
  debug("Send I2C message [" + String(messageSize) + "]");
  do {
    debug("Send to slave: ");
    debug(value);
    Wire.beginTransmission(I2C_SLAVE_ID );
    Wire.write(value);
  } while (Wire.endTransmission() != 0);
  int availableBytes =  Wire.requestFrom(I2C_SLAVE_ID , messageSize);
  if (availableBytes == messageSize) {
    if (longMode) {
      return processSlaveMessage32();
    } else {
      return processSlaveMessage4();
    }
  }
  debug("ERROR! Unexpected number of bytes received");
  debug(availableBytes);
  sendRequestToSlale(value);
}

unsigned long processSlaveMessage32() {
  byte buffer[4];
  buffer[0] = Wire.read();
  buffer[1] = Wire.read();
  buffer[2] = Wire.read();
  buffer[3] = Wire.read();
  unsigned long response = 0;
  response = buffer[0];
  response = (response << 8) | buffer[1];
  response = (response << 8) | buffer[2];
  response = (response << 8) | buffer[3];
  debug("Response from slave: ");
  debug(response);
  return response;
}

unsigned long processSlaveMessage4() {
  return Wire.read();
}

void redBlink() {
  led.changeColorFull(RED_COLOR);
  delay(300);
  changeLight(ALL, OFF);
}

void greenBlink() {
  lightsOn = false;
  led.changeColorFull(GREEN_COLOR);
  delay(300);
  changeLight(ALL, OFF);
}
