#include "ControllerAnalog.h"
#include "ControllerRFID.h"
#include "ControllerLedIndicators.h"
#include <Wire.h>

#define debug(...) Serial.println(__VA_ARGS__)

#define SLAVE_ID 42
#define RST_PIN         9  
#define SS_PIN          10

#define LED 1
#define RFID B10
#define POT B11

#define TURN_ON 1
#define TURN_OFF 0
#define READ B11

#define CONFIRMATION_BYTE  0xff

const byte pinToLed[] = {8,7,5,4,3,2}; 
const byte ledCount = 6;

const int analogInputToMappedOutput[][2] = {{A0,100},{A1,23},{A2,100}};
const byte analogInputNum = 3;
const int analogMaxValue = 720;

ErrorBus errorBus;
ControllerRFID *controllerRFID; 
ControllerLedIndicators controllerLedIndicators(pinToLed, ledCount, errorBus);
ControllerAnalog controllerAnalog(analogInputToMappedOutput, analogInputNum, analogMaxValue, errorBus);

int inputValue;
byte componentId;

unsigned long tempResult = 0;
bool resultAvailable = false;

const int RFID_DELAY = 10;
int loopsAfterLastNotEmptyRFID = RFID_DELAY;

bool sync = false;

byte command = 0;

void setup() {
  Serial.begin(9600); 
  debug("Setup start");
  controllerRFID = new ControllerRFID(RST_PIN, SS_PIN);
  controllerRFID->extractID();

  for(byte i = 0; i < ledCount; i++) {
    controllerLedIndicators.turnOnLed(i);
    delay(10); 
  }
  
  Wire.begin(SLAVE_ID);     
  Wire.onRequest(slavesRespond); 
  Wire.onReceive(receiveCommand);
}

//void sync() {
//   while(!Serial.available()) {
//    delay(2);
//  }
//
//  if(Serial.read() == 0) {
//    Serial.write(0);
//    return;
//  }
//  sync();
//}

void loop() {

  delay(10);
//  errorBus.removeError();
//  

//  
//    
//  processInputCommand(Serial.read());
//  if(errorBus.isErrorSet()) {
//    processError();
//    return;
//  }
//  
//  if(resultAvailable) {
//   Serial.println(tempResult);
//   resultAvailable = false;
//  } else {
//    Serial.write(0);
//  }
// 
}

void processInputCommand(byte command) {

  if(RFID_DELAY != loopsAfterLastNotEmptyRFID) { 
    loopsAfterLastNotEmptyRFID++;
  } else {
    controllerRFID->updateID();
  }
  
  errorBus.removeError();
  resultAvailable = true;
  tempResult = 0;
  debug("Command recived :");
  debug(command);
  if(command == 0) {
    for(byte i = 0; i < ledCount; i++) {
      controllerLedIndicators.turnOffLed(i);
      delay(10); 
    }
    return;
  }
  componentId = (command & 0x3C) >> 2;
  switch (command & 0xC3) { // clear component id part
    case POT << 6 | READ:
      debug("POTENTIOMETER");
      tempResult = controllerAnalog.readValue(componentId);
    break;
    case RFID << 6 | READ:
      debug("RFID");
      tempResult = controllerRFID->extractID();
      if(tempResult != 0) {
        loopsAfterLastNotEmptyRFID = 0;
      }
    break;
    case LED << 6 | TURN_ON:
      debug("LED ON");
      controllerLedIndicators.turnOnLed(componentId);
    break;
    case LED << 6 | TURN_OFF:
      debug("LED OFF");
      controllerLedIndicators.turnOffLed(componentId);
    break;
    default:
      errorBus.setError(ILLEGAL_FUNCTION);
    break;
  }
  if(errorBus.isErrorSet()) {
    processError();
  }
}

void receiveCommand(int howMany){
  command = Wire.read();
 
}
 
void slavesRespond(){
  debug("Response is requested");
  processInputCommand(command);
  unsigned long returnValue = 0;
  if(resultAvailable) {
    returnValue = tempResult;
    resultAvailable = false;
  } else {
    debug("ERROR! Can't return any result");   
    Wire.write(1);  
    return;
  }
  debug("Response to master: ");
  debug(tempResult);
  byte buffer[4];                 
  buffer[0] = (returnValue >> 24) & 0xFF;
  buffer[1] = (returnValue >> 16) & 0xFF;
  buffer[2] = (returnValue >> 8) & 0xFF;
  buffer[3] = returnValue & 0xFF;
  Wire.write(buffer, 4);
}
 

void processError() {
  debug("ERROR");
  debug(errorBus.getError());
  resultAvailable = false;
}



