#include "ErrorBus.h"
#include "DigitalController.h"
#include "LED.h"
#include "Sunblind.h"
#include "StepperDC.h"
#include "PIR.h"
#include <Wire.h>;


#define debug(...) Serial.println(__VA_ARGS__); 

#define SLAVE_ID 42
//ID already shifted << 6
#define LED_ID 64
#define RFID_ID 128
#define POT_ID 192

#define TURN_ON 1
#define TURN_OFF 0
#define READ B11

#define CO2_SENSOR 0
#define TEMPERATURE_SENSOR 2
#define TIME 1

#define GAS_VALVE 3
#define ELECTRICAL_RELAY 4
#define SECURITY_ALARM_ACTIVITY_IDICATOR 5
#define PLUG_SOCKET 0
#define CONDITIONER 2
#define HEATING 1

#define ON true
#define OFF false

#define NIGHT 0
#define EVENING 1
#define DAY 2

#define ALL 0
#define FIRST_FLOOR 1
#define SECOND_FLOOR 2

#define SUNBLIND_OPEN 100
#define SUNBLIND_CLOSE 0

#define FAN 23

#define ATTEMPTS_COUNT 200

//#define CALIBRATE

ErrorBus errorBus;
const byte sectors[] = {5,3,5,4}; 
const byte sectorCount = 4;
const byte ledPin = 6;
LED led(sectors,sectorCount,ledPin,errorBus);

const byte additionalPins = 16;
const byte dataPin = 9;
const byte clockPin = 7;
const byte latchPin = 8;
const byte platformDigitalPinsNum = 13;
DigitalController digitalController(additionalPins, dataPin, clockPin, latchPin, platformDigitalPinsNum, errorBus);

const int stepsPerRevolution = 4095;
const byte speed = 20;
StepperDC sunblindStepper(digitalController , stepsPerRevolution, 15, 16, 17, 18);

const int perMove = 2800;
const int stateAddress = 2;
const byte sensorPin = 2;
Sunblind sunblind(sunblindStepper, perMove/100, stateAddress, sensorPin);

const byte MAX_GAS_VALUE = 85;
const byte MAX_TEMP_VALUE = 45;
bool fireAlarmSignalActive = false;

const unsigned long VALID_RFID = 2058868779;
unsigned long tempRFID = 0;
bool securityAlarmSystemActive = false;
bool securityAlarmSignalActive = false;

PIR firstFloorPIR(4);
PIR secondFloorPIR(3);
bool firstFloorMovement, secondFloorMovement, newMovement;

bool lightsOn = false;
byte firstFloorLightTimer;
byte secondFloorLightTimer;
const uint32_t BLACK_COLOR =  led.packColor(0,0,0);
const uint32_t WHITE_COLOR =  led.packColor(255,255,255);
const uint32_t RED_COLOR =  led.packColor(255,0,0);
const uint32_t GREEN_COLOR =  led.packColor(0,255,0);
const byte LIGHT_TIMER = 100;

const byte NORMAL_CO2_VALUE = 50;
const byte COLD_TEMP_VALUE = 0;
const byte HOT_TEMP_VALUE = 25;
bool conditionerActive;
bool heatingActive;
bool ventilationActive;

int attempts = 0;



void setup() {
   Serial.begin(9600);
  debug("Start setup");
  sunblindStepper.setSpeed(speed);
  digitalController.resetAllPins();
  led.init();
  debug("Calibration");
  #ifdef CALIBRATE
    sunblind.calibrate();
  #endif
  if(sunblind.readCurrentPosition() > 100) {
    sunblind.calibrate();
  }
  debug("Calibration finish");
  delay(2500);
  Wire.begin();
  sync();
  greenBlink();
}

void sync() {
  debug("Sync call to slave");
  if(sendRequestToSlale(0) == 0) {
    debug("Sync finish");
    return;
  }
  sync();
}

void loop() {
  process();
  postProcess();
  delay(2);
}

void process() {

  int temp = convertTemp(readSensorValue(TEMPERATURE_SENSOR));
  int co2 = readSensorValue(CO2_SENSOR);
  //Check fire alarm system
  if(co2 > MAX_GAS_VALUE || temp > MAX_TEMP_VALUE) {
    cahngeFireAlarmSignalState(ON);
    return;
  }
  cahngeFireAlarmSignalState(OFF);
  
  //Check security alarm system
  tempRFID = readRFID();
  if(tempRFID != 0) {
    if(tempRFID == VALID_RFID) {
      cahngeSecurityAlarmSystemState(!securityAlarmSystemActive);
      sunblind.setPosition(SUNBLIND_CLOSE);
      greenBlink();
    } else {
      redBlink();
    }
  }
  if(securityAlarmSignalActive) {
    return;
  }

  //Check PIR
  updatePIRStatuses();
  
  if((firstFloorMovement || secondFloorMovement) && newMovement && securityAlarmSystemActive) {
       securityAlarmSignalActive = true;
       return;
  }
 
  if(securityAlarmSystemActive) {
    return;
  }
  
  //Time check
  byte currentTime = getTime();
  if(currentTime == NIGHT) {
    if(lightsOn) {
      changeLight(ALL,OFF);        
    }
    processNightMovement(firstFloorMovement, FIRST_FLOOR, &firstFloorLightTimer);
    processNightMovement(secondFloorMovement, SECOND_FLOOR, &secondFloorLightTimer);
  }

  if(currentTime == EVENING) {
    if(!lightsOn) {
      changeLight(ALL,ON); 
    }
  }
  
  sunblind.setPosition(SUNBLIND_CLOSE);
  
  if(currentTime == DAY) {
     if(lightsOn || firstFloorLightTimer != 0 || secondFloorLightTimer != 0) {
      changeLight(ALL,OFF);        
    }
    sunblind.setPosition(SUNBLIND_OPEN);
  }

  if(temp > HOT_TEMP_VALUE) {
    changeConditionerSystemState(ON);
    changeVentilationSystemState(ON);
  } else {
    changeConditionerSystemState(OFF);
  }
  
  if(co2 > NORMAL_CO2_VALUE) {
    changeVentilationSystemState(ON);
  } else if(!conditionerActive) {
    changeVentilationSystemState(OFF);
  }
  
  if(temp < COLD_TEMP_VALUE) {
    changeHeatingSystemState(ON);
  } else {
    changeHeatingSystemState(OFF);
  }


}

void postProcess() {
  if(fireAlarmSignalActive || securityAlarmSignalActive) {
    redBlink();
    return;
  }
  sunblind.move();
}
void cahngeFireAlarmSignalState(bool state) {
  if(fireAlarmSignalActive == state) {
    return;    
  }
  fireAlarmSignalActive = state;
  changeComponentState(GAS_VALVE, state);
  changeComponentState(ELECTRICAL_RELAY, state);
  turnOffSubSystems();
}

void cahngeSecurityAlarmSystemState(bool state) {
  if(securityAlarmSystemActive == state) {
    return;    
  }
  if(state == OFF) {
    securityAlarmSignalActive = false;
  }
  securityAlarmSystemActive = state;
  changeComponentState(SECURITY_ALARM_ACTIVITY_IDICATOR, state);
  turnOffSubSystems();
}

void turnOffSubSystems() {
  changeConditionerSystemState(OFF);
  changeHeatingSystemState(OFF);
  changeVentilationSystemState(OFF);
}

void changeConditionerSystemState(bool state) {
  if(conditionerActive == state) {
    return;
  }
  conditionerActive = state;
  changeComponentState(CONDITIONER, state);
}

void changeHeatingSystemState(bool state) {
  if(heatingActive == state) {
    return;
  }
  heatingActive = state;
  changeComponentState(HEATING, state);
}

void changeVentilationSystemState(bool state) {
  if(ventilationActive == state) {
    return;
  }
  ventilationActive = state;
  changeFanState(state);
}

void changeLight(byte mode, bool state) {
  uint32_t color = state ? WHITE_COLOR : BLACK_COLOR;
  switch (mode) {
    case ALL:
      lightsOn = state;
      firstFloorLightTimer = 0;
      secondFloorLightTimer = 0;
      led.changeColorFull(color);
    break;
    case FIRST_FLOOR:
      firstFloorLightTimer = state ? LIGHT_TIMER : 0;
      led.changeColorInSector(0,color);
      led.changeColorInSector(1,color);
    break;
    case SECOND_FLOOR:
      secondFloorLightTimer = state ? LIGHT_TIMER : 0;
      led.changeColorInSector(2,color);
      led.changeColorInSector(3,color);
    break;
  }
}

void processNightMovement(bool pirValue, byte floorCode, byte *floorTimer) {
    if(newMovement) {
     if(pirValue) {
        if(*floorTimer == 0) {
          changeLight(floorCode,ON);
        } else {
          *floorTimer = LIGHT_TIMER;
        }
      }
    }

    if(*floorTimer > 0) {
      *floorTimer = *floorTimer - 1;
    } else {
      changeLight(floorCode,OFF);
    }
}

//STANDALONE MODE

void changeFanState(bool state) {
  digitalController.setPin(FAN,state);
}

byte getTime() {
  int timeValue = readSensorValue(TIME);
  if(timeValue >= 6 && timeValue < 18)
    return DAY;
  if(timeValue >= 18 && timeValue <= 22)
    return EVENING;
  return NIGHT; 
}

void updatePIRStatuses() {
  firstFloorMovement = firstFloorPIR.isMovmentDetected();
  secondFloorMovement = secondFloorPIR.isMovmentDetected();
  newMovement = firstFloorPIR.isNewMovement() || secondFloorPIR.isNewMovement();
}

int convertTemp(int temperatureValue) {
  return map(temperatureValue, 0, 100, -40, 60);
}

unsigned long readRFID() {
  return sendRequestToSlale(convertToCommand(RFID_ID,0,READ));
}

int readSensorValue(byte componentId) {
  return sendRequestToSlale(convertToCommand(POT_ID,componentId,READ));
}

void changeComponentState(byte componentId, bool state) {
  do {
  } while(sendRequestToSlale(convertToCommand(LED_ID,componentId,state ? TURN_ON : TURN_OFF)) != 0);
}

byte convertToCommand(byte componentGroupID, byte componentID, byte componentFucntion) {
  return componentGroupID | componentID << 2 | componentFucntion;
}

//TODO: Possibility of optimization - only RFID needs 32bit respionse 
unsigned long sendRequestToSlale(byte value) {
  do {
    debug("Send to slave: ");
    debug(value);
    Wire.beginTransmission(SLAVE_ID);
    Wire.write(value);
  } while(Wire.endTransmission() != 0);
  int availableBytes =  Wire.requestFrom(SLAVE_ID,4);
  if(availableBytes == 4) {
    byte buffer[4];                 
    buffer[0] = Wire.read();
    buffer[1] = Wire.read();
    buffer[2] = Wire.read();
    buffer[3] = Wire.read();
    unsigned long response = 0;
    response = buffer[0];
    response = (response << 8) | buffer[1];
    response = (response << 8) | buffer[2];
    response = (response << 8) | buffer[3];
    debug("Response from slave: ");
    debug(response);
    return response;
  }
  debug("ERROR! Unexpected number of bytes received");
  debug(availableBytes);
  sendRequestToSlale(value);
}

void redBlink() {
  led.changeColorFull(RED_COLOR);
  delay(300);
  changeLight(ALL,OFF);
}

void greenBlink() {
  lightsOn = false;
  led.changeColorFull(GREEN_COLOR);
  delay(300);
  changeLight(ALL,OFF);
}


