#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define OUT_PIN             9
#define RED_ANALOG_IN_PIN   A0
#define GREEN_ANALOG_IN_PIN A1
#define BLUE_ANALOG_IN_PIN  A2
#define NUMPIXELS           30

//const int redAnalogIn = A0;  // Analog input pin that the potentiometer is attached to
//const int greenAnalogIn = A1;  // Analog input pin that the potentiometer is attached to
//const int blueAnalogIn = A2;  // Analog input pin that the potentiometer is attached to
//const int analogOutPin = 9; // Analog output pin that the LED is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, OUT_PIN, NEO_RGB + NEO_KHZ800);

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  pixels.begin(); 
}

void loop() {

 for(int i=0;i<NUMPIXELS;i++){
 
    pixels.setPixelColor(i, pixels.Color(readNormalAnalogIn(RED_ANALOG_IN_PIN),
                                         readNormalAnalogIn(GREEN_ANALOG_IN_PIN),
                                         readNormalAnalogIn(BLUE_ANALOG_IN_PIN)
                                         )); 

    pixels.show(); // This sends the updated pixel color to the hardware.

    delay(10);

  }
  
}

int readNormalAnalogIn(int input) {
  sensorValue = analogRead(input);
  outputValue = map(sensorValue, 0, 1023, 0, 255);

  Serial.print(input);
  Serial.print(" sensor = ");
  Serial.print(sensorValue);
  Serial.print("\t output = ");
  Serial.println(outputValue);
  
  return outputValue;
}

