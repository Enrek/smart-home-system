#include <DigitalController.h>
#include <StepperDC.h>
#include <EEPROM.h>

ErrorBus errorBus;
const byte additionalPins = 16;
const byte dataPin = 9;
const byte clockPin = 7;
const byte latchPin = 8;
const byte platformDigitalPinsNum = 13;
DigitalController digitalController(additionalPins, dataPin, clockPin, latchPin, platformDigitalPinsNum, errorBus);
const int stepsPerRevolution = 4095;
StepperDC myStepper(digitalController , stepsPerRevolution, 15, 16, 17, 18); 
int moveSpeed = 7;
void setup() {
  myStepper.setSpeed(moveSpeed);
   Serial.begin(9600);
}

bool zero = false;
bool setUp = false;
bool waitValue = false;
int inputValue;
byte head;
int perMove = 0;

void loop() {
  
  if(!zero) {
    readComm();
    if(inputValue == 0) {
      zero = true;
      Serial.println("ZERO POS SET");
      return;
    }
    Serial.print("Move ");
    Serial.println(inputValue);
    myStepper.step(inputValue);
  } else if(!setUp) {
    readComm();
     if(inputValue == 0) {
      setUp = true;
      Serial.println("SET UP DONE");
      return;
    }
    Serial.print("Move ");
    Serial.println(inputValue);
    myStepper.step(inputValue);
    perMove -= inputValue;
  } else {
    if(Serial.available()) {
      moveSpeed = Serial.parseInt();
      while(Serial.available()) {
       Serial.read();
      }
       Serial.print("New speed ");
       Serial.println(moveSpeed);
       myStepper.setSpeed(moveSpeed);
    }
    Serial.println(" + ");
    delay(200);
    myStepper.step(perMove);
    delay(200);
    Serial.println(" - ");
    myStepper.step(-perMove);
  }
}

void readComm() {
  Serial.println("Wait command");
  while(!Serial.available()) {
   delay(2);
  }
  if(waitValue) {
    inputValue = (head == 0 ? -1 : 1) * Serial.parseInt();
    waitValue = false;
  } else {
    head  = Serial.parseInt();
    waitValue = true;
  }
  while(Serial.available()) {
   Serial.read();
  }
  if(waitValue) {
    readComm();
  }
}

