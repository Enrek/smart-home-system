// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            6

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      17

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_RGB + NEO_KHZ800);

int delayval = 1000; // delay for half a second

int ledAddressSectors[] = {5,3,5,4}; 
int colors[][3] = {{255,0,0},{0,255,0},{0,0,255},{255,255,0},{255,0,255},{0,255,255}};
void setup() {
   pixels.begin(); 
   randomSeed(analogRead(0));
   Serial.begin(9600);
}

void loop() {

  // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.
  int offset = 0;
  int index = (int)random(4);
  for(int i=0;i<index;i++){
     offset+=ledAddressSectors[i];
  }
  int color = (int)random(6);
  changeRange(offset,ledAddressSectors[index] + offset, pixels.Color(colors[color][0],colors[color][1],colors[color][2]));
  delay(delayval); 
}

void changeRange(int from, int to, uint32_t color) {
  for(int ledInSectorIndex=from; ledInSectorIndex < to; ledInSectorIndex++) { 
     pixels.setPixelColor(ledInSectorIndex, color);  
  }
  pixels.show(); 
}

