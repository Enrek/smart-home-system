#include <DigitalController.h>

ErrorBus errorBus;
const byte additionalPins = 16;
const byte dataPin = 9;
const byte clockPin = 7;
const byte latchPin = 8;
const byte platformDigitalPinsNum = 13;
DigitalController digitalController(additionalPins, dataPin, clockPin, latchPin, platformDigitalPinsNum, errorBus);

void setup() {
}



void loop() {
  digitalController.setPin(23, HIGH);
  delay(5000);
  digitalController.setPin(23, LOW);
  delay(5000);
}
