#include <DigitalController.h>

ErrorBus errorBus;
const byte additionalPins = 16;
const byte dataPin = 9;
const byte clockPin = 7;
const byte latchPin = 8;
const byte platformDigitalPinsNum = 13;
DigitalController digitalController(additionalPins, dataPin, clockPin, latchPin, platformDigitalPinsNum, errorBus);

void setup() {
  Serial.begin(9600);
}

byte ledON[] = {65,69,73,77,81,85};
byte ledOFF[] = {64,68,72,76,80,84};

void loop() {
  digitalController.setPin(23, HIGH);
  
  for(byte i=0; i < 6; i++) {
    Serial.println(ledON[i]);
    delay(100);       
  }
  delay(1000);
  for(byte i=0; i < 6; i++) {
    Serial.println(ledOFF[i]);
    delay(100);       
  }

 
  digitalController.setPin(23, LOW);
  delay(1000);
}
