#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN            6
#define NUMPIXELS      17

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
int inputPin = 3;
int ledAddressSectors[] = {5,3,5,4}; 
void setup() {
   pixels.begin(); 
   pinMode(inputPin, INPUT); 
   Serial.begin(9600);
}
int prevVal = LOW;
void loop() {
  int offset = 0;

  if(digitalRead(inputPin) == HIGH && prevVal != HIGH) {
    changeRange(0,16, pixels.Color(255,0,0));
    prevVal = HIGH;
  } else if(prevVal != LOW) {
     changeRange(0,16, pixels.Color(0,0,0));
    prevVal = LOW;
  }
  delay(100); 
}

void changeRange(int from, int to, uint32_t color) {
  for(int ledInSectorIndex=from; ledInSectorIndex < to; ledInSectorIndex++) { 
     pixels.setPixelColor(ledInSectorIndex, color);  
  }
  pixels.show(); 
}

