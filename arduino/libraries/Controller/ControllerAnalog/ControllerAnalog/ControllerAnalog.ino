#include "ControllerAnalog.h"

ErrorBus errorBus;
int inputToMappedOutput[][2] = {{A0,4},{A1,3},{A2,2}};
byte inputNum = 3;
int maxValue = 720;

int testLights[][3] = {{8,7,5},
                       {4,3,0},
                       {2,0,0}};
ControllerAnalog controllerAnalog(inputToMappedOutput, inputNum, maxValue, errorBus);

void setup() {
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  Serial.begin(9600);
}

void loop() {
   for(byte i=0; i<inputNum; i++) {
     int outIndex = controllerAnalog.readValue(i);
     for(byte j=0; j<3; j++) {
        int ledIndex = testLights[i][j];
        if(ledIndex == 0) 
          break;
        if(outIndex <= j) {
          digitalWrite(ledIndex, LOW);        
        } else {
          digitalWrite(ledIndex, HIGH);  
        }
     }
     delay(10);
  }

  delay(10);
}


