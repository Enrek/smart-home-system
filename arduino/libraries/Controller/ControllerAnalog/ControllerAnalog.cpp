#include "ControllerAnalog.h"

ControllerAnalog::ControllerAnalog(int inputToMaxOutput[][2], byte inputNum, int maxValue, ErrorBus &errorBus) {
  this->inputToMaxOutput = inputToMaxOutput;
  this->errorBus = &errorBus;
  this->inputNum = inputNum;
  this->maxValue = maxValue;
}

int ControllerAnalog::readValue(byte inputIndex) {
  if(inputIndex >= inputNum || inputIndex < 0) {
    errorBus->setError(ILLEGAL_DATA_ADDRESS);
    return -1;
  }
 
  return min(map(analogRead(inputToMaxOutput[inputIndex][0]), 0, maxValue, 0, inputToMaxOutput[inputIndex][1]),  inputToMaxOutput[inputIndex][1]);
}
