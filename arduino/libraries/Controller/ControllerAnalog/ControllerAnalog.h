#ifndef ControllerAnalog_h
#define ControllerAnalog_h

#include "Arduino.h"
#include "ErrorBus.h"

/** 
* Ovcharov Dmytro
*/
class ControllerAnalog {
  public:
    ControllerAnalog(int inputToMaxOutput[][2], byte inputNum, int maxValue, ErrorBus &errorBus);
	int readValue(byte inputIndex);
  private:
	int (*inputToMaxOutput)[2];
	ErrorBus *errorBus;
	byte inputNum;
	int maxValue;
};

#endif

