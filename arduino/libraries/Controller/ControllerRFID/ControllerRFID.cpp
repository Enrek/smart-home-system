#include "ControllerRFID.h"

ControllerRFID::ControllerRFID(byte RSTpin, byte SSpin) {
	this->mfrc522 = new MFRC522(SSpin, RSTpin);
	SPI.begin();
    this->mfrc522->PCD_Init();
}

bool ControllerRFID::updateID() {
	if (mfrc522->PICC_IsNewCardPresent() && mfrc522->PICC_ReadCardSerial()) {
		id = 0;
		for (byte i = 0; i < mfrc522->uid.size; i++)
		{
		  idTemp = mfrc522->uid.uidByte[i];
		  id = id * 256 + idTemp;
		}
		return true;
	} 
	return false;
}

unsigned long ControllerRFID::extractID() {
	idTemp = id;
	id = 0;
	return idTemp;
}
