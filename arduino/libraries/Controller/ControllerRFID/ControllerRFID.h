#ifndef ControllerRFID_h
#define ControllerRFID_h

#include "Arduino.h"
#include <SPI.h>
#include <MFRC522.h>

/**
* For extracting card id (RFID) on controller block
* 
* Ovcharov Dmytro
*/
class ControllerRFID {
  public:
    ControllerRFID(byte RSTpin, byte SSpin);
	/**
	* Tries to read card ID if card was read return TRUE and save value else just return FASLE
	*/
	bool updateID();
	/**
	* Extracts ID from storage and resets it.
	* First call extractID() ==> returns ID ####### 
	* Second call  extractID() ==> returns 0
	*/
	unsigned long extractID();
  private:
	MFRC522 *mfrc522;
	unsigned long id, idTemp;
};

#endif

