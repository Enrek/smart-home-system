
#include "ControllerRFID.h"

#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN         9         
#define SS_PIN          10      

ControllerRFID *controllerRFID;  

void setup() {
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  controllerRFID = new ControllerRFID(RST_PIN, SS_PIN);
}

int riseIndex = 2;
bool goUp = false;
unsigned long id = 0;
unsigned long idTemp = 0;

//Turns on leds if card was read only once if twice turns off
void loop() {

  if(controllerRFID->updateID()) { 
    id = controllerRFID->extractID();
    if(id != 0 && idTemp != id) {
      goUp = true;
      idTemp = id;
    } else {
      goUp = false;
    }
  }
  
  if(goUp) {
    digitalWrite(riseIndex, HIGH);   
	  riseIndex++;
    if(riseIndex == 9) {
      riseIndex = 2;
    }
	} else {
	  digitalWrite(riseIndex, LOW);  
    riseIndex--; 
    if(riseIndex == 1) {
      riseIndex = 8;
    }
	}
  delay(1000);
}


