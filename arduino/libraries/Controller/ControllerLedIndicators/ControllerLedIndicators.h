#ifndef ControllerLedIndicators_h
#define ControllerLedIndicators_h

#include "Arduino.h"
#include "ErrorBus.h"

/**
* For switching LEDs (ON/OFF) on controller block
* 
* Ovcharov Dmytro
*/
class ControllerLedIndicators {
  public:
    ControllerLedIndicators(byte pinToLed[], byte ledCount, ErrorBus &errorBus);
	void turnOnLed(byte ledIndex);
	void turnOffLed(byte ledIndex);
  private:
	byte *pinToLed;
	ErrorBus *errorBus;
	byte ledCount;
	bool isLedIndexInRange(byte ledIndex);
};

#endif

