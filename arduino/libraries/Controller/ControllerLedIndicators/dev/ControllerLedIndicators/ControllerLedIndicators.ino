#include "ControllerLedIndicators.h"

byte pinToLed[] = {8,7,5,4,3,2}; 
byte ledCount = 6;
ErrorBus errorBus;
ControllerLedIndicators controllerLedIndicators(pinToLed, 6, errorBus);

void setup() {
    
}

// Test on/off in loop 
void loop() {
  if(errorBus.isErrorSet()) {
    delay(100);
  } else {
    for(byte i = 0; i < ledCount; i++) {
      controllerLedIndicators.turnOnLed(i);
      delay(100); 
    }
    delay(1000);                     
    for(byte i = 0; i < ledCount; i++) {
      controllerLedIndicators.turnOffLed(i);
      delay(100); 
    }
    delay(1000);
//    controllerLedIndicators.turnOffLed(6); // ERROR
  }
}


