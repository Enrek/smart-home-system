#include "ControllerLedIndicators.h"

ControllerLedIndicators::ControllerLedIndicators(byte pinToLed[], byte ledCount, ErrorBus &errorBus) {
  this->pinToLed = pinToLed;
  this->errorBus = &errorBus;
  this->ledCount = ledCount;
  for(byte i = 0; i < ledCount; i++) {
      pinMode(pinToLed[i], OUTPUT);  
  }
}

void ControllerLedIndicators::turnOnLed(byte ledIndex) {
  if(!isLedIndexInRange(ledIndex))
	errorBus->setError(ILLEGAL_DATA_ADDRESS);
  digitalWrite(pinToLed[ledIndex],HIGH);
}

void ControllerLedIndicators::turnOffLed(byte ledIndex) {
  if(!isLedIndexInRange(ledIndex))
	errorBus->setError(ILLEGAL_DATA_ADDRESS);
  digitalWrite(pinToLed[ledIndex],LOW); 
}

bool ControllerLedIndicators::isLedIndexInRange(byte ledIndex) {
	return ledCount > ledIndex;
}
