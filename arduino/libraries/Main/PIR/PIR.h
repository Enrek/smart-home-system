#ifndef PIR_h
#define PIR_h

#include "Arduino.h"

class PIR {
  public:
    PIR(byte pirPin);
	bool isMovmentDetected();
	bool isNewMovement();
  private:
   	byte pirPin;
	bool newMovement;
	bool movement;
};

#endif
