#include <Adafruit_NeoPixel.h>
#include <PIR.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN            6
#define NUMPIXELS      17



Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

PIR topPIR(3);
PIR bottomPIR(4);

int ledAddressSectors[] = {5,3,5,4}; 
void setup() {
   pixels.begin(); 
   Serial.begin(9600);
    changeRange(0,16, pixels.Color(0,0,0));
}
void loop() {
  int offset = 0;
  if(topPIR.isMovmentDetected() && topPIR.isNewMovement()) {
    changeRange(0,16, pixels.Color(255,0,0));
   } else if(bottomPIR.isMovmentDetected() && bottomPIR.isNewMovement()) {
    changeRange(0,16, pixels.Color(0,0,0)); 
  } 
  delay(100); 
}

void changeRange(int from, int to, uint32_t color) {
  for(int ledInSectorIndex=from; ledInSectorIndex < to; ledInSectorIndex++) { 
     pixels.setPixelColor(ledInSectorIndex, color);  
  }
  pixels.show(); 
}
