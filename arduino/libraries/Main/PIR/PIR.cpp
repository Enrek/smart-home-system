#include "PIR.h"

PIR::PIR(byte pirPin)
{
  this->pirPin = pirPin;
  pinMode(pirPin, INPUT); 
}
bool PIR::isMovmentDetected() {
  if(digitalRead(pirPin) == HIGH) {
    if(!movement) {
      newMovement = true;
      movement = true;
    }
  } else {
    newMovement = false;
    movement = false;
  }
  return movement;
}

bool PIR::isNewMovement() {
  bool temp = newMovement;
  newMovement = false;
  return temp;
}
