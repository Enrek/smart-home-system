#include <DigitalController.h>
#include <StepperDC.h>
#include <Sunblind.h>

#define CLOSE 0
#define OPEN 1
#define IN_PROGRESS 2

#define MANUAL

ErrorBus errorBus;
const byte additionalPins = 16;
const byte dataPin = 9;
const byte clockPin = 7;
const byte latchPin = 8;
const byte platformDigitalPinsNum = 13;
DigitalController digitalController(additionalPins, dataPin, clockPin, latchPin, platformDigitalPinsNum, errorBus);
const int stepsPerRevolution = 4095;
const int stepsPerPercent = stepsPerRevolution/100;
StepperDC sunblindStepper(digitalController , stepsPerRevolution, 15, 16, 17, 18);
int perMove = 3600;
int stateAddress = 10;
int sensorPin = 2;
Sunblind sunblind(sunblindStepper, stepsPerPercent, stateAddress, sensorPin);


int moveSpeed = 7;
byte state;
void setup() {
   pinMode(sensorPin, INPUT);
   sunblindStepper.setSpeed(moveSpeed);
   Serial.begin(9600);
   #ifndef MANUAL
   sunblind.calibrate();
   #endif
}

void loop() {
  #ifndef MANUAL 

    Serial.println("Enter move to (0-100))");
    while(!Serial.available()) {
      Serial.print("Current Position: ");
      Serial.println(sunblind.getCurrentPosition());
      sunblind.move();
      delay(100);
    }
    int moveTo = Serial.parseInt();
    Serial.print("Move to : ");
    Serial.println(moveTo);
    sunblind.setPosition(moveTo);
    
    while(Serial.available()) {
     Serial.read();
    }
    delay(100);

  #else    
      Serial.print("Sensor pin");
      Serial.println(digitalRead(sensorPin));
      Serial.println("Enter direction (0 / 1)");
      while(!Serial.available()) {
        delay(2);
      }
      int dir = Serial.parseInt();
      while(Serial.available()) {
       Serial.read();
      }
      Serial.println("Enter steps ");
      while(!Serial.available()) {
        delay(2);
      }
      int steps = (dir == 0 ? -1 : 1) * Serial.parseInt();
      while(Serial.available()) {
       Serial.read();
      }
      sunblindStepper.step(steps);
  #endif
}

