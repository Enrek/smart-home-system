#include "Sunblind.h"

Sunblind::Sunblind(StepperDC &stepperDC, int stepsPerOnePercent, byte erromAddress, byte sensorPin)
{
  this->stepperDC = &stepperDC;
  this->stepsPerOnePercent = stepsPerOnePercent;    
  this->erromAddress = erromAddress;
  this->sensorPin = sensorPin;
}

byte Sunblind::readCurrentPosition() {
  currentPosition = EEPROM.read(erromAddress);
  return currentPosition;
}

void Sunblind::saveCurrentPosition() {
  if(positionUpdated) {
	EEPROM.write(erromAddress, currentPosition);
	positionUpdated = false;
  }
}

void Sunblind::setCurrentPosition(byte value) {
   positionUpdated = true;
   currentPosition = value;
}

byte Sunblind::getCurrentPosition() {
   return currentPosition;
}

void Sunblind::setPosition(byte percents) {
	position = percents;
}

void Sunblind::move() {
	int percentsToPosition = currentPosition - position;
	if(percentsToPosition == 0) {
		saveCurrentPosition();
		return;
	}
	int percents = min(abs(percentsToPosition), PERCENTS_PER_MOVE) * (percentsToPosition/abs(percentsToPosition));
	byte oldPosition = currentPosition;
	if(!positionUpdated) {
		setCurrentPosition(SUNBLIND_IN_PROGRESS);
		saveCurrentPosition();
	} else {
		setCurrentPosition(SUNBLIND_IN_PROGRESS);
	}
	stepperDC->step(percents * stepsPerOnePercent);
	setCurrentPosition(oldPosition+percents*-1);
}

byte Sunblind::getPosition() {
  return position;
}

void Sunblind::calibrate() {
	sensorValue =  digitalRead(sensorPin);
	while(sensorValue == LOW) {
	  stepperDC->step(CALIBRATION_STEP);
	  sensorValue = digitalRead(sensorPin);
	}
	setCurrentPosition(0);
	saveCurrentPosition();
}

