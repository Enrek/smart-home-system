#ifndef Sunblind_h
#define Sunblind_h
#include "Arduino.h"
#include "StepperDC.h"
#include "EEPROM.h"

#define CALIBRATION_STEP 5
#define SUNBLIND_IN_PROGRESS 255
#define PERCENTS_PER_MOVE 1

class Sunblind {
  public:
    Sunblind(StepperDC &stepperDC, int stepsPerOnePercent, byte erromAddress, byte sensorPin);
	void setPosition(byte percents);
	byte getPosition();
	byte getCurrentPosition();
	byte readCurrentPosition();
	void move();
	void calibrate();
  private:
    StepperDC *stepperDC;
	byte erromAddress;
	byte sensorPin;
	byte sensorValue;
	int stepsPerOnePercent;
	byte currentPosition = 255;
	byte position;
	bool positionUpdated;
	void setCurrentPosition(byte value);
	void saveCurrentPosition();
};

#endif

