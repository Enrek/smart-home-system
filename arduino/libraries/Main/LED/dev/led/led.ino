#include <LED.h>

byte sectors[] = {5,3,5,4}; 
int colors[][3] = {{255,0,0},{0,255,0},{0,0,255},{255,255,0},{255,0,255},{0,255,255}};
ErrorBus errorBus;
LED led(sectors,4,6,errorBus);

void setup() {
   led.init(); 
   randomSeed(analogRead(0));
}

void loop() {
  int index = (int)random(4);
  int color = (int)random(6);
  led.changeColorFull(led.packColor(colors[color][0],colors[color][1],colors[color][2]));
  delay(1000); 
  color = (int)random(6);
  led.changeColorInSector(index, led.packColor(colors[color][0],colors[color][1],colors[color][2]));
  delay(1000); 
}

