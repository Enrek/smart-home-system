#include "LED.h"

LED::LED(byte sectors[], byte sectorCount, byte pin, ErrorBus &errorBus)
{
  this->errorBus = &errorBus;
  this->sectorCount = sectorCount;    
  this->sectors = sectors;
  for(byte i=0; i < sectorCount; i++) {
	ledCount+=sectors[i];
  }
  this->led = new Adafruit_NeoPixel(ledCount, pin, NEO_GRB + NEO_KHZ800);
}

void LED::init() {
	led->begin(); 
}

void LED::changeColorFull(uint32_t packedColor) {
	changeRange(0, ledCount, packedColor);
}

void LED::changeColorInSector(int sectorIndex, uint32_t packedColor) {
	if(sectorIndex < 0 || sectorIndex >= sectorCount) {
		errorBus->setError(ILLEGAL_DATA_VALUE);
	}
	byte offset = 0;
	for(byte i=0; i<sectorIndex; i++){
	 offset+=sectors[i];
	}
	changeRange(offset,offset + sectors[sectorIndex], packedColor);
}

void LED::changeRange(byte from, byte to, uint32_t packedColor) {
  for(int ledInSectorIndex=from; ledInSectorIndex < to; ledInSectorIndex++) { 
     led->setPixelColor(ledInSectorIndex, packedColor);  
  }
  led->show(); 
}

uint32_t LED::packColor(byte r, byte g, byte b) {
  return ((uint32_t)r << 16) | ((uint32_t)g <<  8) | b;
}