#ifndef LED_h
#define LED_h
#include <ErrorBus.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define LED_OFF 0
#define LED_LIGHT_ON 1
#define ALARM 2
#define ERROR_BLINK 3
#define SUCCESS_BLINK 4

#define BLACK_INDEX 0
#define WHITE_INDEX 1
#define RED_INDEX 2
#define GREEN_INDEX 2

class LED {
  public:
    LED(byte sectors[], byte sectorCount, byte pin, ErrorBus &errorBus);
	void changeColorFull(uint32_t packedColor);
	void changeColorInSector(int sectorIndex, uint32_t packedColor);
	void changeRange(byte from, byte to, uint32_t packedColor);
	void init();
	uint32_t packColor(byte r, byte g, byte b);
  private:
   	byte *sectors;
	ErrorBus *errorBus;
	Adafruit_NeoPixel *led;
	byte ledCount;
	byte sectorCount;
};

#endif
