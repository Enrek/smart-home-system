#ifndef StepperDC_h
#define StepperDC_h
#include "Arduino.h"
#include "DigitalController.h"

// library interface description
class StepperDC {
  public:
    // constructors:
    StepperDC(DigitalController &digitalController, int number_of_steps, int motor_pin_1, int motor_pin_2,
                                 int motor_pin_3, int motor_pin_4);
    
    // speed setter method:
    void setSpeed(long whatSpeed);

    // mover method:
    void step(int number_of_steps);
   
  private:
	void stepMotor(int this_step);
	
    int direction;            // Direction of rotation
    unsigned long step_delay; // delay between steps, in ms, based on speed
    int number_of_steps;      // total number of steps this motor can take
    int step_number;          // which step the motor is on
	DigitalController *digitalController; 
	
    // motor pin numbers:
    int inputPins[4];
	bool sequence[8][4] = {{LOW, LOW, LOW, HIGH},

							{LOW, LOW, HIGH, HIGH},

							{LOW, LOW, HIGH, LOW},

							{LOW, HIGH, HIGH, LOW},

							{LOW, HIGH, LOW, LOW},

							{HIGH, HIGH, LOW, LOW},

							{HIGH, LOW, LOW, LOW},

							{HIGH, LOW, LOW, HIGH}};
	
    unsigned long last_step_time; // time stamp in us of when the last step was taken
};

#endif

