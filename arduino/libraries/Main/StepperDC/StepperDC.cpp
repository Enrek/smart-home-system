#include "StepperDC.h"

StepperDC::StepperDC(DigitalController &digitalController, int number_of_steps, int motor_pin_1, int motor_pin_2,
                                      int motor_pin_3, int motor_pin_4)
{
  this->digitalController = &digitalController;
  this->step_number = 0;    // which step the motor is on
  this->direction = 0;      // motor direction
  this->last_step_time = 0; // time stamp in us of the last step taken
  this->number_of_steps = number_of_steps; // total number of steps for this motor

  // Arduino pins for the motor control connection:
	this->inputPins[0] = motor_pin_1;
	this->inputPins[1] = motor_pin_2;
	this->inputPins[2] = motor_pin_3;
	this->inputPins[3] = motor_pin_4;

  // setup the pins on the microcontroller:
  for (int inputCount = 0; inputCount < 4; inputCount++){
	pinMode(this->inputPins[inputCount], OUTPUT);
  }

}

/*
 * Sets the speed in revs per minute
 */
void StepperDC::setSpeed(long whatSpeed)
{
  this->step_delay = 60L * 1000L * 1000L / this->number_of_steps / whatSpeed;
}

/*
 * Moves the motor steps_to_move steps.  If the number is negative,
 * the motor moves in the reverse direction.
 */
void StepperDC::step(int steps_to_move)
{
  int steps_left = abs(steps_to_move);  // how many steps to take

  // determine direction based on whether steps_to_mode is + or -:
  if (steps_to_move > 0) { this->direction = 1; }
  if (steps_to_move < 0) { this->direction = 0; }


  // decrement the number of steps, moving one step each time:
  while (steps_left > 0)
  {
    unsigned long now = micros();
    // move only if the appropriate delay has passed:
    if (now - this->last_step_time >= this->step_delay)
    {
      // get the timeStamp of when you stepped:
      this->last_step_time = now;
	 
      // increment or decrement the step number,
      // depending on direction:
      if (this->direction == 1)
      {
        this->step_number++;
        if (this->step_number == this->number_of_steps) {
          this->step_number = 0;
        }
      }
      else
      {
        if (this->step_number == 0) {
          this->step_number = this->number_of_steps;
        }
        this->step_number--;
      }
      // decrement the steps left:
      steps_left--;
      // step the motor to step number 0, 1, ..., {3 or 10}
      stepMotor(this->step_number % 8);
    }
  }
  //clear
  for (int inputCount = 0; inputCount < 4; inputCount++){
	this->digitalController->setPin(this->inputPins[inputCount], LOW);
  }
}

/*
 * Moves the motor forward or backwards.
 */
void StepperDC::stepMotor(int thisStep)
{
	for(int inputIndex = 0; inputIndex < 4; inputIndex++){
        this->digitalController->setPin(inputPins[inputIndex], sequence[thisStep][inputIndex]);
    }
}

