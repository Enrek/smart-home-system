#include "ErrorBus.h"

void ErrorBus::setError(byte errorCode) {
  this->errorCode = errorCode;
}

bool ErrorBus::isErrorSet() {
  return errorCode != 0;
}

byte ErrorBus::getError() {
  return errorCode;
}

void ErrorBus::removeError() {
  errorCode = 0;
}

