#include "ErrorBus.h"

ErrorBus errorBus;

void setup() {
  pinMode(2, OUTPUT);
  pinMode(8, OUTPUT);
  randomSeed(analogRead(0));
}

void loop() {
  atomicFunction(random(3));
  if(errorBus.isErrorSet()) {
    if(errorBus.getError() == ILLEGAL_DATA_VALUE) {
        digitalWrite(2, HIGH);   
        delay(1000);                       
        digitalWrite(2, LOW);   
        delay(1000); 
    }
  } else {
    digitalWrite(8, HIGH);   
    delay(1000);                       
    digitalWrite(8, LOW);   
    delay(1000); 
  }
   delay(500); 
   errorBus.removeError();
}

boolean atomicFunction(int param) {
  if(param == 0) {
    errorBus.setError(ILLEGAL_DATA_VALUE);
    return false; //will be ignored
  }
  return false;
}
