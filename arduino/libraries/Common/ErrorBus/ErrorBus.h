#ifndef ErrorBus_h
#define ErrorBus_h

// Modbus error codes
#define ILLEGAL_FUNCTION 0x01
#define ILLEGAL_DATA_ADDRESS 0x02
#define ILLEGAL_DATA_VALUE 0x03
#define SLAVE_DEVICE_FAILURE 0x04

#include "Arduino.h"

class ErrorBus 
{
public:  	
	void setError(byte errorCode);
	bool isErrorSet();
	byte getError();
	void removeError();
private:
	  byte errorCode;
};

#endif
