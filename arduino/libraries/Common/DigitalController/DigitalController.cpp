#include "DigitalController.h"

DigitalController::DigitalController(byte bitCount, byte dataPin, byte clockPin, byte latchPin, byte platformDigitalPinsNum, ErrorBus &errorBus) {
  this->bitCount = bitCount;
  this->byteCount = bitCount/8;
  this->writeBuffer = new byte[byteCount];
  pinMode(dataPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(latchPin, OUTPUT);
  this->dataPin = dataPin;
  this->clockPin = clockPin;
  this->latchPin = latchPin;
  this->platformDigitalPinsNum = platformDigitalPinsNum;
}

void DigitalController::setPin(byte pinIndex, bool value) {
	byte bitIndex = pinIndex - platformDigitalPinsNum - 1;
	if(pinIndex >= bitCount || bitIndex < 0) {
		errorBus->setError(ILLEGAL_DATA_ADDRESS);
	}
	setBit(bitIndex, value);
	writeAllBits();
}

void DigitalController::resetAllPins() {
	for(byte i = 0; i < this->byteCount; i++) {
		this->writeBuffer[i] = 0;
    }
	writeAllBits();
}

void DigitalController::setBit(byte bitIndex, bool value) {
	byte bytenum = bitIndex / 8;
	byte offset = bitIndex % 8;
	byte b = this->writeBuffer[bytenum];
	bitWrite(b, offset, value);
	this->writeBuffer[bytenum] = b;
}

void DigitalController::writeAllBits() {
  digitalWrite(latchPin, LOW);
  digitalWrite(clockPin, LOW);
  for(byte i = 0; i < this->byteCount; i++) {
    shiftOut(dataPin, clockPin, MSBFIRST, this->writeBuffer[i]);
  }
  digitalWrite(latchPin, HIGH);
}

