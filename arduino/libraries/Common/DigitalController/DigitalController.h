
#ifndef DigitalController_h
#define DigitalController_h

#include "Arduino.h"
#include "ErrorBus.h"

class DigitalController 
{
public:
	  DigitalController(byte bitCount, byte dataPin, byte clockPin, byte latchPin, byte platformDigitalPinsNum, ErrorBus &errorBus);
	  void setPin(byte pinIndex, bool value);
	  void resetAllPins();
private:
	ErrorBus *errorBus;

	byte dataPin;
	byte clockPin;
	byte latchPin;
	byte platformDigitalPinsNum;
	
	byte bitCount;
	byte byteCount;
	byte *writeBuffer;
		
	void writeAllBits();
	void setBit(byte bitIndex, bool value);
};

#endif
