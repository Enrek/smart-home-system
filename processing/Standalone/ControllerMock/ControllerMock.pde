import processing.serial.*;

//JUST FOR TESTING DURING DEVELOPMENT 
//WARNING! BAD CODE

Serial arduinoPort; 
String val;       

final int LED =  64;
final int RFID = 128;
final int POT = 192;

final int TURN_ON =  1;
final int TURN_OFF =  0;
final int READ =  0b11;

Handle[] handles;
PImage img;
Button validRfidButton, invalidRfidButton;
boolean sendValidRFID;
boolean sendInvalidRFID;
int validRFID = 2058868779;

void setup() 
{
  size(640, 360);
  int num = 3;
  handles = new Handle[num];
  int hsize = 30;
  handles[0] = new Handle(width/2, hsize, 50-hsize/2,  hsize, 100, handles);
  handles[1] = new Handle(width/2, hsize+100, 50-hsize/2, hsize, 24, handles);
  handles[2] = new Handle(width/2, hsize+40, 50-hsize/2, hsize, 100, handles);
  String[] portName = Serial.list();
  if(portName.length != 0) {
    arduinoPort = new Serial(this, portName[0], 9600);
  }
  img = loadImage("house.jpg");
  validRfidButton = new Button("Valid RFID", width/2 + (width/2)*0.5 - 140, 300, 130, 50);
  invalidRfidButton = new Button("Invalid RFID", width/2 + (width/2)*0.5 + 10, 300, 130, 50);
}

boolean[] leds = new boolean[6];

boolean synchronize = false;

void draw() {

  background(153);


//COLD ZONE
  fill(0,155,175);
  rect(width/2, 50, (width/2)*0.4, 50);
//WARM ZONE
  fill(255, 212, 0);
  rect(width/2 + (width/2)*0.4, 50, 200, 50);
  
//RED ZONE
  fill(255,0,0);
  rect(width/2 + (width/2)*0.8, 0,  (width/2)*0.2, 100);
  
//NIGHT <6
  fill(100);
  rect(width/2 , 100, (width/2)*0.25, 50);
//DAY >6 <18
  fill(255);
  rect(width/2 + (width/2)*0.25, 100, (width/2)*0.5, 50);
//NIGHT >18
  fill(100);
  rect(width/2 + (width/2)*0.75, 100, (width/2)*0.25, 50);
  for (int i = 0; i < handles.length; i++) {
    handles[i].update();
    handles[i].display();
  }
  if(sendValidRFID) {
    fill(70, 226, 112);
    validRfidButton.draw();
    fill(234, 234, 234);
    invalidRfidButton.draw();
  } else if(sendInvalidRFID) {
    fill(234, 234, 234);
    validRfidButton.draw();
    fill(247, 77, 4);
    invalidRfidButton.draw();
  } else {
    fill(234, 234, 234);
    validRfidButton.draw();
     fill(234, 234, 234);
    invalidRfidButton.draw();
  }

//LEDS
  //fill(103, 214, 59);
  //rect(width/2 , 225,  (width/2)*0.5, 50);
  //fill(219, 78, 13);
  //rect(width/2 + (width/2)*0.5, 225,  (width/2)*0.5, 50);
  for (int i = 0; i < leds.length; i++) {
     if(leds[i]) {
       if(i < 3) {
         fill(5, 255, 71);
       } else {
         fill(255, 58, 0);
       }
     } else {
       if(i < 3) {
         fill(13, 142, 11);
       } else {
         fill(142, 41, 11);
       }
     }
     ellipse(width/2 + 35 + 50 * i, 250 , 30, 30);
  }

  image(img, 0, 0, width/2, height);

  if(arduinoPort != null && arduinoPort.available() != 0) {
    println("IN");
    if(!synchronize) {
      println("SYN");
      if(arduinoPort.readBytes()[0] == 0) {
        println("SYN DONE");
        arduinoPort.write(0);
        synchronize = true;
      }
      return;
    }
    processInputCommand(arduinoPort.readBytes()[0]);
  }
}



void mousePressed()
{
  if (validRfidButton.mouseIsOver()) {
    sendValidRFID = true;
    sendInvalidRFID = false;
  } else if(invalidRfidButton.mouseIsOver()) {
    sendValidRFID = false;
    sendInvalidRFID =  true;
  }
}
void mouseReleased()  {
  for (int i = 0; i < handles.length; i++) {
    handles[i].releaseEvent();
  }
}

void processInputCommand(int command) {
  int componentId = (command & 0x3C) >> 2;
  switch (command & 0xC3) { // clear component id part
    case POT | READ:
      println("POT " + componentId);
      writeLn(readPot(componentId));
    break;
    case RFID | READ:
        println("RFID");
        writeLn(readRFID());
        sendValidRFID = sendInvalidRFID = false;
    break;
    case LED | TURN_ON:
       println("LED ON " + componentId);
       leds[componentId] = true;
       arduinoPort.write(0);
    break;
    case LED | TURN_OFF:
       println("LED OFF " + componentId);
       leds[componentId] = false;
       arduinoPort.write(0);
    break;
    default:
      
    break;
  }
}

int readRFID() {
  if(sendValidRFID) 
    return validRFID;
  if(sendInvalidRFID)
    return 1;
  return 0;
}

int readPot(int id) {
  return handles[id].getValue();
}

void writeLn(int value) {
    char[] chars = String.valueOf(value).toCharArray();
    for(int i=0; i < chars.length;  i++) {
      arduinoPort.write(chars[i]);
    }
    arduinoPort.write('\r');
}


boolean isNumeric(String str)  
{  
  try  
  {  
    double d = Double.parseDouble(str);  
  }  
  catch(NumberFormatException nfe)  
  {  
    return false;  
  }  
  return true;  
}


class Button {
  String label; // button label
  float x;      // top left corner x position
  float y;      // top left corner y position
  float w;      // width of button
  float h;      // height of button
   
  
  // constructor
  Button(String labelB, float xpos, float ypos, float widthB, float heightB) {
    label = labelB;
    x = xpos;
    y = ypos;
    w = widthB;
    h = heightB;
  }
  
  void draw() {
    stroke(141);
    rect(x, y, w, h, 10);
    textAlign(CENTER, CENTER);
    fill(0);
    text(label, x + (w / 2), y + (h / 2));
  }
  
  boolean mouseIsOver() {
    if (mouseX > x && mouseX < (x + w) && mouseY > y && mouseY < (y + h)) {
      return true;
    }
    return false;
  }
}


class Handle {
  
  int x, y;
  int boxx, boxy;
  int stretch;
  int size;
  int maxValue;
  boolean over;
  boolean press;
  boolean locked = false;
  boolean otherslocked = false;
  Handle[] others;
  
  Handle(int ix, int iy, int il, int is, int maxValue, Handle[] o) {
    x = ix;
    y = iy;
    stretch = il;
    size = is;
    boxx = x+stretch - size/2;
    boxy = y - size/2;
    this.maxValue = maxValue;
    others = o;
  }
  
  int getValue() {
    return (int)map(stretch, 0, width-x-size, 0, maxValue+1);
  }
  
  void update() {
    boxx = x+stretch;
    boxy = y - size/2;
    
    for (int i=0; i<others.length; i++) {
      if (others[i].locked == true) {
        otherslocked = true;
        break;
      } else {
        otherslocked = false;
      }  
    }
    
    if (otherslocked == false) {
      overEvent();
      pressEvent();
    }
    
    if (press) {
      stretch = lock(mouseX-width/2-size/2, 0, width/2-size-1);
    }
  }
  
  void overEvent() {
    if (overRect(boxx, boxy, size, size)) {
      over = true;
    } else {
      over = false;
    }
  }
  
  void pressEvent() {
    if (over && mousePressed || locked) {
      press = true;
      locked = true;
    } else {
      press = false;
    }
  }
  
  void releaseEvent() {
    locked = false;
  }
  
  void display() {
    line(x, y, x+stretch, y);
    fill(255);
    stroke(0);
    rect(boxx, boxy, size, size);
    fill(0);
    textSize(16);
    text(getValue(), boxx-size*0.5, boxy+size*0.2);
    if (over || press) {
      line(boxx, boxy, boxx+size, boxy+size);
      line(boxx, boxy+size, boxx+size, boxy);
    }

  }
}

boolean overRect(int x, int y, int width, int height) {
  if (mouseX >= x && mouseX <= x+width && 
      mouseY >= y && mouseY <= y+height) {
    return true;
  } else {
    return false;
  }
}

int lock(int val, int minv, int maxv) { 
  return  min(max(val, minv), maxv); 
} 