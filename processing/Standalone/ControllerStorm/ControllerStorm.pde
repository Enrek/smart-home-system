import processing.serial.*;

//JUST FOR TESTING DURING DEVELOPMENT 
//WARNING! BAD CODE

Serial arduinoPort; 
String val = "NONE";       

final int LED =  64;
final int RFID = 128;
final int POT = 192;

final int TURN_ON =  1;
final int TURN_OFF =  0;
final int READ =  0b11;


void setup() 
{
  size(640, 360);

  String[] portName = Serial.list();
  if(portName.length != 0) {
    arduinoPort = new Serial(this, portName[0], 9600);
  }
}

boolean sync = false;

boolean wait = false;

void draw() {

  background(153);



  if(!sync) {
    while(arduinoPort.available() == 0) {
       println("Sync");
       arduinoPort.write(0);
       delay(10);
    }
    sync =  true;
    int val = arduinoPort.readBytes()[0];
    println(val);
    if(val == 0) {
      println(val);
      //arduinoPort.write(0);
      //arduinoPort.write('\r');
      sync =  true;
      
      arduinoPort.clear();
       println("DONE");
    }
    delay(100);
    return;
  }
// readERFID();
 //getPortValue();
 changeState();
}

void readERFID() {
  arduinoPort.write(131);
  delay(50);
  while(arduinoPort.available() == 0) {
    delay(20);
  };
  println(arduinoPort.readStringUntil('\r'));
}

boolean on = false;

void changeState() {
  int attempts = 0;
  long past = System.currentTimeMillis();
  arduinoPort.write(on ? 64 : 65);
  delay(50);
  while(arduinoPort.available() == 0) {
    attempts++;
    delay(20);
  };
  int val = arduinoPort.readBytes()[0];
  if(val != 0) {
    println("ERROR");
  }
  println("Value \t Time \t Attempts");
  println(val + "\t" + (System.currentTimeMillis() - past) + "\t" + attempts);
  on = !on;
}

void getPortValue() {
  int attempts = 0;
  long past = System.currentTimeMillis();
  arduinoPort.write(195);
  delay(50);
  while(arduinoPort.available() == 0) {
    attempts++;
    delay(20);
  }
  val = arduinoPort.readStringUntil('\r');
  if(val == null) {
    println("NULL");
    return;
  }
  val = val.trim();
  println("Value \t Time \t Attempts");
  println(val + "\t" + (System.currentTimeMillis() - past) + "\t" + attempts);
  arduinoPort.clear();
 
}

//void writeLn(int value) {
//    char[] chars = String.valueOf(value).toCharArray();
//    for(int i=0; i < chars.length;  i++) {
//      arduinoPort.write(chars[i]);
//    }
//    arduinoPort.write('\r');
//}


boolean isNumeric(String str)  
{  
  try  
  {  
    double d = Double.parseDouble(str);  
  }  
  catch(NumberFormatException nfe)  
  {  
    return false;  
  }  
  return true;  
}

 