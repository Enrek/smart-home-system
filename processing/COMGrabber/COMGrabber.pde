import processing.serial.*; 
 
Serial esp;    // The serial port
Serial ard;
PFont myFont;     // The display font
int data;  // Input string from serial port
int lf = 10;      // ASCII linefeed 
 
void setup() { 
  size(400,200); 
  // List all the available serial ports: 
  printArray(Serial.list()); 
  ard = new Serial(this, Serial.list()[0], 38400); 
  esp = new Serial(this, Serial.list()[1], 38400); 
  
} 
 
void draw() { 
  background(0); 
  text("received: " + Integer.toHexString( data ), 10,50);
  
} 

boolean ARD_LAST = true;

void serialEvent(Serial p) {
  if(ARD_LAST && esp == p) {
   println();
   println("FROM ESP"); 
   ARD_LAST = false;
  }
  if(!ARD_LAST && ard == p) {
     println();
   println("FROM ARD");
   ARD_LAST = true;
  }
  data = p.read();
  String hexS = Integer.toHexString( data );
  hexS = hexS.length() == 1 ? '0' + hexS : hexS;
  hexS = ' '+hexS;
 
   print(hexS);
  
  if(esp == p) {
   ard.write((byte)data);
  }
  if(ard == p) {
    //if(Integer.toHexString( data ).length() == 1)
    //  esp.write(0);
    esp.write((byte)data);
  }
} 